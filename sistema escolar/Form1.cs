﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDbConnection;




namespace Sistema_Escolar
{
    public partial class Form1 : Form
    {
        void LlenarCmbCursos()
        {
            using (DbConnection conn = new DbConnection(true))
            {
                cmbCursos.DataSource = conn.ObtenerNivelesDeCurso();
                cmbCursos.DisplayMember = "Descripcion";
                conn.Close();
            }
        }


        int id;
        public Form1(int id)
        {
            this.id = id;


            if (id == 0)
            {
                MessageBox.Show("Bienvenido Administrador", "Bienvenido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                InitializeComponent();
                LlenarCmbCursos();
                Login2 login = new Login2();
                login.Close();



            }
            else
            {
                MessageBox.Show("Bienvenido Usuario", "Bienvenido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                InitializeComponent();
                LlenarCmbCursos();
                reiniciarAñoToolStripMenuItem.Enabled = false;
                modificarPreciosToolStripMenuItem.Enabled = false;
                sueldosToolStripMenuItem.Enabled = false;
                controlToolStripMenuItem.Enabled = false;
                maestroToolStripMenuItem.Enabled = false;
                Login2 login = new Login2();
                login.Close();
            }

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿Realmente Desea Salir?", "Informacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (dr == DialogResult.OK)
            {
                Close();
            }

            else { }
        }

        private void alumnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Nuevo_Alumno NA = new Nuevo_Alumno();
            NA.ShowDialog();

        }

        private void colegiaturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            radNombre.Checked = true;
            txtBusqueda.Text = contenido;
            lstAlumnos.Items.Clear();
            //          btnAgosto.Enabled = false;
            btnSeptiembre.Enabled = false;
            btnOctubre.Enabled = false;
            btnNoviembre.Enabled = false;
            btnDiciembre.Enabled = false;
            btnDiciembre.Enabled = false;
            btnEnero.Enabled = false;
            btnFebrero.Enabled = false;
            btnMarzo.Enabled = false;
            btnAbril.Enabled = false;
            btnMayo.Enabled = false;
            btnJunio.Enabled = false;
            btnJulio.Enabled = false;
            btnSeguro.Enabled = false;
            btnLibros.Enabled = false;
            btnInscripcion.Enabled = false;
            btnMaterial.Enabled = false;

            Modificar_Informacion_Alumnos ma = new Modificar_Informacion_Alumnos(null, null);
            ma.ShowDialog();
        }

        private void sueldosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InformacionProfesores ip = new InformacionProfesores();
            ip.ShowDialog();
        }

        private void maestroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AgregarPersonalAcademico ap = new AgregarPersonalAcademico();
            ap.ShowDialog();
        }

        string contenido = "Introduzca el nombre o apellido del alumno...";
        private void radNombre_CheckedChanged(object sender, EventArgs e)
        {
            contenido = txtBusqueda.Text = "Introduzca el nombre o apellido del alumno...";
            txtBusqueda.Enabled = true;
            btnBuscar.Enabled = false;
            cmbCursos.Enabled = false;
            lblCurso.Enabled = false;
            LlenarCmbCursos();
        }

        private void radCURP_CheckedChanged(object sender, EventArgs e)
        {
            contenido = txtBusqueda.Text = "Introduzca el CURP del alumno...";
            txtBusqueda.Enabled = true;
            btnBuscar.Enabled = false;
            cmbCursos.Enabled = false;
            lblCurso.Enabled = false;
            LlenarCmbCursos();
        }

        private void radCurso_CheckedChanged(object sender, EventArgs e)
        {
            contenido = txtBusqueda.Text = "Seleccione el curso en el que se encuentra el alumno...";
            txtBusqueda.Enabled = false;
            btnBuscar.Enabled = false;
            cmbCursos.Enabled = true;
            lblCurso.Enabled = true;
            LlenarCmbCursos();
        }

        private void txtBusqueda_TextChanged(object sender, EventArgs e)
        {
            if (txtBusqueda.Text == "")
            {
                txtBusqueda.ForeColor = System.Drawing.Color.DarkGray;
                txtBusqueda.Text = contenido;
            }
            else if (txtBusqueda.Text.Contains(contenido) && txtBusqueda.Text != contenido
                && txtBusqueda.Text != " " + contenido)
            {
                string letra = txtBusqueda.Text.Substring(0, 1);
                txtBusqueda.Text = letra;
                txtBusqueda.Select(1, 0);
                txtBusqueda.ForeColor = System.Drawing.Color.Black;
            }

            if (txtBusqueda.Text.Contains("  "))
            {
                int i = txtBusqueda.Text.IndexOf("  ");
                txtBusqueda.Text = txtBusqueda.Text.Remove(i, 1);
                txtBusqueda.Select(i + 1, 0);
            }

            if (txtBusqueda.Text == " " + contenido)
            {
                txtBusqueda.Text = " " + contenido;
                txtBusqueda.Text = txtBusqueda.Text.Remove(0, 1);
                txtBusqueda.Select(0, 0);
            }
            if (txtBusqueda.Text == " ")
            {
                txtBusqueda.Text = "";
            }

            if (txtBusqueda.Text.Length > 0) btnBuscar.Enabled = true;
            else btnBuscar.Enabled = false;

            if (txtBusqueda.Text == contenido)
            {
                txtBusqueda.ForeColor = System.Drawing.Color.DarkGray;
                btnBuscar.Enabled = false;
                txtBusqueda.Select(0, 0);
            }
        }

        private void txtBusqueda_MouseClick(object sender, MouseEventArgs e)
        {
            if (txtBusqueda.Text == contenido)
            {
                txtBusqueda.Select(0, 0);
            }
        }

        private void txtBusqueda_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnBuscar.PerformClick();

                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        void AgregarCurso(Alumno alumno)
        {
            string curso;

            if (alumno.IdCurso == "MATERNAL")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "1 KINDER")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "2 KINDER")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "3 KINDER")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "1 PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "2 PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "3 PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "4 PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "5 PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "6 PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }

            else if (alumno.IdCurso == "SITUACION ESPECIAL")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "BECADO KINDER")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "BECADO PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            //btnAgosto.Enabled = false;
            btnSeptiembre.Enabled = false;
            btnOctubre.Enabled = false;
            btnNoviembre.Enabled = false;
            btnDiciembre.Enabled = false;
            btnDiciembre.Enabled = false;
            btnEnero.Enabled = false;
            btnFebrero.Enabled = false;
            btnMarzo.Enabled = false;
            btnAbril.Enabled = false;
            btnMayo.Enabled = false;
            btnJunio.Enabled = false;
            btnJulio.Enabled = false;
            btnSeguro.Enabled = false;
            btnLibros.Enabled = false;
            btnInscripcion.Enabled = false;
            btnMaterial.Enabled = false;
            lstAlumnos.Items.Clear();

            if (radNombre.Checked == true)
            {
                string cadena = txtBusqueda.Text.Trim().ToLower();

                using (DbConnection conn = new DbConnection(true))
                {
                    string a, b = "";
                    foreach (Alumno alumno in conn.ObtenerAlumnos())
                    {
                        string nombre = alumno.Nombre.ToLower().Trim();
                        string apellido = alumno.Apellido.ToLower().Trim();
                        string cadenanombre = nombre + " " + apellido;
                        string[] nombre1 = nombre.Split(' ');
                        string[] apellido1 = apellido.Split(' ');
                        string nombre1apellido1 = nombre1[0] + " " + apellido1[0];
                        if ((nombre1apellido1.Contains(cadena)
                            || cadenanombre.Contains(cadena)
                            || apellido.Contains(cadena)
                            || nombre.Contains(cadena)) && txtBusqueda.Text.Length != 0)
                        {
                            a = alumno.IdCurso;
                            if (a != b || lstAlumnos.Items.Count == 0)
                                AgregarCurso(alumno);
                            lstAlumnos.Items.Add(alumno);
                            b = a;
                        }
                        else
                        {
                            string[] cadena2 = txtBusqueda.Text.Trim().ToLower().Split();

                            if (nombre.Contains(cadena2[0]))
                            {
                                a = alumno.IdCurso;
                                if (a != b || lstAlumnos.Items.Count == 0)
                                    AgregarCurso(alumno);
                                lstAlumnos.Items.Add(alumno);
                                b = a;
                            }
                        }
                    }
                    if (lstAlumnos.Items.Count == 0)
                    {
                        MessageBox.Show("¡No se encontró ningún alumno con\nel nombre y/o apellido proporcionado!", "Sin resultados", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    }
                    if (lstAlumnos.Items.Count > 0)
                    {
                        lstAlumnos.Enabled = true;
                        lstAlumnos.Items.RemoveAt(0);
                    }
                    lstAlumnos.DisplayMember = "NombreCompleto";

                    conn.Close();
                }
            }
            else if (radCURP.Checked == true)
            {
                txtBusqueda.Text = txtBusqueda.Text.ToUpper();
                string cadena = txtBusqueda.Text;

                using (DbConnection conn = new DbConnection(true))
                {
                    string a, b = "";
                    Alumno alumno = conn.ObtenerAlumnoPorCURP(cadena);

                    if (alumno != null)
                    {
                        a = alumno.IdCurso;
                        if (a != b || lstAlumnos.Items.Count == 0)
                            AgregarCurso(alumno);
                        lstAlumnos.Items.Add(alumno);
                        b = a;
                    }

                    if (lstAlumnos.Items.Count == 0)
                    {
                        MessageBox.Show("¡No se encontró ningún alumno con\nel CURP proporcionado!", "Sin resultados", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    }
                    if (lstAlumnos.Items.Count > 0)
                    {
                        lstAlumnos.Enabled = true;
                        lstAlumnos.Items.RemoveAt(0);
                    }
                    lstAlumnos.DisplayMember = "NombreCompleto";

                    conn.Close();
                }
            }
        }

        private void cmbCursos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radCurso.Checked == true)
            {
                lstAlumnos.Items.Clear();

                using (DbConnection conn = new DbConnection(true))
                {
                    if (cmbCursos.SelectedItem != null)
                    {
                        Curso curso = cmbCursos.SelectedItem as Curso;
                        string a, b = "";
                        foreach (Alumno alumno in conn.ObtenerAlumnosPorCurso(curso.Id))
                        {
                            a = alumno.IdCurso;
                            if (a != b || lstAlumnos.Items.Count == 0)
                                AgregarCurso(alumno);
                            lstAlumnos.Items.Add(alumno);
                            b = a;
                        }

                        if (lstAlumnos.Items.Count > 0)
                        {
                            lstAlumnos.Enabled = true;
                            lstAlumnos.Items.RemoveAt(0);
                        }
                        lstAlumnos.DisplayMember = "NombreCompleto";
                    }
                    conn.Close();
                }
            }
        }

        private void lstAlumnos_DoubleClick(object sender, EventArgs e)
        {
            string SelectedItem = Convert.ToString(lstAlumnos.SelectedItem);

            if (lstAlumnos.SelectedItem != null && SelectedItem.Length > 0 && !SelectedItem.Contains("CURSO:"))
            {
                using (DbConnection conn = new DbConnection(true))
                {
                    Alumno alumno = lstAlumnos.SelectedItem as Alumno;

                    Papa papa = conn.ObtenerPapaPorId(Convert.ToInt32(alumno.IdPapa));

                    string informacion = String.Format("¿Desea visualizar la información general del alumno?");

                    DialogResult result = MessageBox.Show(informacion,
                                                            "Confirmación",
                                                            MessageBoxButtons.YesNo,
                                                            MessageBoxIcon.Question);
                    switch (result)
                    {
                        case DialogResult.Yes:
                            radNombre.Checked = true;
                            txtBusqueda.Text = contenido;
                            lstAlumnos.Items.Clear();
                            //btnAgosto.Enabled = false;
                            btnSeptiembre.Enabled = false;
                            btnOctubre.Enabled = false;
                            btnNoviembre.Enabled = false;
                            btnDiciembre.Enabled = false;
                            btnDiciembre.Enabled = false;
                            btnEnero.Enabled = false;
                            btnFebrero.Enabled = false;
                            btnMarzo.Enabled = false;
                            btnAbril.Enabled = false;
                            btnMayo.Enabled = false;
                            btnJunio.Enabled = false;
                            btnJulio.Enabled = false;
                            btnSeguro.Enabled = false;
                            btnLibros.Enabled = false;
                            btnInscripcion.Enabled = false;
                            btnMaterial.Enabled = false;
                            Modificar_Informacion_Alumnos form = new Modificar_Informacion_Alumnos(alumno, papa);

                            DialogResult resultado = form.ShowDialog();
                            break;

                        case DialogResult.No:
                        default:
                            break;
                    }

                    conn.Close();
                }
            }
        }
        public bool ChooseFolder()
        {

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
                return true;
            }
            else
            {
                return false;
            }

        }
        private void btnJulio_Click(object sender, EventArgs e)
        {
            Button boton = sender as Button;
            string nombre = boton.Text;

            int dia = dateTimePicker1.Value.Day;
            int mes = dateTimePicker1.Value.Month;
            int año = dateTimePicker1.Value.Year;
            Alumno alumno = lstAlumnos.SelectedItem as Alumno;
            string mesN = alumno.Mes(mes);

            string name = alumno.Nombre;
            string lastname = alumno.Apellido;
            char c = name[0];
            char c1 = lastname[0];
            char c3 = nombre[0];

            string fecha = string.Format("{0} de {1} del {2}", dia, mesN, año);
            string fecha2 = string.Format("{0}/{1}/{2}", año, mes, dia);
            Papa papa;
            int idcurso;
            Costos costo;
            bool flag_folder = true;
            string ruta = ""; 

            using (DbConnection conn = new DbConnection(true))
            {
                papa = conn.ObtenerPapaPorId(alumno.IdPapa);
                idcurso = conn.ObtenerIdCurso(alumno.Id);
                costo = conn.ObtenerCostos(idcurso);
                conn.Close();
            }

            DialogResult res = MessageBox.Show(string.Format("Esta Seguro que desea cobrar el mes de {0}, del Alumno: {1}, el dia {2}", nombre, alumno.NombreCompleto, fecha), "Información", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (res == DialogResult.OK)
            {
                string Folio = string.Format("{0}{1}{2}{3}{4}{5}{6}", c3, dia, mes, año, c, c1, alumno.Id);
                double pricetopay = 0;

                DialogResult NewWindow = MessageBox.Show("¿Desea Realizar el cobro automatico?", "Información", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (NewWindow == DialogResult.Yes)
                {
                    DialogResult res2 = MessageBox.Show("¿Esta pagando un mes atrasado?", "Información", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if ((dia <= 6 && res2 == DialogResult.No) || (res2 == DialogResult.No && alumno.Mes(mes) != nombre))
                    {
                        pricetopay = costo.Primer;
                    }
                    else if ((dia > 6 && dia <= 15) && res2 == DialogResult.No)
                    {
                        pricetopay = costo.Segundo;

                    }
                    else if ((dia > 15 && res2 == DialogResult.No) || (res2 == DialogResult.Yes))
                    {
                        pricetopay = costo.Tercero;
                    }
           
                    using (DbConnection conn = new DbConnection(true))
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            if (ChooseFolder() == true)
                            {
                                ruta = string.Format(@"{0}\{1}.pdf", textBox1.Text, Folio);
                                conn.AgregarIngresos(fecha2, string.Format("Mensualidad: {0} / Alumno: {1}", nombre, alumno.NombreCompleto), pricetopay);
                                conn.ActualizarEstadoMeses(alumno.Id, nombre);
                                conn.GenerarNotaMes(Folio, pricetopay, papa.NombreCompleto, nombre, año, alumno.NombreCompleto, alumno.IdCurso, fecha, textBox1.Text);
                                conn.Close();
                                flag_folder = true;
                                break;
                            }
                            else
                            {
                                flag_folder = false;
                                continue;

                            }
                        }
                    }
                    if (flag_folder == true)
                    {
                        MessageBox.Show("Operacion Exitosa", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        boton.Enabled = false;
                        LectordePdf form = new LectordePdf(ruta);
                        DialogResult resultado = form.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo procesar la solicitud indicada");
                    }
                    
                }
                else
                {
                    Option_Payment payment = new Option_Payment(idcurso);
                    var result = payment.ShowDialog();

                    if (payment.ReturnStatus == true)
                    {
                        if (result == DialogResult.OK)
                        {
                            pricetopay = payment.price;
                            using (DbConnection conn = new DbConnection(true))
                            {
                                for (int i = 0; i<3; i++)
                                {
                                    if (ChooseFolder() == true)
                                    {
                                        ruta = string.Format(@"{0}\{1}.pdf", textBox1.Text, Folio);
                                        conn.AgregarIngresos(fecha2, string.Format("Mensualidad: {0} / Alumno: {1}", nombre, alumno.NombreCompleto), pricetopay);
                                        conn.ActualizarEstadoMeses(alumno.Id, nombre);
                                        conn.GenerarNotaMes(Folio, pricetopay, papa.NombreCompleto, nombre, año, alumno.NombreCompleto, alumno.IdCurso, fecha, textBox1.Text);
                                        conn.Close();
                                        flag_folder = true;
                                        break;
                                    }
                                    else
                                    {
                                        flag_folder = false;
                                        continue;
                                    }
                                }
                            }
                            if(flag_folder == true)
                            {
                                MessageBox.Show("Operacion Exitosa", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                boton.Enabled = false;
                                LectordePdf form = new LectordePdf(ruta);
                                form.ShowDialog();
                            }
                            else
                            {
                                MessageBox.Show("No se proceso la solicitud indicada");
                            }
                            
                        }

                    }
                    else
                    {
                        MessageBox.Show("No se proceso la solicitud indicada");
                    }
                }



            }
        }

        private void lstAlumnos_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SelectedItem = Convert.ToString(lstAlumnos.SelectedItem);

            if (lstAlumnos.SelectedItem != null && SelectedItem.Length > 0 && !SelectedItem.Contains("CURSO:"))
            {
                using (DbConnection conn = new DbConnection(true))
                {
                    Alumno alumno = lstAlumnos.SelectedItem as Alumno;

                    Mensualidades meses = conn.ObtenerMeses(alumno.Id);
                    //btnAgosto.Enabled = meses.Agosto;
                    btnSeptiembre.Enabled = meses.Septiembre;
                    btnOctubre.Enabled = meses.Octubre;
                    btnNoviembre.Enabled = meses.Noviembre;
                    btnDiciembre.Enabled = meses.Diciembre;
                    btnEnero.Enabled = meses.Enero;
                    btnFebrero.Enabled = meses.Febrero;
                    btnMarzo.Enabled = meses.Marzo;
                    btnAbril.Enabled = meses.Abril;
                    btnMayo.Enabled = meses.Mayo;
                    btnJunio.Enabled = meses.Junio;
                    btnJulio.Enabled = meses.Julio;
                    btnInscripcion.Enabled = meses.Inscripcion;
                    btnLibros.Enabled = meses.Libros;
                    btnMaterial.Enabled = meses.Material;
                    btnSeguro.Enabled = meses.Seguro;
                    conn.Close();
                }
            }
            else
            {
                //btnAgosto.Enabled = false;
                btnSeptiembre.Enabled = false;
                btnOctubre.Enabled = false;
                btnNoviembre.Enabled = false;
                btnDiciembre.Enabled = false;
                btnDiciembre.Enabled = false;
                btnEnero.Enabled = false;
                btnFebrero.Enabled = false;
                btnMarzo.Enabled = false;
                btnAbril.Enabled = false;
                btnMayo.Enabled = false;
                btnJunio.Enabled = false;
                btnJulio.Enabled = false;
                btnSeguro.Enabled = false;
                btnLibros.Enabled = false;
                btnInscripcion.Enabled = false;
                btnMaterial.Enabled = false;


            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void modificarPreciosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modificar_Precios mp = new Modificar_Precios();
            mp.ShowDialog();
        }

        private void agregarIngresoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AgregarIngreso ingreso = new AgregarIngreso();
            ingreso.ShowDialog();
        }

        private void corteDiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Control_de_Caja caja = new Control_de_Caja();
            caja.ShowDialog();
        }

        private void agregarGastoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Agregar_Egreso egreso = new Agregar_Egreso();
            egreso.ShowDialog();
        }

        private void adeudoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Adeudores deudas = new Adeudores();
            deudas.ShowDialog();
        }



        private void reiniciarAñoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<Alumno> alumnos6 = new List<Alumno>();
            Papa papa;

            DialogResult dr = MessageBox.Show("Realmente Desea Avanzar al siguiente año, eso ocasionara que su Base de Datos de Ingreso y Egreso se reinicien, de igual manera sus alumnos de 6 de Primaria se eliminaran de su base de datos permanentemente.\n¿Realmente Desea Continuar?", "Advertencia", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (dr == DialogResult.OK)
            {
                using (DbConnection conn = new DbConnection(true))
                {
                    alumnos6 = conn.ObtenerAlumnosPorCurso(9);
                    conn.Close();
                }
                using (DbConnection conn = new DbConnection(true))
                {
                    if (alumnos6.Count() == 0)
                    {
                        conn.ReiniciarAño();
                        conn.Close();
                    }
                    else
                    {
                        if(alumnos6 == null)
                        {
                            Console.WriteLine("ERROR");
                            return;
                        }
                        foreach (Alumno alumno in alumnos6)
                        {
                            papa = conn.ObtenerPapaPorId(alumno.Id);
                            conn.EliminarMensualidades(alumno.Id);
                            conn.DarDeBajaAlumno(alumno.Id);
                            conn.DarDeBajaPapa(papa.Id);
                        }
                        conn.ReiniciarAño();
                        conn.Close();
                    }
                }
                MessageBox.Show("Promocion Concluida Exitosamente", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lstAlumnos.Items.Clear();
            }
        }

        private void cargarListasPorSalonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Login2 login = new Login2();
            login.Show();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool flag_folder = false;
            Button boton = sender as Button;
            string nombre = boton.Text;
            string cuota = "Cuota";
            if (nombre == "Cuota")
            {
                nombre = "libros";
            }
            int dia = dateTimePicker1.Value.Day;
            int mes = dateTimePicker1.Value.Month;
            int año = dateTimePicker1.Value.Year;
            Alumno alumno = lstAlumnos.SelectedItem as Alumno;
            string mesN = alumno.Mes(mes);

            string name = alumno.Nombre;
            string lastname = alumno.Apellido;
            char c = name[0];
            char c1 = lastname[0];
            char c3 = nombre[0];

            string fecha = string.Format("{0} de {1} del {2}", dia, mesN, año);
            string fecha2 = string.Format("{0}/{1}/{2}", dia, mes, año);
            string Descripcion = string.Format("{0} del Alumno {1}", nombre, alumno.NombreCompleto);
            string Folio = string.Format("{0}{1}{2}{3}{4}{5}{6}", c3, dia, mes, año, c, c1, alumno.Id);
            string ruta = "";

            DialogResult res = MessageBox.Show(string.Format("Esta Seguro que desea cobrar {0}, del Alumno: {1}, el dia {2}", nombre, alumno.NombreCompleto, fecha), "Información", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (res == DialogResult.OK)
            {
                
                using (DbConnection conn = new DbConnection(true))
                {
                    Papa papa = conn.ObtenerPapaPorId(alumno.IdPapa);
                    int idcurso = conn.ObtenerIdCurso(alumno.Id);
                    
                    double total;

                    for (int i = 0; i < 3; i++)
                    {
                        if (ChooseFolder() == true)
                        {
                            conn.ActualizarEstadoMeses(alumno.Id, nombre);
                            total = conn.ObtenerTotalBotnesExtra(nombre, idcurso);
                            boton.Enabled = false;
                            conn.AgregarIngresos(dateTimePicker1.Value.ToString("yyyy-MM-dd"), Descripcion, total);
                            ruta = string.Format(@"{0}\{1}.pdf", textBox1.Text, Folio);
                            if (nombre == "libros")
                            {
                                conn.GenerarCheckBox(Folio, total, papa.NombreCompleto, alumno.NombreCompleto, alumno.IdCurso, fecha, cuota, ruta);
                            }
                            else
                            {
                                conn.GenerarCheckBox(Folio, total, papa.NombreCompleto, alumno.NombreCompleto, alumno.IdCurso, fecha, nombre, ruta);
                            }
                            flag_folder = true;
                            break;
                        }
                        else
                        {
                            flag_folder = false;
                            continue;
                        }
                    }
                    conn.Close();
                }
                if (flag_folder == true)
                {
                    MessageBox.Show("Operacion Exitosa", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    boton.Enabled = false;
                    LectordePdf form = new LectordePdf(ruta);
                    DialogResult resultado = form.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No se pudo procesar la solicitud indicada");
                }
                    
            }
        }
                

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }
    }
}
