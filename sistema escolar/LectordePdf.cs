﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AxAcroPDFLib;

namespace Sistema_Escolar
{
    public partial class LectordePdf : Form
    {

        string ruta;
        public LectordePdf(string ruta)
        {
            this.ruta = ruta;
            InitializeComponent();
        }

        private void LectordePdf_Load(object sender, EventArgs e)
        {
            axAcroPDF1.LoadFile(ruta);

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            axAcroPDF1.printWithDialog();
        }

        private void btnSalir_Click_1(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿Realmente Desea Salir?", "Informacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (dr == DialogResult.OK)
            {
                Close();
            }

            else { }
        }

    }
}
