﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDbConnection;

namespace Sistema_Escolar
{
    public partial class AgregarIngreso : Form
    {
        public AgregarIngreso()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != "" && txtTotal.Text != "")
            {
                DialogResult dr = MessageBox.Show("¿Realmente desea agregar el siguiente Ingreso?", "Informacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (dr == DialogResult.OK)
                {
                    using (DbConnection conn = new DbConnection(true))
                    {
                        string fecha = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                        conn.AgregarIngresos(fecha, txtDescripcion.Text, Convert.ToDouble(txtTotal.Text));
                        conn.Close();
                        DialogResult dr2 = MessageBox.Show("¿Desea Agregar Otro Ingreso?", "Información", MessageBoxButtons.YesNo);
                        if (dr2 == DialogResult.Yes)
                        {
                            txtTotal.Clear();
                            txtDescripcion.Clear();
                            dateTimePicker1.Value.ToLocalTime();
                        }
                        else
                        {
                            Close();
                        }
                    }
                }
                else
                {

                }
            }

            else
            {
                MessageBox.Show("Favor de Rellenar Todos los Campos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿Realmente Desea Salir?", "Informacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (dr == DialogResult.OK)
            {
                Close();
            }

            else { }
        }
        public string Mes(int mes)
        {
            if (mes == 1)
            {
                return "Enero";
            }
            else if (mes == 2)
            {
                return "Febrero";
            }
            else if (mes == 3)
            {
                return "Marzo";
            }
            else if (mes == 4)
            {
                return "Abril";
            }
            else if (mes == 5)
            {
                return "Mayo";
            }
            else if (mes == 6)
            {
                return "Junio";
            }
            else if (mes == 7)
            {
                return "Julio";
            }
            else if (mes == 8)
            {
                return "Agosto";
            }
            else if (mes == 9)
            {
                return "Septiembre";
            }
            else if (mes == 10)
            {
                return "Octubre";
            }
            else if (mes == 11)
            {
                return "Noviembre";
            }
            else
            {
                return "Diciembre";
            }
        }
        public bool ChooseFolder()
        {

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
                return true;
            }
            else
            {
                return false;
            }

        }
        private void button3_Click(object sender, EventArgs e)
        {
            bool flag_folder = false;
            string ruta = "";
            DialogResult dr = MessageBox.Show("¿Desea Realizar el siguiente Ingreso?", "Ingresos", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dr == DialogResult.OK)
            {
                int dia = dateTimePicker1.Value.Day;
                int mes = dateTimePicker1.Value.Month;
                int año = dateTimePicker1.Value.Year;
                int hora = dateTimePicker1.Value.Hour;
                int minuto = dateTimePicker1.Value.Minute;
                string fecha2 = string.Format("{0}{1}{2}", dia, mes, año);
                string mesN = Mes(mes);
                string fecha = string.Format("{0} de {1} del {2}", dia, mesN, año);
                string Folio = string.Format("INGRESO{0}{1}{2}{3}", txtTotal.Text, fecha2, hora, minuto);
                using (DbConnection conn = new DbConnection(true))
                {
                    for (int i = 0; i < 3; i++)
                    {
                        if (ChooseFolder() == true)
                        {
                            conn.AgregarIngresos(dateTimePicker1.Value.ToString("yyyy-MM-dd"), txtDescripcion.Text, Convert.ToDouble(txtTotal.Text));
                            ruta = string.Format(@"{0}\{1}.pdf", textBox1.Text, Folio);
                            conn.GeneraPdfIngresos(Convert.ToDouble(txtTotal.Text), dateTimePicker1.Value.ToString("yyyy-MM-dd"), txtDescripcion.Text, ruta);
                            flag_folder = true;
                            break;
                        }
                        else
                        {

                            continue;
                        }
                    }
                    conn.Close();
                }
                if (flag_folder == true)
                {
                    MessageBox.Show("Operacion Exitosa", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LectordePdf form = new LectordePdf(ruta);
                    form.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No se pudo procesar la informacion indicada.");
                }
            }
        }
    }
}
