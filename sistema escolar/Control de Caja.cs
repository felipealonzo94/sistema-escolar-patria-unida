﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDbConnection;

namespace Sistema_Escolar
{
    public partial class Control_de_Caja : Form
    {
        public Control_de_Caja()
        {
            InitializeComponent();
        }
        public int fecha (string nombre)
        {
            nombre = nombre.Trim().ToLower();
            if(nombre=="enero")
            {
                return 1;
            }
            else if (nombre == "febrero")
            {
                return 2;
            }
            else if(nombre =="marzo")
            {
                return 3;
            }
            else if (nombre == "abril")
            {
                return 4;
            }
            else if( nombre=="mayo")
            {
                return 5;
            }
            else if(nombre=="junio")
            {
                return 6;
            }
            else if (nombre == "julio")
            {
                return 7;
            }
            else if(nombre=="agosto")
            {
                return 8;
            }
            else if(nombre=="septiembre")
            {
                return 9;
            }
            else if(nombre== "octubre")
            {
                return 10;
            }
            else if (nombre == "noviembre")
            {
                return 11;
            }
            else if(nombre=="diciembre")
            {
                return 12;
            }
            return 0;
        }
        public int UltimoDiaDelMes(int fecha)
        {
            if (fecha == 2)
            {
                 return 28;
                
            }
            else if (fecha == 4 || fecha == 6 || fecha == 9 || fecha == 11)
            {
                return 30;
            }
            else
            {
                return 31;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            using (DbConnection conn = new DbConnection(true))
            {
                if (rbDia.Checked == true)
                {
                    lstIngresos.DataSource = conn.ObtenerIngresosPorDia(dateTimePicker1.Value.ToString("yyyy-MM-dd"));
                    lstIngresos.DisplayMember = "DescripcionTotal";
                    txtIngresos.Text = conn.ObtenerTotalIngresosDia(dateTimePicker1.Value.ToString("yyyy-MM-dd")).ToString("C2");
                   
                    
                    lstEgresos.DataSource = conn.ObtenerEgresosPorDia(dateTimePicker1.Value.ToString("yyyy-MM-dd"));
                    lstEgresos.DisplayMember = "DescripcionTotal";
                    txtEgresos.Text = conn.ObtenerTotalEgresosDia(dateTimePicker1.Value.ToString("yyyy-MM-dd")).ToString("C2");
                    conn.Close();
                }
                else if(rbAño.Checked==true)
                {
                    lstIngresos.DataSource = conn.ObtenerIngresosPorAño();
                    lstIngresos.DisplayMember = "DescripcionTotal";
                    txtIngresos.Text = conn.ObtenerTotalIngresos().ToString("C2");

                    lstEgresos.DataSource = conn.ObtenerEgresosPorAño();
                    lstEgresos.DisplayMember = "DescripcionTotal";
                    txtEgresos.Text = conn.ObtenerTotalEgresos().ToString("C2");
                }
                else if (rbMes.Checked==true)
                {
                    lstIngresos.DataSource = conn.ObtenerIngresosPorMes(fecha(comboBox1.Text),UltimoDiaDelMes(fecha(comboBox1.Text)),dateTimePicker1.Value.Year);
                    lstIngresos.DisplayMember = "DescripcionTotal";
                    txtIngresos.Text = conn.ObtenerTotalIngresosMes(fecha(comboBox1.Text), UltimoDiaDelMes(fecha(comboBox1.Text)), dateTimePicker1.Value.Year).ToString("C2");

                    lstEgresos.DataSource = conn.ObtenerEgresosPorMes(fecha(comboBox1.Text), UltimoDiaDelMes(fecha(comboBox1.Text)), dateTimePicker1.Value.Year);
                    lstEgresos.DisplayMember = "DescripcionTotal";
                    txtEgresos.Text = conn.ObtenerTotalEgresosMes(fecha(comboBox1.Text), UltimoDiaDelMes(fecha(comboBox1.Text)), dateTimePicker1.Value.Year).ToString("C2");
                }
            }


        }

        private void Control_de_Caja_Load(object sender, EventArgs e)
        {
           
        }

        private void rbMes_CheckedChanged(object sender, EventArgs e)
        {
          
        }

        private void rbMes_Click(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
