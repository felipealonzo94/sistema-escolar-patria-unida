﻿using MyDbConnection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema_Escolar
{
    public partial class Option_Payment : Form
    {
        Costos costo;
        public bool ReturnStatus { get; set; }
        public double price { get; set; }
        int idcurso;

        public Option_Payment(int idcurso)
        {
            this.idcurso = idcurso;
            this.ReturnStatus = false;
            InitializeComponent();
        }
     
        private void Cancelar_Click(object sender, EventArgs e)
        {
            this.ReturnStatus = false;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnCobrar_Click(object sender, EventArgs e)
        {
            if (rbOpcion1.Checked == true)
            {
                this.price = costo.Primer;
            }
            else if (rbOpcion2.Checked == true)
            {
                this.price = costo.Segundo;
            }
            else if (rbOpcion3.Checked == true)
            {
                this.price = costo.Tercero;
            }
            else if (rbOther.Checked == true)
            {
               this.price = Convert.ToDouble(txtOtro.Text);
            }
            this.ReturnStatus = true;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void Option_Payment_Load(object sender, EventArgs e)
        {
            using (DbConnection conn = new DbConnection(true))
            {
                costo = conn.ObtenerCostos(idcurso);
                conn.Close();
            }

            rbOpcion1.Text = costo.Primer.ToString("C2");
            rbOpcion2.Text = costo.Segundo.ToString("C2");
            rbOpcion3.Text = costo.Tercero.ToString("C2");

        }

        private void rbOther_CheckedChanged(object sender, EventArgs e)
        {
            txtOtro.Enabled = true;
            txtOtro.Visible = true;
            txtOtro.Text = "$0.00";
        }

        private void rbOpcion3_Click(object sender, EventArgs e)
        {
            txtOtro.Enabled = false;
            txtOtro.Visible = false;
            txtOtro.Text = "$0.00";
        }

        private void txtOtro_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (ch == 46 && txtOtro.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }
            if(!char.IsDigit(ch) && ch!=8 && ch != 46)
            {
                e.Handled = true;
            }

        }

        private void txtOtro_MouseClick(object sender, MouseEventArgs e)
        {
            txtOtro.Text = "";
        }
    }
}
