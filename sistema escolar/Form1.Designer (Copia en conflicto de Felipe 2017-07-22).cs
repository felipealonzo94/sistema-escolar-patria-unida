﻿namespace Sistema_Escolar
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reiniciarAñoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarListasPorSalonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alumnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maestroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colegiaturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sueldosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarPreciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarGastoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarIngresoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adeudoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corteDiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblAlumnos = new System.Windows.Forms.Label();
            this.lstAlumnos = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblCurso = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.cmbCursos = new System.Windows.Forms.ComboBox();
            this.radCurso = new System.Windows.Forms.RadioButton();
            this.radCURP = new System.Windows.Forms.RadioButton();
            this.radNombre = new System.Windows.Forms.RadioButton();
            this.txtBusqueda = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnJulio = new System.Windows.Forms.Button();
            this.btnMarzo = new System.Windows.Forms.Button();
            this.btnNoviembre = new System.Windows.Forms.Button();
            this.btnJunio = new System.Windows.Forms.Button();
            this.btnMayo = new System.Windows.Forms.Button();
            this.btnFebrero = new System.Windows.Forms.Button();
            this.btnEnero = new System.Windows.Forms.Button();
            this.btnAbril = new System.Windows.Forms.Button();
            this.btnOctubre = new System.Windows.Forms.Button();
            this.btnDiciembre = new System.Windows.Forms.Button();
            this.btnSeptiembre = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSeguro = new System.Windows.Forms.Button();
            this.btnLibros = new System.Windows.Forms.Button();
            this.btnMaterial = new System.Windows.Forms.Button();
            this.btnInscripcion = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.agregarToolStripMenuItem,
            this.modificarToolStripMenuItem,
            this.controlToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1354, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reiniciarAñoToolStripMenuItem,
            this.cargarListasPorSalonToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // reiniciarAñoToolStripMenuItem
            // 
            this.reiniciarAñoToolStripMenuItem.Name = "reiniciarAñoToolStripMenuItem";
            this.reiniciarAñoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.reiniciarAñoToolStripMenuItem.Text = "Reiniciar Año";
            this.reiniciarAñoToolStripMenuItem.Click += new System.EventHandler(this.reiniciarAñoToolStripMenuItem_Click);
            // 
            // cargarListasPorSalonToolStripMenuItem
            // 
            this.cargarListasPorSalonToolStripMenuItem.Name = "cargarListasPorSalonToolStripMenuItem";
            this.cargarListasPorSalonToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.cargarListasPorSalonToolStripMenuItem.Text = "Cerrar Sesion";
            this.cargarListasPorSalonToolStripMenuItem.Click += new System.EventHandler(this.cargarListasPorSalonToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // agregarToolStripMenuItem
            // 
            this.agregarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alumnoToolStripMenuItem,
            this.maestroToolStripMenuItem});
            this.agregarToolStripMenuItem.Name = "agregarToolStripMenuItem";
            this.agregarToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.agregarToolStripMenuItem.Text = "Agregar";
            // 
            // alumnoToolStripMenuItem
            // 
            this.alumnoToolStripMenuItem.Name = "alumnoToolStripMenuItem";
            this.alumnoToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.alumnoToolStripMenuItem.Text = "Alumno";
            this.alumnoToolStripMenuItem.Click += new System.EventHandler(this.alumnoToolStripMenuItem_Click);
            // 
            // maestroToolStripMenuItem
            // 
            this.maestroToolStripMenuItem.Name = "maestroToolStripMenuItem";
            this.maestroToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.maestroToolStripMenuItem.Text = "Personal Academico";
            this.maestroToolStripMenuItem.Click += new System.EventHandler(this.maestroToolStripMenuItem_Click);
            // 
            // modificarToolStripMenuItem
            // 
            this.modificarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.colegiaturasToolStripMenuItem,
            this.sueldosToolStripMenuItem});
            this.modificarToolStripMenuItem.Name = "modificarToolStripMenuItem";
            this.modificarToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.modificarToolStripMenuItem.Text = "Ver o Modificar";
            // 
            // colegiaturasToolStripMenuItem
            // 
            this.colegiaturasToolStripMenuItem.Name = "colegiaturasToolStripMenuItem";
            this.colegiaturasToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.colegiaturasToolStripMenuItem.Text = "Informacion Alumnos";
            this.colegiaturasToolStripMenuItem.Click += new System.EventHandler(this.colegiaturasToolStripMenuItem_Click);
            // 
            // sueldosToolStripMenuItem
            // 
            this.sueldosToolStripMenuItem.Name = "sueldosToolStripMenuItem";
            this.sueldosToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.sueldosToolStripMenuItem.Text = "Informacion Personal";
            this.sueldosToolStripMenuItem.Click += new System.EventHandler(this.sueldosToolStripMenuItem_Click);
            // 
            // controlToolStripMenuItem
            // 
            this.controlToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modificarPreciosToolStripMenuItem,
            this.agregarGastoToolStripMenuItem,
            this.agregarIngresoToolStripMenuItem,
            this.adeudoresToolStripMenuItem,
            this.corteDiaToolStripMenuItem});
            this.controlToolStripMenuItem.Name = "controlToolStripMenuItem";
            this.controlToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.controlToolStripMenuItem.Text = "Control Caja";
            // 
            // modificarPreciosToolStripMenuItem
            // 
            this.modificarPreciosToolStripMenuItem.Name = "modificarPreciosToolStripMenuItem";
            this.modificarPreciosToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.modificarPreciosToolStripMenuItem.Text = "Modificar Precios de Curso";
            this.modificarPreciosToolStripMenuItem.Click += new System.EventHandler(this.modificarPreciosToolStripMenuItem_Click);
            // 
            // agregarGastoToolStripMenuItem
            // 
            this.agregarGastoToolStripMenuItem.Name = "agregarGastoToolStripMenuItem";
            this.agregarGastoToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.agregarGastoToolStripMenuItem.Text = "Agregar Egreso";
            this.agregarGastoToolStripMenuItem.Click += new System.EventHandler(this.agregarGastoToolStripMenuItem_Click);
            // 
            // agregarIngresoToolStripMenuItem
            // 
            this.agregarIngresoToolStripMenuItem.Name = "agregarIngresoToolStripMenuItem";
            this.agregarIngresoToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.agregarIngresoToolStripMenuItem.Text = "Agregar Ingreso";
            this.agregarIngresoToolStripMenuItem.Click += new System.EventHandler(this.agregarIngresoToolStripMenuItem_Click);
            // 
            // adeudoresToolStripMenuItem
            // 
            this.adeudoresToolStripMenuItem.Name = "adeudoresToolStripMenuItem";
            this.adeudoresToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.adeudoresToolStripMenuItem.Text = "Adeudores";
            this.adeudoresToolStripMenuItem.Click += new System.EventHandler(this.adeudoresToolStripMenuItem_Click);
            // 
            // corteDiaToolStripMenuItem
            // 
            this.corteDiaToolStripMenuItem.Name = "corteDiaToolStripMenuItem";
            this.corteDiaToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.corteDiaToolStripMenuItem.Text = "Corte";
            this.corteDiaToolStripMenuItem.Click += new System.EventHandler(this.corteDiaToolStripMenuItem_Click);
            // 
            // lblAlumnos
            // 
            this.lblAlumnos.AutoSize = true;
            this.lblAlumnos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlumnos.Location = new System.Drawing.Point(12, 193);
            this.lblAlumnos.Name = "lblAlumnos";
            this.lblAlumnos.Size = new System.Drawing.Size(58, 15);
            this.lblAlumnos.TabIndex = 22;
            this.lblAlumnos.Text = "Alumnos:";
            // 
            // lstAlumnos
            // 
            this.lstAlumnos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstAlumnos.FormattingEnabled = true;
            this.lstAlumnos.ItemHeight = 15;
            this.lstAlumnos.Location = new System.Drawing.Point(19, 214);
            this.lstAlumnos.Name = "lstAlumnos";
            this.lstAlumnos.Size = new System.Drawing.Size(341, 439);
            this.lstAlumnos.TabIndex = 21;
            this.lstAlumnos.SelectedIndexChanged += new System.EventHandler(this.lstAlumnos_SelectedIndexChanged);
            this.lstAlumnos.DoubleClick += new System.EventHandler(this.lstAlumnos_DoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblCurso);
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Controls.Add(this.cmbCursos);
            this.groupBox1.Controls.Add(this.radCurso);
            this.groupBox1.Controls.Add(this.radCURP);
            this.groupBox1.Controls.Add(this.radNombre);
            this.groupBox1.Controls.Add(this.txtBusqueda);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(454, 121);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opciones de Busqueda:";
            // 
            // lblCurso
            // 
            this.lblCurso.AutoSize = true;
            this.lblCurso.Enabled = false;
            this.lblCurso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurso.Location = new System.Drawing.Point(6, 92);
            this.lblCurso.Name = "lblCurso";
            this.lblCurso.Size = new System.Drawing.Size(42, 15);
            this.lblCurso.TabIndex = 26;
            this.lblCurso.Text = "Curso:";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Enabled = false;
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Location = new System.Drawing.Point(359, 60);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(87, 29);
            this.btnBuscar.TabIndex = 23;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // cmbCursos
            // 
            this.cmbCursos.Enabled = false;
            this.cmbCursos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCursos.FormattingEnabled = true;
            this.cmbCursos.Location = new System.Drawing.Point(53, 89);
            this.cmbCursos.Name = "cmbCursos";
            this.cmbCursos.Size = new System.Drawing.Size(137, 23);
            this.cmbCursos.TabIndex = 25;
            this.cmbCursos.SelectedIndexChanged += new System.EventHandler(this.cmbCursos_SelectedIndexChanged);
            // 
            // radCurso
            // 
            this.radCurso.AutoSize = true;
            this.radCurso.Location = new System.Drawing.Point(341, 31);
            this.radCurso.Name = "radCurso";
            this.radCurso.Size = new System.Drawing.Size(79, 19);
            this.radCurso.TabIndex = 8;
            this.radCurso.Text = "Por Curso";
            this.radCurso.UseVisualStyleBackColor = true;
            this.radCurso.CheckedChanged += new System.EventHandler(this.radCurso_CheckedChanged);
            // 
            // radCURP
            // 
            this.radCURP.AutoSize = true;
            this.radCURP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCURP.Location = new System.Drawing.Point(208, 31);
            this.radCURP.Name = "radCURP";
            this.radCURP.Size = new System.Drawing.Size(81, 19);
            this.radCURP.TabIndex = 7;
            this.radCURP.Text = "Por CURP";
            this.radCURP.UseVisualStyleBackColor = true;
            this.radCURP.CheckedChanged += new System.EventHandler(this.radCURP_CheckedChanged);
            // 
            // radNombre
            // 
            this.radNombre.AutoSize = true;
            this.radNombre.Checked = true;
            this.radNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radNombre.Location = new System.Drawing.Point(7, 31);
            this.radNombre.Name = "radNombre";
            this.radNombre.Size = new System.Drawing.Size(149, 19);
            this.radNombre.TabIndex = 5;
            this.radNombre.TabStop = true;
            this.radNombre.Text = "Por Nombre o Apellido";
            this.radNombre.UseVisualStyleBackColor = true;
            this.radNombre.CheckedChanged += new System.EventHandler(this.radNombre_CheckedChanged);
            // 
            // txtBusqueda
            // 
            this.txtBusqueda.BackColor = System.Drawing.SystemColors.Window;
            this.txtBusqueda.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBusqueda.ForeColor = System.Drawing.Color.DarkGray;
            this.txtBusqueda.Location = new System.Drawing.Point(7, 62);
            this.txtBusqueda.Name = "txtBusqueda";
            this.txtBusqueda.Size = new System.Drawing.Size(341, 21);
            this.txtBusqueda.TabIndex = 18;
            this.txtBusqueda.Text = "Introduzca el nombre o apellido del alumno...";
            this.txtBusqueda.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtBusqueda_MouseClick);
            this.txtBusqueda.TextChanged += new System.EventHandler(this.txtBusqueda_TextChanged);
            this.txtBusqueda.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBusqueda_KeyDown);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnJulio);
            this.groupBox2.Controls.Add(this.btnLibros);
            this.groupBox2.Controls.Add(this.btnMarzo);
            this.groupBox2.Controls.Add(this.btnNoviembre);
            this.groupBox2.Controls.Add(this.btnJunio);
            this.groupBox2.Controls.Add(this.btnMayo);
            this.groupBox2.Controls.Add(this.btnFebrero);
            this.groupBox2.Controls.Add(this.btnEnero);
            this.groupBox2.Controls.Add(this.btnAbril);
            this.groupBox2.Controls.Add(this.btnOctubre);
            this.groupBox2.Controls.Add(this.btnDiciembre);
            this.groupBox2.Controls.Add(this.btnSeptiembre);
            this.groupBox2.Location = new System.Drawing.Point(420, 193);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(670, 460);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Meses";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // btnJulio
            // 
            this.btnJulio.Enabled = false;
            this.btnJulio.Location = new System.Drawing.Point(513, 345);
            this.btnJulio.Name = "btnJulio";
            this.btnJulio.Size = new System.Drawing.Size(132, 108);
            this.btnJulio.TabIndex = 13;
            this.btnJulio.Text = "Julio";
            this.btnJulio.UseVisualStyleBackColor = true;
            this.btnJulio.Click += new System.EventHandler(this.btnJulio_Click);
            // 
            // btnMarzo
            // 
            this.btnMarzo.Enabled = false;
            this.btnMarzo.Location = new System.Drawing.Point(513, 181);
            this.btnMarzo.Name = "btnMarzo";
            this.btnMarzo.Size = new System.Drawing.Size(132, 108);
            this.btnMarzo.TabIndex = 13;
            this.btnMarzo.Text = "Marzo";
            this.btnMarzo.UseVisualStyleBackColor = true;
            this.btnMarzo.Click += new System.EventHandler(this.btnJulio_Click);
            // 
            // btnNoviembre
            // 
            this.btnNoviembre.Enabled = false;
            this.btnNoviembre.Location = new System.Drawing.Point(513, 21);
            this.btnNoviembre.Name = "btnNoviembre";
            this.btnNoviembre.Size = new System.Drawing.Size(132, 108);
            this.btnNoviembre.TabIndex = 13;
            this.btnNoviembre.Text = "Noviembre";
            this.btnNoviembre.UseVisualStyleBackColor = true;
            this.btnNoviembre.Click += new System.EventHandler(this.btnJulio_Click);
            // 
            // btnJunio
            // 
            this.btnJunio.Enabled = false;
            this.btnJunio.Location = new System.Drawing.Point(344, 345);
            this.btnJunio.Name = "btnJunio";
            this.btnJunio.Size = new System.Drawing.Size(132, 108);
            this.btnJunio.TabIndex = 13;
            this.btnJunio.Text = "Junio";
            this.btnJunio.UseVisualStyleBackColor = true;
            this.btnJunio.Click += new System.EventHandler(this.btnJulio_Click);
            // 
            // btnMayo
            // 
            this.btnMayo.Enabled = false;
            this.btnMayo.Location = new System.Drawing.Point(175, 345);
            this.btnMayo.Name = "btnMayo";
            this.btnMayo.Size = new System.Drawing.Size(132, 108);
            this.btnMayo.TabIndex = 13;
            this.btnMayo.Text = "Mayo";
            this.btnMayo.UseVisualStyleBackColor = true;
            this.btnMayo.Click += new System.EventHandler(this.btnJulio_Click);
            // 
            // btnFebrero
            // 
            this.btnFebrero.Enabled = false;
            this.btnFebrero.Location = new System.Drawing.Point(344, 181);
            this.btnFebrero.Name = "btnFebrero";
            this.btnFebrero.Size = new System.Drawing.Size(132, 108);
            this.btnFebrero.TabIndex = 13;
            this.btnFebrero.Text = "Febrero";
            this.btnFebrero.UseVisualStyleBackColor = true;
            this.btnFebrero.Click += new System.EventHandler(this.btnJulio_Click);
            // 
            // btnEnero
            // 
            this.btnEnero.Enabled = false;
            this.btnEnero.Location = new System.Drawing.Point(175, 181);
            this.btnEnero.Name = "btnEnero";
            this.btnEnero.Size = new System.Drawing.Size(132, 108);
            this.btnEnero.TabIndex = 13;
            this.btnEnero.Text = "Enero";
            this.btnEnero.UseVisualStyleBackColor = true;
            this.btnEnero.Click += new System.EventHandler(this.btnJulio_Click);
            // 
            // btnAbril
            // 
            this.btnAbril.Enabled = false;
            this.btnAbril.Location = new System.Drawing.Point(6, 344);
            this.btnAbril.Name = "btnAbril";
            this.btnAbril.Size = new System.Drawing.Size(132, 108);
            this.btnAbril.TabIndex = 13;
            this.btnAbril.Text = "Abril";
            this.btnAbril.UseVisualStyleBackColor = true;
            this.btnAbril.Click += new System.EventHandler(this.btnJulio_Click);
            // 
            // btnOctubre
            // 
            this.btnOctubre.Enabled = false;
            this.btnOctubre.Location = new System.Drawing.Point(344, 19);
            this.btnOctubre.Name = "btnOctubre";
            this.btnOctubre.Size = new System.Drawing.Size(132, 108);
            this.btnOctubre.TabIndex = 13;
            this.btnOctubre.Text = "Octubre";
            this.btnOctubre.UseVisualStyleBackColor = true;
            this.btnOctubre.Click += new System.EventHandler(this.btnJulio_Click);
            // 
            // btnDiciembre
            // 
            this.btnDiciembre.Enabled = false;
            this.btnDiciembre.Location = new System.Drawing.Point(6, 181);
            this.btnDiciembre.Name = "btnDiciembre";
            this.btnDiciembre.Size = new System.Drawing.Size(132, 108);
            this.btnDiciembre.TabIndex = 13;
            this.btnDiciembre.Text = "Diciembre";
            this.btnDiciembre.UseVisualStyleBackColor = true;
            this.btnDiciembre.Click += new System.EventHandler(this.btnJulio_Click);
            // 
            // btnSeptiembre
            // 
            this.btnSeptiembre.Enabled = false;
            this.btnSeptiembre.Location = new System.Drawing.Point(175, 21);
            this.btnSeptiembre.Name = "btnSeptiembre";
            this.btnSeptiembre.Size = new System.Drawing.Size(132, 108);
            this.btnSeptiembre.TabIndex = 13;
            this.btnSeptiembre.Text = "Septiembre";
            this.btnSeptiembre.UseVisualStyleBackColor = true;
            this.btnSeptiembre.Click += new System.EventHandler(this.btnJulio_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(993, 27);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(218, 20);
            this.dateTimePicker1.TabIndex = 24;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(192, 669);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(109, 20);
            this.textBox1.TabIndex = 25;
            this.textBox1.Text = "C:\\Users\\Lenovo\\Desktop";
            this.textBox1.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnSeguro);
            this.groupBox3.Controls.Add(this.btnMaterial);
            this.groupBox3.Controls.Add(this.btnInscripcion);
            this.groupBox3.Location = new System.Drawing.Point(1096, 193);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(230, 232);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Otras Opciones";
            // 
            // btnSeguro
            // 
            this.btnSeguro.Enabled = false;
            this.btnSeguro.Location = new System.Drawing.Point(116, 126);
            this.btnSeguro.Name = "btnSeguro";
            this.btnSeguro.Size = new System.Drawing.Size(88, 64);
            this.btnSeguro.TabIndex = 27;
            this.btnSeguro.Text = "Seguro";
            this.btnSeguro.UseVisualStyleBackColor = true;
            this.btnSeguro.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnLibros
            // 
            this.btnLibros.Enabled = false;
            this.btnLibros.Location = new System.Drawing.Point(6, 21);
            this.btnLibros.Name = "btnLibros";
            this.btnLibros.Size = new System.Drawing.Size(132, 108);
            this.btnLibros.TabIndex = 28;
            this.btnLibros.Text = "Cuota";
            this.btnLibros.UseVisualStyleBackColor = true;
            this.btnLibros.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnMaterial
            // 
            this.btnMaterial.Enabled = false;
            this.btnMaterial.Location = new System.Drawing.Point(116, 18);
            this.btnMaterial.Name = "btnMaterial";
            this.btnMaterial.Size = new System.Drawing.Size(88, 64);
            this.btnMaterial.TabIndex = 27;
            this.btnMaterial.Text = "Material";
            this.btnMaterial.UseVisualStyleBackColor = true;
            this.btnMaterial.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnInscripcion
            // 
            this.btnInscripcion.Enabled = false;
            this.btnInscripcion.Location = new System.Drawing.Point(6, 21);
            this.btnInscripcion.Name = "btnInscripcion";
            this.btnInscripcion.Size = new System.Drawing.Size(88, 64);
            this.btnInscripcion.TabIndex = 28;
            this.btnInscripcion.Text = "Inscripcion";
            this.btnInscripcion.UseVisualStyleBackColor = true;
            this.btnInscripcion.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(327, 678);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(10, 20);
            this.textBox2.TabIndex = 29;
            this.textBox2.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 701);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lblAlumnos);
            this.Controls.Add(this.lstAlumnos);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control Escolar Patria Unida";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reiniciarAñoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alumnoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maestroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sueldosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colegiaturasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem controlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarGastoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarIngresoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem corteDiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarListasPorSalonToolStripMenuItem;
        private System.Windows.Forms.Label lblAlumnos;
        private System.Windows.Forms.ListBox lstAlumnos;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.RadioButton radCURP;
        private System.Windows.Forms.RadioButton radNombre;
        private System.Windows.Forms.TextBox txtBusqueda;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnJulio;
        private System.Windows.Forms.Button btnMarzo;
        private System.Windows.Forms.Button btnNoviembre;
        private System.Windows.Forms.Button btnJunio;
        private System.Windows.Forms.Button btnMayo;
        private System.Windows.Forms.Button btnFebrero;
        private System.Windows.Forms.Button btnEnero;
        private System.Windows.Forms.Button btnAbril;
        private System.Windows.Forms.Button btnOctubre;
        private System.Windows.Forms.Button btnDiciembre;
        private System.Windows.Forms.Button btnSeptiembre;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ToolStripMenuItem modificarPreciosToolStripMenuItem;
        private System.Windows.Forms.RadioButton radCurso;
        private System.Windows.Forms.Label lblCurso;
        private System.Windows.Forms.ComboBox cmbCursos;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripMenuItem adeudoresToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btnSeguro;
        private System.Windows.Forms.Button btnLibros;
        private System.Windows.Forms.Button btnMaterial;
        private System.Windows.Forms.Button btnInscripcion;
    }
}

