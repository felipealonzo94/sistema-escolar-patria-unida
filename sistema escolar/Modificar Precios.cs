﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDbConnection;
namespace Sistema_Escolar
{
    public partial class Modificar_Precios : Form
    {
        public Modificar_Precios()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void Modificar_Precios_Load(object sender, EventArgs e)
        {
            using (DbConnection conn = new DbConnection(true))
            {
                cmbCursos.DataSource = conn.ObtenerNivelesDeCurso();
                cmbCursos.DisplayMember = "Descripcion";
                conn.Close();
            }
        }

        private void cmbCursos_SelectedIndexChanged(object sender, EventArgs e)
        {
            Curso curso= cmbCursos.SelectedItem as Curso;
            using (DbConnection conn = new DbConnection(true))
            {
                Costos costo = conn.ObtenerCostos(curso.Id);
                txt1mes.Text = costo.Primer.ToString() ;
                txt2mes.Text = costo.Segundo.ToString();
                txt3Mes.Text = costo.Tercero.ToString();
                txtInscripcion.Text = costo.Inscripcion.ToString();
                txtLibros.Text = costo.Libros.ToString();
                txtMaterial.Text = costo.Material.ToString();
                txtSeguro.Text = costo.Seguro.ToString();

                conn.Close();
            }
        }

        private void rbVer_CheckedChanged(object sender, EventArgs e)
        {
            if(rbVer.Checked==true)
            {
                txt1mes.ReadOnly = true;
                txt2mes.ReadOnly = true;
                txt3Mes.ReadOnly = true;
                txtInscripcion.ReadOnly = true;
                txtLibros.ReadOnly = true;
                txtMaterial.ReadOnly = true;
                txtSeguro.ReadOnly = true;
                btnModificar.Enabled = false;
            }
            else
            {
                txt1mes.ReadOnly = false;
                txt2mes.ReadOnly = false;
                txt3Mes.ReadOnly = false;
                txtInscripcion.ReadOnly = false;
                txtLibros.ReadOnly = false;
                txtMaterial.ReadOnly = false;
                txtSeguro.ReadOnly = false;
                btnModificar.Enabled = true;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿Realmente Desea Salir?", "Informacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (dr == DialogResult.OK)
            {
                Close();
            }

            else { }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            Curso curso=cmbCursos.SelectedItem as Curso;
            DialogResult dr = MessageBox.Show(string.Format("¿Desea Modificar la siguiente Informacion de {0}?", curso.Descripcion), "Informacion", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dr == DialogResult.Yes)
            {
                using (DbConnection conn = new DbConnection(true))
                {
                    conn.ActualizarPreciosCurso(curso.Id, Convert.ToDouble(txtSeguro.Text), Convert.ToDouble(txtInscripcion.Text), Convert.ToDouble(txtLibros.Text), Convert.ToDouble(txtMaterial.Text), Convert.ToDouble(txt1mes.Text), Convert.ToDouble(txt2mes.Text), Convert.ToDouble(txt3Mes.Text));
                    conn.Close();
                }
            }
        }
    }
}
