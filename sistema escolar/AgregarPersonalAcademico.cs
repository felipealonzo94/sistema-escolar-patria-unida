﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDbConnection;

namespace Sistema_Escolar
{
    public partial class AgregarPersonalAcademico : Form
    {
        public AgregarPersonalAcademico()
        {
            InitializeComponent();
        }

        private void AgregarPersonalAcademico_Load(object sender, EventArgs e)
        {
            using (DbConnection conn = new DbConnection(true))
            {
                cmbCategorias.DataSource = conn.ObtenerNivelCategoria();
                cmbCategorias.DisplayMember = "Descripcion";
                cmbCivil.DataSource = conn.ObtenerEstadoCivil();
                cmbCivil.DisplayMember = "Descripcion";
                cmbNivelEstudios.DataSource = conn.ObtenerNivelesDeCursoProfesor();
                cmbNivelEstudios.DisplayMember = "Descripcion";
                conn.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CategoriaPersonal categoriapersona = cmbCategorias.SelectedItem as CategoriaPersonal;
            EstadoCivil estadocivil = cmbCivil.SelectedItem as EstadoCivil;
            NivelEstudiosPersonal estudiospersonal = cmbNivelEstudios.SelectedItem as NivelEstudiosPersonal;
            //dtpFechaNacimiento.Value.ToString("yyyy-MM-dd")
            if(txtNombre.Text==""|| txtApellido.Text=="" ||txtCelular.Text==""|| txtSueldo.Text=="")
            {
                MessageBox.Show("Favor de rellenar Todos Los Campos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult dr = MessageBox.Show(string.Format("¿Realmente Desea Agregar al Siguiente Personal {0}, {1} en la categoria {2} , en la base de datos?", txtNombre.Text, txtApellido.Text, categoriapersona.Descripcion), "Informacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if(dr==DialogResult.OK)
                {
                    using (DbConnection conn = new DbConnection(true))
                    {
                        conn.AgregarNuevoPersonal(txtNombre.Text, txtApellido.Text, txtCelular.Text, dtpFechaNacimiento.Value.ToString("yyyy-MM-dd"), estadocivil.Id, estudiospersonal.Id, categoriapersona.Id, Convert.ToDouble(txtSueldo.Text));
                        conn.Close();
                    }
                    DialogResult dr2 = MessageBox.Show("¿Desea Agregar Otro Elemento?", "Informacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                    if(dr2==DialogResult.OK)
                    {
                        txtNombre.Text = "";
                        txtApellido.Text = "";
                        txtCelular.Text = "";
                        txtSueldo.Text = "";
                        using (DbConnection conn = new DbConnection(true))
                        {
                            cmbCategorias.DataSource = conn.ObtenerNivelCategoria();
                            cmbCategorias.DisplayMember = "Descripcion";
                            cmbCivil.DataSource = conn.ObtenerEstadoCivil();
                            cmbCivil.DisplayMember = "Descripcion";
                            cmbNivelEstudios.DataSource = conn.ObtenerNivelesDeCursoProfesor();
                            cmbNivelEstudios.DisplayMember = "Descripcion";
                            conn.Close();
                        }
                        dtpFechaNacimiento.ResetText();

                    }
                    else
                    {
                        Close();
                    }
                }
                else
                {

                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿Realmente Desea Salir?", "Informacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (dr == DialogResult.OK)
            {
                Close();
            }

            else { }
        }
    }
}
