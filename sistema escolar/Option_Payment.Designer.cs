﻿namespace Sistema_Escolar
{
    partial class Option_Payment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCobrar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.rbOpcion1 = new System.Windows.Forms.RadioButton();
            this.rbOpcion2 = new System.Windows.Forms.RadioButton();
            this.rbOpcion3 = new System.Windows.Forms.RadioButton();
            this.rbOther = new System.Windows.Forms.RadioButton();
            this.txtOtro = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnCobrar
            // 
            this.btnCobrar.Location = new System.Drawing.Point(237, 23);
            this.btnCobrar.Margin = new System.Windows.Forms.Padding(4);
            this.btnCobrar.Name = "btnCobrar";
            this.btnCobrar.Size = new System.Drawing.Size(163, 58);
            this.btnCobrar.TabIndex = 0;
            this.btnCobrar.Text = "Cobrar";
            this.btnCobrar.UseVisualStyleBackColor = true;
            this.btnCobrar.Click += new System.EventHandler(this.btnCobrar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(237, 110);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(163, 58);
            this.btnCancelar.TabIndex = 0;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.Cancelar_Click);
            // 
            // rbOpcion1
            // 
            this.rbOpcion1.AutoSize = true;
            this.rbOpcion1.Location = new System.Drawing.Point(16, 42);
            this.rbOpcion1.Margin = new System.Windows.Forms.Padding(4);
            this.rbOpcion1.Name = "rbOpcion1";
            this.rbOpcion1.Size = new System.Drawing.Size(110, 21);
            this.rbOpcion1.TabIndex = 1;
            this.rbOpcion1.TabStop = true;
            this.rbOpcion1.Text = "radioButton1";
            this.rbOpcion1.UseVisualStyleBackColor = true;
            this.rbOpcion1.Click += new System.EventHandler(this.rbOpcion3_Click);
            // 
            // rbOpcion2
            // 
            this.rbOpcion2.AutoSize = true;
            this.rbOpcion2.Location = new System.Drawing.Point(16, 70);
            this.rbOpcion2.Margin = new System.Windows.Forms.Padding(4);
            this.rbOpcion2.Name = "rbOpcion2";
            this.rbOpcion2.Size = new System.Drawing.Size(110, 21);
            this.rbOpcion2.TabIndex = 1;
            this.rbOpcion2.TabStop = true;
            this.rbOpcion2.Text = "radioButton1";
            this.rbOpcion2.UseVisualStyleBackColor = true;
            this.rbOpcion2.Click += new System.EventHandler(this.rbOpcion3_Click);
            // 
            // rbOpcion3
            // 
            this.rbOpcion3.AutoSize = true;
            this.rbOpcion3.Location = new System.Drawing.Point(16, 98);
            this.rbOpcion3.Margin = new System.Windows.Forms.Padding(4);
            this.rbOpcion3.Name = "rbOpcion3";
            this.rbOpcion3.Size = new System.Drawing.Size(110, 21);
            this.rbOpcion3.TabIndex = 1;
            this.rbOpcion3.TabStop = true;
            this.rbOpcion3.Text = "radioButton1";
            this.rbOpcion3.UseVisualStyleBackColor = true;
            this.rbOpcion3.Click += new System.EventHandler(this.rbOpcion3_Click);
            // 
            // rbOther
            // 
            this.rbOther.AutoSize = true;
            this.rbOther.Location = new System.Drawing.Point(16, 127);
            this.rbOther.Margin = new System.Windows.Forms.Padding(4);
            this.rbOther.Name = "rbOther";
            this.rbOther.Size = new System.Drawing.Size(57, 21);
            this.rbOther.TabIndex = 1;
            this.rbOther.TabStop = true;
            this.rbOther.Text = "Otro";
            this.rbOther.UseVisualStyleBackColor = true;
            this.rbOther.CheckedChanged += new System.EventHandler(this.rbOther_CheckedChanged);
            // 
            // txtOtro
            // 
            this.txtOtro.Location = new System.Drawing.Point(16, 155);
            this.txtOtro.Name = "txtOtro";
            this.txtOtro.Size = new System.Drawing.Size(146, 22);
            this.txtOtro.TabIndex = 2;
            this.txtOtro.Visible = false;
            this.txtOtro.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtOtro_MouseClick);
            this.txtOtro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOtro_KeyPress);
            // 
            // Option_Payment
            // 
            this.AcceptButton = this.btnCobrar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(439, 225);
            this.Controls.Add(this.txtOtro);
            this.Controls.Add(this.rbOther);
            this.Controls.Add(this.rbOpcion3);
            this.Controls.Add(this.rbOpcion2);
            this.Controls.Add(this.rbOpcion1);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnCobrar);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Option_Payment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Seleccionar Opcion";
            this.Load += new System.EventHandler(this.Option_Payment_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCobrar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.RadioButton rbOpcion1;
        private System.Windows.Forms.RadioButton rbOpcion2;
        private System.Windows.Forms.RadioButton rbOpcion3;
        private System.Windows.Forms.RadioButton rbOther;
        private System.Windows.Forms.TextBox txtOtro;
    }
}