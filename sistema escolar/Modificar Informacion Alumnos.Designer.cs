﻿namespace Sistema_Escolar
{
    partial class Modificar_Informacion_Alumnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Modificar_Informacion_Alumnos));
            this.btnBuscar = new System.Windows.Forms.Button();
            this.lstBusqueda = new System.Windows.Forms.ListBox();
            this.txtBusqueda = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radBaja = new System.Windows.Forms.RadioButton();
            this.rbVer = new System.Windows.Forms.RadioButton();
            this.rbModificar = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtCURP = new System.Windows.Forms.TextBox();
            this.lblCURP = new System.Windows.Forms.Label();
            this.cmbCursoEscolar = new System.Windows.Forms.ComboBox();
            this.txtAlergias = new System.Windows.Forms.TextBox();
            this.txtProblemasNacer = new System.Windows.Forms.TextBox();
            this.txtFechaNacimiento = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.lblDirección = new System.Windows.Forms.Label();
            this.cmbEstadoCivil = new System.Windows.Forms.ComboBox();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.txtProfesion = new System.Windows.Forms.TextBox();
            this.txtEstadoCivil = new System.Windows.Forms.Label();
            this.lblcel = new System.Windows.Forms.Label();
            this.lblpr = new System.Windows.Forms.Label();
            this.txtApellidoP = new System.Windows.Forms.TextBox();
            this.lblapellido = new System.Windows.Forms.Label();
            this.txtNombreP = new System.Windows.Forms.TextBox();
            this.lblnombre = new System.Windows.Forms.Label();
            this.btnBaja = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBuscar
            // 
            this.btnBuscar.Enabled = false;
            this.btnBuscar.Location = new System.Drawing.Point(298, 118);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 11;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // lstBusqueda
            // 
            this.lstBusqueda.FormattingEnabled = true;
            this.lstBusqueda.Location = new System.Drawing.Point(12, 182);
            this.lstBusqueda.Name = "lstBusqueda";
            this.lstBusqueda.Size = new System.Drawing.Size(307, 199);
            this.lstBusqueda.TabIndex = 13;
            this.lstBusqueda.SelectedIndexChanged += new System.EventHandler(this.lstBuscar_SelectedIndexChanged);
            // 
            // txtBusqueda
            // 
            this.txtBusqueda.ForeColor = System.Drawing.Color.DarkGray;
            this.txtBusqueda.Location = new System.Drawing.Point(107, 92);
            this.txtBusqueda.Name = "txtBusqueda";
            this.txtBusqueda.Size = new System.Drawing.Size(266, 20);
            this.txtBusqueda.TabIndex = 10;
            this.txtBusqueda.Text = "Introduzca el Nombre o Apellido del alumno...";
            this.txtBusqueda.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtBusqueda_MouseClick);
            this.txtBusqueda.TextChanged += new System.EventHandler(this.txtBusqueda_TextChanged);
            this.txtBusqueda.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBusqueda_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 153);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Busqueda";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 95);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Digite El Nombre";
            // 
            // btnModificar
            // 
            this.btnModificar.Enabled = false;
            this.btnModificar.Location = new System.Drawing.Point(420, 418);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(154, 66);
            this.btnModificar.TabIndex = 14;
            this.btnModificar.Text = "Modificar Informacion";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(809, 418);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(154, 66);
            this.btnSalir.TabIndex = 14;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radBaja);
            this.groupBox1.Controls.Add(this.rbVer);
            this.groupBox1.Controls.Add(this.rbModificar);
            this.groupBox1.Location = new System.Drawing.Point(138, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(235, 42);
            this.groupBox1.TabIndex = 66;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informacion";
            // 
            // radBaja
            // 
            this.radBaja.AutoSize = true;
            this.radBaja.Location = new System.Drawing.Point(144, 16);
            this.radBaja.Name = "radBaja";
            this.radBaja.Size = new System.Drawing.Size(81, 17);
            this.radBaja.TabIndex = 2;
            this.radBaja.TabStop = true;
            this.radBaja.Text = "Dar de Baja";
            this.radBaja.UseVisualStyleBackColor = true;
            this.radBaja.CheckedChanged += new System.EventHandler(this.rbVer_CheckedChanged);
            // 
            // rbVer
            // 
            this.rbVer.AutoSize = true;
            this.rbVer.Checked = true;
            this.rbVer.Location = new System.Drawing.Point(15, 16);
            this.rbVer.Name = "rbVer";
            this.rbVer.Size = new System.Drawing.Size(41, 17);
            this.rbVer.TabIndex = 0;
            this.rbVer.TabStop = true;
            this.rbVer.Text = "Ver";
            this.rbVer.UseVisualStyleBackColor = true;
            this.rbVer.CheckedChanged += new System.EventHandler(this.rbVer_CheckedChanged);
            // 
            // rbModificar
            // 
            this.rbModificar.AutoSize = true;
            this.rbModificar.Location = new System.Drawing.Point(66, 16);
            this.rbModificar.Name = "rbModificar";
            this.rbModificar.Size = new System.Drawing.Size(68, 17);
            this.rbModificar.TabIndex = 1;
            this.rbModificar.Text = "Modificar";
            this.rbModificar.UseVisualStyleBackColor = true;
            this.rbModificar.CheckedChanged += new System.EventHandler(this.rbVer_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 45);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 13);
            this.label12.TabIndex = 65;
            this.label12.Text = "¿ Qué Desea Hacer?";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtCURP);
            this.groupBox2.Controls.Add(this.lblCURP);
            this.groupBox2.Controls.Add(this.cmbCursoEscolar);
            this.groupBox2.Controls.Add(this.txtAlergias);
            this.groupBox2.Controls.Add(this.txtProblemasNacer);
            this.groupBox2.Controls.Add(this.txtFechaNacimiento);
            this.groupBox2.Controls.Add(this.txtTelefono);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtApellido);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtNombre);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(420, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(313, 374);
            this.groupBox2.TabIndex = 69;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos del Alumno";
            // 
            // txtCURP
            // 
            this.txtCURP.Location = new System.Drawing.Point(67, 27);
            this.txtCURP.Name = "txtCURP";
            this.txtCURP.ReadOnly = true;
            this.txtCURP.Size = new System.Drawing.Size(238, 20);
            this.txtCURP.TabIndex = 66;
            // 
            // lblCURP
            // 
            this.lblCURP.AutoSize = true;
            this.lblCURP.Location = new System.Drawing.Point(21, 30);
            this.lblCURP.Name = "lblCURP";
            this.lblCURP.Size = new System.Drawing.Size(40, 13);
            this.lblCURP.TabIndex = 65;
            this.lblCURP.Text = "CURP:";
            // 
            // cmbCursoEscolar
            // 
            this.cmbCursoEscolar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCursoEscolar.Enabled = false;
            this.cmbCursoEscolar.FormattingEnabled = true;
            this.cmbCursoEscolar.Location = new System.Drawing.Point(67, 156);
            this.cmbCursoEscolar.Name = "cmbCursoEscolar";
            this.cmbCursoEscolar.Size = new System.Drawing.Size(238, 21);
            this.cmbCursoEscolar.TabIndex = 56;
            // 
            // txtAlergias
            // 
            this.txtAlergias.Location = new System.Drawing.Point(67, 329);
            this.txtAlergias.Name = "txtAlergias";
            this.txtAlergias.ReadOnly = true;
            this.txtAlergias.Size = new System.Drawing.Size(238, 20);
            this.txtAlergias.TabIndex = 64;
            // 
            // txtProblemasNacer
            // 
            this.txtProblemasNacer.Location = new System.Drawing.Point(67, 286);
            this.txtProblemasNacer.Name = "txtProblemasNacer";
            this.txtProblemasNacer.ReadOnly = true;
            this.txtProblemasNacer.Size = new System.Drawing.Size(238, 20);
            this.txtProblemasNacer.TabIndex = 62;
            // 
            // txtFechaNacimiento
            // 
            this.txtFechaNacimiento.Location = new System.Drawing.Point(67, 200);
            this.txtFechaNacimiento.Name = "txtFechaNacimiento";
            this.txtFechaNacimiento.ReadOnly = true;
            this.txtFechaNacimiento.Size = new System.Drawing.Size(238, 20);
            this.txtFechaNacimiento.TabIndex = 59;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(67, 243);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.ReadOnly = true;
            this.txtTelefono.Size = new System.Drawing.Size(238, 20);
            this.txtTelefono.TabIndex = 60;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(-1, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 55;
            this.label6.Text = "Curso Escolar";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 334);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 63;
            this.label5.Text = "Alergias";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(4, 293);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 35);
            this.label4.TabIndex = 61;
            this.label4.Text = "Problemas al Nacer";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(7, 199);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 36);
            this.label10.TabIndex = 57;
            this.label10.Text = "Fecha de Nacimiento";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 249);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 58;
            this.label3.Text = "Telefono";
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(67, 113);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.ReadOnly = true;
            this.txtApellido.Size = new System.Drawing.Size(238, 20);
            this.txtApellido.TabIndex = 54;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Apellido";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(67, 70);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(238, 20);
            this.txtNombre.TabIndex = 52;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "Nombre";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtDireccion);
            this.groupBox3.Controls.Add(this.lblDirección);
            this.groupBox3.Controls.Add(this.cmbEstadoCivil);
            this.groupBox3.Controls.Add(this.txtCelular);
            this.groupBox3.Controls.Add(this.txtProfesion);
            this.groupBox3.Controls.Add(this.txtEstadoCivil);
            this.groupBox3.Controls.Add(this.lblcel);
            this.groupBox3.Controls.Add(this.lblpr);
            this.groupBox3.Controls.Add(this.txtApellidoP);
            this.groupBox3.Controls.Add(this.lblapellido);
            this.groupBox3.Controls.Add(this.txtNombreP);
            this.groupBox3.Controls.Add(this.lblnombre);
            this.groupBox3.Location = new System.Drawing.Point(739, 27);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(313, 374);
            this.groupBox3.TabIndex = 70;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Datos del Papa";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(63, 143);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.ReadOnly = true;
            this.txtDireccion.Size = new System.Drawing.Size(215, 20);
            this.txtDireccion.TabIndex = 76;
            // 
            // lblDirección
            // 
            this.lblDirección.AutoSize = true;
            this.lblDirección.Location = new System.Drawing.Point(2, 146);
            this.lblDirección.Name = "lblDirección";
            this.lblDirección.Size = new System.Drawing.Size(55, 13);
            this.lblDirección.TabIndex = 75;
            this.lblDirección.Text = "Direccion:";
            // 
            // cmbEstadoCivil
            // 
            this.cmbEstadoCivil.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEstadoCivil.Enabled = false;
            this.cmbEstadoCivil.FormattingEnabled = true;
            this.cmbEstadoCivil.Location = new System.Drawing.Point(63, 201);
            this.cmbEstadoCivil.Name = "cmbEstadoCivil";
            this.cmbEstadoCivil.Size = new System.Drawing.Size(215, 21);
            this.cmbEstadoCivil.TabIndex = 70;
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(63, 318);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.ReadOnly = true;
            this.txtCelular.Size = new System.Drawing.Size(215, 20);
            this.txtCelular.TabIndex = 74;
            // 
            // txtProfesion
            // 
            this.txtProfesion.Location = new System.Drawing.Point(63, 260);
            this.txtProfesion.Name = "txtProfesion";
            this.txtProfesion.ReadOnly = true;
            this.txtProfesion.Size = new System.Drawing.Size(215, 20);
            this.txtProfesion.TabIndex = 72;
            // 
            // txtEstadoCivil
            // 
            this.txtEstadoCivil.Location = new System.Drawing.Point(6, 194);
            this.txtEstadoCivil.Name = "txtEstadoCivil";
            this.txtEstadoCivil.Size = new System.Drawing.Size(51, 28);
            this.txtEstadoCivil.TabIndex = 69;
            this.txtEstadoCivil.Text = "Estado Civil";
            // 
            // lblcel
            // 
            this.lblcel.AutoSize = true;
            this.lblcel.Location = new System.Drawing.Point(6, 328);
            this.lblcel.Name = "lblcel";
            this.lblcel.Size = new System.Drawing.Size(39, 13);
            this.lblcel.TabIndex = 73;
            this.lblcel.Text = "Celular";
            // 
            // lblpr
            // 
            this.lblpr.AutoSize = true;
            this.lblpr.Location = new System.Drawing.Point(6, 263);
            this.lblpr.Name = "lblpr";
            this.lblpr.Size = new System.Drawing.Size(51, 13);
            this.lblpr.TabIndex = 71;
            this.lblpr.Text = "Profesion";
            // 
            // txtApellidoP
            // 
            this.txtApellidoP.Location = new System.Drawing.Point(63, 85);
            this.txtApellidoP.Name = "txtApellidoP";
            this.txtApellidoP.ReadOnly = true;
            this.txtApellidoP.Size = new System.Drawing.Size(215, 20);
            this.txtApellidoP.TabIndex = 68;
            // 
            // lblapellido
            // 
            this.lblapellido.AutoSize = true;
            this.lblapellido.Location = new System.Drawing.Point(13, 88);
            this.lblapellido.Name = "lblapellido";
            this.lblapellido.Size = new System.Drawing.Size(44, 13);
            this.lblapellido.TabIndex = 67;
            this.lblapellido.Text = "Apellido";
            // 
            // txtNombreP
            // 
            this.txtNombreP.Location = new System.Drawing.Point(63, 27);
            this.txtNombreP.Name = "txtNombreP";
            this.txtNombreP.ReadOnly = true;
            this.txtNombreP.Size = new System.Drawing.Size(215, 20);
            this.txtNombreP.TabIndex = 66;
            // 
            // lblnombre
            // 
            this.lblnombre.AutoSize = true;
            this.lblnombre.Location = new System.Drawing.Point(13, 30);
            this.lblnombre.Name = "lblnombre";
            this.lblnombre.Size = new System.Drawing.Size(44, 13);
            this.lblnombre.TabIndex = 65;
            this.lblnombre.Text = "Nombre";
            // 
            // btnBaja
            // 
            this.btnBaja.Enabled = false;
            this.btnBaja.Location = new System.Drawing.Point(579, 418);
            this.btnBaja.Name = "btnBaja";
            this.btnBaja.Size = new System.Drawing.Size(154, 66);
            this.btnBaja.TabIndex = 71;
            this.btnBaja.Text = "Dar de Baja";
            this.btnBaja.UseVisualStyleBackColor = true;
            this.btnBaja.Click += new System.EventHandler(this.btnBaja_Click);
            // 
            // Modificar_Informacion_Alumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 496);
            this.Controls.Add(this.btnBaja);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.lstBusqueda);
            this.Controls.Add(this.txtBusqueda);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Modificar_Informacion_Alumnos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modificar Informacion Alumnos";
            this.Load += new System.EventHandler(this.Modificar_Informacion_Alumnos_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.ListBox lstBusqueda;
        private System.Windows.Forms.TextBox txtBusqueda;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbVer;
        private System.Windows.Forms.RadioButton rbModificar;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbCursoEscolar;
        private System.Windows.Forms.TextBox txtAlergias;
        private System.Windows.Forms.TextBox txtProblemasNacer;
        private System.Windows.Forms.TextBox txtFechaNacimiento;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cmbEstadoCivil;
        private System.Windows.Forms.TextBox txtCelular;
        private System.Windows.Forms.TextBox txtProfesion;
        private System.Windows.Forms.Label txtEstadoCivil;
        private System.Windows.Forms.Label lblcel;
        private System.Windows.Forms.Label lblpr;
        private System.Windows.Forms.TextBox txtApellidoP;
        private System.Windows.Forms.Label lblapellido;
        private System.Windows.Forms.TextBox txtNombreP;
        private System.Windows.Forms.Label lblnombre;
        private System.Windows.Forms.Button btnBaja;
        private System.Windows.Forms.RadioButton radBaja;
        private System.Windows.Forms.TextBox txtCURP;
        private System.Windows.Forms.Label lblCURP;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label lblDirección;
    }
}