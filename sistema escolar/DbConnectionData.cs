﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Devart.Data.PostgreSql;

using System.IO;
using iTextSharp.text;
using itextsharp.pdfa;
using iTextSharp.text.pdf;

namespace MyDbConnection
{
    class Usuarios
    {
        public int Id { get; private set; }
        public string Usuario { get; private set; }
        public string Contrasena { get; private set; }

        public Usuarios(int id, string usuario, string contrasena)
        {
            Id = id;
            Usuario = usuario;
            Contrasena = contrasena;
        }
    }
    class Costos
    {
        public int Id { get; private set; }
        public int IdCurso { get; private set; }
        public double Seguro { get; private set; }
        public double Inscripcion { get; private set; }
        public double Libros { get; private set; }
        public double Material { get; private set; }
        public double  Primer { get; private set; }
        public double Segundo { get; private set; }
        public double Tercero { get; private set; }

        public Costos(int id, int idcurso, double seguro, double inscripcion, double libros, double material, double primer, double segundo, double tercero)
        {
            Id = id;
            IdCurso = idcurso;
            Seguro = seguro;
            Inscripcion = inscripcion;
            Libros = libros;
            Material = material;
            Primer = primer;
            Segundo = segundo;
            Tercero = tercero;
        }
    }
    class Ingresos
    {
        public int Id { get; private set; }
        public string Fecha { get; private set; }
        public string Descripcion { get; private set; }
        public double Total { get; private set; }
        public string DescripcionTotal
        {
            get
            {
                return string.Format("{0} | $ {1}", Descripcion, Total);
            }
        }
        public Ingresos(int id, string fecha, string descripcion, double total)
        {
            Id = id;
            Fecha = fecha;
            Descripcion = descripcion;
            Total = total;
        }
    }
    class Egresos
    {
        public int Id { get; private set; }
        public string Fecha { get; private set; }
        public string Descripcion { get; private set; }
        public double Total { get; private set; }
        public string DescripcionTotal
        {
            get
            {
                return string.Format("{0} | $ {1}", Descripcion, Total);
            }
        }
        public Egresos(int id, string fecha, string descripcion, double total)
        {
            Id = id;
            Fecha = fecha;
            Descripcion = descripcion;
            Total = total;
        }
    }
    class Mensualidades
    {
        public int Idalumno { get; private set; }
        public bool Agosto { get; private set; }
        public bool Septiembre { get; private set; }
        public bool Octubre { get; private set; }
        public bool Noviembre { get; private set; }
        public bool Diciembre { get; private set; }
        public bool Enero { get; private set; }
        public bool Febrero { get; private set; }
        public bool Marzo { get; private set; }
        public bool Abril { get; private set; }
        public bool Mayo { get; private set; }
        public bool Junio { get; private set; }
        public bool Julio { get; private set; }
        public bool Seguro { get; private set; }
        public bool Material { get; private set; }
        public bool Libros { get; private set; }
        public bool Inscripcion { get; private set; }
        public Mensualidades(int idalumno, bool agosto, bool septiembre, bool octubre, bool noviembre, bool diciembre, bool enero, bool febrero, bool marzo, bool abril, bool mayo, bool junio, bool julio, bool seguro, bool libros, bool material, bool inscripcion)
        {
            idalumno = Idalumno;
            Agosto = agosto;
            Septiembre = septiembre;
            Octubre = octubre;
            Noviembre = noviembre;
            Diciembre = diciembre;
            Enero = enero;
            Febrero = febrero;
            Marzo = marzo;
            Abril = abril;
            Mayo = mayo;
            Junio = junio;
            Julio = julio;
            Seguro = seguro;
            Material = material;
            Libros = libros;
            Inscripcion = inscripcion;
        }
    }
    public class Alumno
    {
        public int Id { get; private set; }
        public int IdPapa { get; private set; }
        public string IdCurso { get; private set; }
        public string FechaNacimiento { get; private set; }
        public string CURP { get; private set; }
        public string Nombre { get; private set; }
        public string Apellido { get; private set; }
        public string NombreCompleto
        {
            get
            {
                return string.Format("{0}, {1}", Apellido, Nombre);
            }
        }
        public string DatosBusqueda
        {
            get
            {
                return string.Format("{0}   |   {1}", NombreCompleto, IdCurso);
            }
        }
        public string Telefono { get; private set; }
        public string ProblemasNacer { get; private set; }
        public string Alergias { get; private set; }
        public Alumno(int id, int idpapa, string idcurso, string fechanacimiento, string curp, string nombre, string apellido, string telefono, string problemasnacer, string alergias)
        {
            Id = id;
            IdPapa = idpapa;
            IdCurso = idcurso;
            CURP = curp;
            FechaNacimiento = fechanacimiento;
            Nombre = nombre;
            Apellido = apellido;
            Telefono = telefono;
            ProblemasNacer = problemasnacer;
            Alergias = alergias;
        }
        public string Mes(int mes)
        {
            if (mes==1)
            {
                return "Enero";
            }
            else if(mes==2)
            {
                return "Febrero";
            }
            else if (mes == 3)
            {
                return "Marzo";
            }
            else if (mes == 4)
            {
                return "Abril";
            }
            else if (mes == 5)
            {
                return "Mayo";
            }
            else if (mes == 6)
            {
                return "Junio";
            }
            else if (mes == 7)
            {
                return "Julio";
            }
            else if (mes == 8)
            {
                return "Agosto";
            }
            else if (mes == 9)
            {
                return "Septiembre";
            }
            else if (mes == 10)
            {
                return "Octubre";
            }
            else if (mes == 11)
            {
                return "Noviembre";
            }
            else
            {
                return "Diciembre";
            }
        }

    }
    public class Papa
    {
        public int Id { get; private set; }
        public string Nombre { get; private set; }
        public string Apellido { get; private set; }
        public string Direccion { get; private set; }
        public string Celular { get; private set; }
        public string EstadoCivil { get; private set; }
        public string Profesion { get; private set; }
        public string NombreCompleto
        {
            get
            {
                return string.Format("{0}, {1}", Apellido, Nombre);
            }
        }
        public string DatosBusqueda
        {
            get
            {
                return string.Format("{0}   |   {1}", NombreCompleto, Celular);
            }
        }
        public Papa (int id, string nombre, string apellido, string direccion, string celular , string estadocivil, string profesion)
        {
            Id = id;
            Nombre = nombre;
            Apellido = apellido;
            Direccion = direccion;
            Celular = celular;
            EstadoCivil = estadocivil;
            Profesion = profesion;
        }

    }
    class EstadoCivil
    {
        public int Id { get; private set; }
        public string Descripcion { get; private set; }

        public EstadoCivil(int id, string descripcion)
        {
            Id = id;
            Descripcion = descripcion;
        }
    }
    class Curso
    {
        public int Id { get; private set; }
        public string Descripcion { get; private set; }
        
        public Curso (int id, string descripcion)
        {
            Id=id;
            Descripcion=descripcion;
        }
    }
    class NivelEstudios
    {
        public int Id { get; private set; }
        public string Descripcion { get; private set; }
        
        public NivelEstudios (int id, string descripcion)
        {
            Id=id;
            Descripcion=descripcion;
        }
    }
    class NivelEstudiosPersonal
    {
        public int Id { get; private set; }
        public string Descripcion { get; private set; }

        public NivelEstudiosPersonal(int id, string descripcion)
        {
            Id = id;
            Descripcion = descripcion;
        }
    }
    class Personal
    {
        public int Id { get; private set; }
        public string Categoria { get; private set; }
        public string FechaNacimiento { get; private set; }
        public string Nombre { get; private set; }
        public string Apellido { get; private set; }
        public string NombreCompleto
        {
            get
            {
                return string.Format("{0} {1}", Nombre, Apellido);
            }
        }
        public string Celular { get; private set; }
        public string Civil { get; private set; }
        public string NivelEstudios { get; private set; }
        public decimal Sueldo { get; private set;}
        public Personal(int id, string categoria, string fechanacimiento, string nombre, string apellido, string celular, string civil, string nivelestudios, decimal sueldo)
        {
            Id = id;
            Categoria = categoria;
            FechaNacimiento = fechanacimiento;
            Nombre = nombre;
            Apellido = apellido;
            Celular = celular;
            Civil = civil;
            NivelEstudios = nivelestudios;
            Sueldo = sueldo;
        }
    }
    class CategoriaPersonal
    {
        public int Id { get; private set; }
        public string Descripcion { get; private set; }
        
        public CategoriaPersonal (int id, string descripcion)
        {
            Id=id;
            Descripcion=descripcion;
        }
    }

    partial class DbConnection
    {
        //Usuarios

        public void ReiniciarAño()
        {
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText="update mensualidades set agosto=true, septiembre=true, octubre=true, noviembre=true, diciembre=true, enero=true, febrero=true, marzo=true, abril=true, mayo=true, junio=true, julio=true ,seguro=true, material=true, libros=true, inscripcion=true" ;
                cmd.ExecuteNonQuery();
            }
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "delete From ingresos";
                cmd.ExecuteNonQuery();
            }
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "delete From egresos";
                cmd.ExecuteNonQuery();
            }
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Update alumnos set idcurso=idcurso+1 where idcurso<9";
                cmd.ExecuteNonQuery();
            }
        }
        public List<Usuarios> ObtenerUsuarios()
        {
            List<Usuarios> usuario = new List<Usuarios>();

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select * From Usuarios";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        usuario.Add(new Usuarios(dr.GetInt32("id"), dr.GetString("usuario"),dr.GetString("contrasena")));
                    }

                    dr.Close();
                }
            }
            return usuario;
        }
        public void ActualizarChekbox(bool inscripcion, bool seguro, bool libros, bool material, int idalumno)
        {
            if (inscripcion == false)
            {
                inscripcion = true;
            }
            
            else if (seguro == false)
            {
                seguro = true;
            }
            
            else if (material == false)
            {
                material = true;
            }
            
            else if (libros == false)
            {
                libros = true;
            }

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = string.Format("UPDATE mensualidades set inscripcion={0}, libros={1}, material={2}, seguro={3}  where idalumno={4}", inscripcion, libros, material, seguro, idalumno);
                cmd.ExecuteNonQuery();
            }
        }
        //Deudas
        public List<Alumno> AlumnosQueDebenMes(string mes)
        {
            mes = mes.Trim().ToUpper();
            List<Alumno> alumno = new List<Alumno>();

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select al.id, al.idpapa, cu.descripcion, " +
                                    "al.fnacimiento, al.curp, al.nombre, al.apellido, " +
                                    "al.telefono, al.pnacer, al.alergias "+
                                    "From alumnos al "+
                                    "inner join mensualidades m on al.id=m.idalumno "+
                                    "inner join curso cu on cu.id= al.idcurso "+
                                    "where m."+mes+"=true    order by al.idcurso;";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        alumno.Add(new Alumno(dr.GetInt32("id"),
                                                 dr.GetInt32("idpapa"),
                                                 dr.GetString("descripcion"),
                                                 dr.GetString("fnacimiento"),
                                                 dr.GetString("curp"),
                                                 dr.GetString("nombre"),
                                                 dr.GetString("apellido"),
                                                 dr.GetString("Telefono"),
                                                 dr.GetString("pnacer"),
                                                 dr.GetString("alergias")));

                    }

                    dr.Close();
                }
            }
            return alumno;
        }
        //Fin Deudas
        //Tablas de Ingresos y Egresos
        public List<Ingresos> ObtenerIngresosPorDia(string fecha)
        {
            List<Ingresos> ingreso = new List<Ingresos>();
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select * From ingresos where fecha='" + fecha + "' order by fecha";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        ingreso.Add(new Ingresos(dr.GetInt32("id"), dr.GetString("fecha"), dr.GetString("descripcion"), dr.GetDouble("total")));
                    }

                    dr.Close();
                }
            }
            return ingreso;
        }
        public List<Ingresos> ObtenerIngresosPorAño()
        {
            List<Ingresos> ingreso = new List<Ingresos>();
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select * From ingresos order by fecha";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        ingreso.Add(new Ingresos(dr.GetInt32("id"), dr.GetString("fecha"), dr.GetString("descripcion"), dr.GetDouble("total")));
                    }

                    dr.Close();
                }
            }
            return ingreso;
        }
        public List<Ingresos> ObtenerIngresosPorMes(int mes, int uDmes, int year)
        {
            List<Ingresos> ingreso = new List<Ingresos>();
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = string.Format("select*from ingresos where fecha >='{2}/{0}/1' and fecha <='{2}/{0}/{1}' order by fecha", mes, uDmes, year);

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        ingreso.Add(new Ingresos(dr.GetInt32("id"), dr.GetString("fecha"), dr.GetString("descripcion"), dr.GetDouble("total")));
                    }

                    dr.Close();
                }
            }
            return ingreso;
        }
        public List<Egresos> ObtenerEgresosPorDia(string fecha)
        {
            List<Egresos> egreso = new List<Egresos>();
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select * From egresos where fecha='" + fecha + "' order by fecha";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        egreso.Add(new Egresos(dr.GetInt32("id"), dr.GetString("fecha"), dr.GetString("descripcion"), dr.GetDouble("total")));
                    }

                    dr.Close();
                }
            }
            return egreso;
        }
        public List<Egresos> ObtenerEgresosPorAño()
        {
            List<Egresos> egreso = new List<Egresos>();
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select * From egresos order by fecha";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        egreso.Add(new Egresos(dr.GetInt32("id"), dr.GetString("fecha"), dr.GetString("descripcion"), dr.GetDouble("total")));
                    }

                    dr.Close();
                }
            }
            return egreso;
        }
        public List<Egresos> ObtenerEgresosPorMes(int mes, int uDmes, int year)
        {
            List<Egresos> egreso = new List<Egresos>();
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = string.Format("select*from egresos where fecha >='{2}/{0}/1' and fecha <='{2}/{0}/{1}' ORDER BY fecha", mes, uDmes,year);

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        egreso.Add(new Egresos(dr.GetInt32("id"), dr.GetString("fecha"), dr.GetString("descripcion"), dr.GetDouble("total")));
                    }

                    dr.Close();
                }
            }
            return egreso;
        }
        //Fin Tablas de Ingresos

        //CURSOS----------------------------------------------------------------
        public int ObtenerIdCurso(int Idalumno)
        {
            int id;

            GetInt32(string.Format("Select idcurso from alumnos where id={0}", Idalumno), "idcurso", out id);
            return id;
        }
        public void ActualizarPreciosCurso(int idcurso, double seguro, double inscripcion, double libros, double material, double primer, double segundo, double tercero)
        {
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = string.Format("UPDATE costos set seguro={0},  insc={1}, libros={2}, material={3}, primer={4}, segundo={5}" +
                                                 ", tercero={6}  Where idcurso={7}", seguro, inscripcion, libros, material, primer, segundo, tercero, idcurso);
                cmd.ExecuteNonQuery();
            }

        }
        public List<Curso> ObtenerNivelesDeCurso()
        {
            List<Curso> curso = new List<Curso>();

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select * From Curso";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        curso.Add(new Curso(dr.GetInt32("id"), dr.GetString("descripcion")));
                    }

                    dr.Close();
                }
            }
            return curso;
        }
        //Fin Cursos 


        //Generar Pdf
        public void GeneraPdfIngresos(double Total, string Fecha, string Concepto, string ruta)
        {
            string nombre = string.Format(@"{0}", ruta);

            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 72, 72, 72, 86);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(nombre, FileMode.OpenOrCreate));
            doc.Open();

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance("patriaunida.jpg");
            jpg.ScalePercent(10f);
            jpg.Alignment = Image.ALIGN_RIGHT;
            jpg.Alignment = Image.TEXTWRAP;
            doc.Add(jpg);
            
            Paragraph parrafo1 = new Paragraph("      \t          Centro Educativo Patria Unida", FontFactory.GetFont("Arial", 16, Font.BOLDITALIC));

            doc.Add(parrafo1);
            Paragraph parrafo2 = new Paragraph(string.Format("Bueno por: ${0}  M.N", Total), FontFactory.GetFont("Arial", 14, Font.ITALIC));
            parrafo2.Alignment = Element.ALIGN_RIGHT;
            doc.Add(parrafo2);
            Paragraph parrafo3 = new Paragraph(string.Format("\nRECIBO por la cantidad de: ${0}  00/100 M.N  por concepto de {1} ." ,Total, Concepto), FontFactory.GetFont("Arial", 16, Font.NORMAL));
            doc.Add(parrafo3);
            Paragraph parrafo4 = new Paragraph(string.Format("\nMérida, Yucatán a {0}", Fecha), FontFactory.GetFont("Arial", 12, Font.BOLD));
            doc.Add(parrafo4);
            Paragraph parrafo5 = new Paragraph("\n\n\n___________________________                    ____________________________");
            doc.Add(parrafo5);
            Paragraph parrafo6 = new Paragraph("            Firma del que Recibe                                               Sello y/o Firma de la Escuela", FontFactory.GetFont("Arial", 10, Font.BOLD));
            doc.Add(parrafo6);

            Paragraph parrafo7 = new Paragraph("\n-------------------------------------------------------------------------------------------------------------------\n\n\n\n");
            doc.Add(parrafo7);
            iTextSharp.text.Image jpg2 = iTextSharp.text.Image.GetInstance("patriaunida.jpg");
            jpg2.ScalePercent(10f);
            jpg2.Alignment = Image.ALIGN_RIGHT;
            jpg2.Alignment = Image.TEXTWRAP;
            doc.Add(jpg2);
            Paragraph parrafo9 = new Paragraph("    \t          Centro Educativo Patria Unida", FontFactory.GetFont("Arial", 16, Font.BOLDITALIC));
            doc.Add(parrafo9);
            Paragraph parrafo10 = new Paragraph(string.Format("Bueno por: ${0}  M.N", Total), FontFactory.GetFont("Arial", 14, Font.ITALIC));
            parrafo10.Alignment = Element.ALIGN_RIGHT;
            doc.Add(parrafo10);
            Paragraph parrafo11 = new Paragraph(string.Format("\n\nRECIBO por la cantidad de: ${0}  00/100 M.N  por concepto de {1} .", Total, Concepto), FontFactory.GetFont("Arial", 16, Font.NORMAL));
            doc.Add(parrafo11);
            Paragraph parrafo12 = new Paragraph(string.Format("\nMérida, Yucatán a {0}", Fecha), FontFactory.GetFont("Arial", 12, Font.BOLD));
            doc.Add(parrafo12);
            Paragraph parrafo13 = new Paragraph("\n\n\n___________________________                    ____________________________");
            doc.Add(parrafo13);
            Paragraph parrafo14 = new Paragraph("            Firma del que Recibe                                               Sello y/o Firma de la Escuela", FontFactory.GetFont("Arial", 10, Font.BOLD));
            doc.Add(parrafo14);




            doc.Close();

        }
        public void GenerarPdfEgresos(double Total, string Fecha, string Concepto, string ruta)
        {
            string nombre = string.Format(@"{0}", ruta);

            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 72, 72, 72, 86);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(nombre, FileMode.OpenOrCreate));
            doc.Open();

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance("patriaunida.jpg");
            jpg.ScalePercent(10f);
            jpg.Alignment = Image.ALIGN_RIGHT;
            jpg.Alignment = Image.TEXTWRAP;
            doc.Add(jpg);

            Paragraph parrafo1 = new Paragraph("      \t          Centro Educativo Patria Unida", FontFactory.GetFont("Arial", 16, Font.BOLDITALIC));

            doc.Add(parrafo1);
            Paragraph parrafo2 = new Paragraph(string.Format("Bueno por: ${0}  M.N", Total), FontFactory.GetFont("Arial", 14, Font.ITALIC));
            parrafo2.Alignment = Element.ALIGN_RIGHT;
            doc.Add(parrafo2);
            Paragraph parrafo3 = new Paragraph(string.Format("\nRECIBO por la cantidad de: ${0}  00/100 M.N  por concepto de {1} .", Total, Concepto), FontFactory.GetFont("Arial", 16, Font.NORMAL));
            doc.Add(parrafo3);
            Paragraph parrafo4 = new Paragraph(string.Format("\nMérida, Yucatán a {0}", Fecha), FontFactory.GetFont("Arial", 12, Font.BOLD));
            doc.Add(parrafo4);
            Paragraph parrafo5 = new Paragraph("\n\n\n___________________________                    ____________________________");
            doc.Add(parrafo5);
            Paragraph parrafo6 = new Paragraph("            Firma del que Recibe                                               Sello y/o Firma de la Escuela", FontFactory.GetFont("Arial", 10, Font.BOLD));
            doc.Add(parrafo6);

            Paragraph parrafo7 = new Paragraph("\n-------------------------------------------------------------------------------------------------------------------\n\n\n\n");
            doc.Add(parrafo7);
            iTextSharp.text.Image jpg2 = iTextSharp.text.Image.GetInstance("patriaunida.jpg");
            jpg2.ScalePercent(10f);
            jpg2.Alignment = Image.ALIGN_RIGHT;
            jpg2.Alignment = Image.TEXTWRAP;
            doc.Add(jpg2);
            Paragraph parrafo9 = new Paragraph("    \t          Centro Educativo Patria Unida", FontFactory.GetFont("Arial", 16, Font.BOLDITALIC));
            doc.Add(parrafo9);
            Paragraph parrafo10 = new Paragraph(string.Format("Bueno por: ${0}  M.N", Total), FontFactory.GetFont("Arial", 14, Font.ITALIC));
            parrafo10.Alignment = Element.ALIGN_RIGHT;
            doc.Add(parrafo10);
            Paragraph parrafo11 = new Paragraph(string.Format("\n\nRECIBO por la cantidad de: ${0}  00/100 M.N  por concepto de {1} .", Total, Concepto), FontFactory.GetFont("Arial", 16, Font.NORMAL));
            doc.Add(parrafo11);
            Paragraph parrafo12 = new Paragraph(string.Format("\nMérida, Yucatán a {0}", Fecha), FontFactory.GetFont("Arial", 12, Font.BOLD));
            doc.Add(parrafo12);
            Paragraph parrafo13 = new Paragraph("\n\n\n___________________________                    ____________________________");
            doc.Add(parrafo13);
            Paragraph parrafo14 = new Paragraph("            Firma del que Recibe                                               Sello y/o Firma de la Escuela", FontFactory.GetFont("Arial", 10, Font.BOLD));
            doc.Add(parrafo14);
            doc.Close();

        }
        public void GenerarNotaMes(string Folio, double Total, string NPapa, string Mes, int año, string NAlumno, string Curso, string Fecha, string ruta)
        {
            string nombre = string.Format(@"{0}\{1}.pdf", ruta, Folio);

            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 72, 72, 72, 86);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(nombre, FileMode.OpenOrCreate));
            doc.Open();

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance("patriaunida.jpg");
            jpg.ScalePercent(10f);
            jpg.Alignment = Image.ALIGN_RIGHT;
            jpg.Alignment = Image.TEXTWRAP;
            doc.Add(jpg);
            Paragraph parrafo0 = new Paragraph(string.Format("Folio:{0}", Folio), FontFactory.GetFont("Arial", 10, Font.NORMAL));

            parrafo0.Alignment = Element.ALIGN_RIGHT;
            doc.Add(parrafo0);

            Paragraph parrafo1 = new Paragraph("      \t          Centro Educativo Patria Unida", FontFactory.GetFont("Arial", 16, Font.BOLDITALIC));

            doc.Add(parrafo1);
            Paragraph parrafo2 = new Paragraph(string.Format("Bueno por: ${0}  M.N", Total), FontFactory.GetFont("Arial", 14, Font.ITALIC));
            parrafo2.Alignment = Element.ALIGN_RIGHT;
            doc.Add(parrafo2);
            Paragraph parrafo3 = new Paragraph(string.Format("\nRECIBÍ del Sr(a). {0}, la cantidad de: ${1}  00/100 M.N  por concepto de la colegiatura del mes de {2} del {3} del alumno {4} que cursa actualmente {5}.", NPapa, Total, Mes, año, NAlumno, Curso), FontFactory.GetFont("Arial", 14, Font.NORMAL));
            doc.Add(parrafo3);
            Paragraph parrafo4 = new Paragraph(string.Format("\nMérida, Yucatán a {0}", Fecha), FontFactory.GetFont("Arial", 12, Font.BOLD));
            doc.Add(parrafo4);
            Paragraph parrafo5 = new Paragraph("\n\n\n___________________________                    ____________________________");
            doc.Add(parrafo5);
            Paragraph parrafo6 = new Paragraph("            Firma del padre o tutor                                               Sello y/o Firma de la Escuela", FontFactory.GetFont("Arial", 10, Font.BOLD));
            doc.Add(parrafo6);

            Paragraph parrafo7 = new Paragraph("\n-------------------------------------------------------------------------------------------------------------------\n\n");
            doc.Add(parrafo7);
            iTextSharp.text.Image jpg2 = iTextSharp.text.Image.GetInstance("patriaunida.jpg");
            jpg2.ScalePercent(10f);
            jpg2.Alignment = Image.ALIGN_RIGHT;
            jpg2.Alignment = Image.TEXTWRAP;
            doc.Add(jpg2);
            Paragraph parrafo8 = new Paragraph(string.Format("Folio:{0}", Folio), FontFactory.GetFont("Arial", 10, Font.NORMAL));
            parrafo8.Alignment = Element.ALIGN_RIGHT;
            doc.Add(parrafo8);
            Paragraph parrafo9 = new Paragraph("    \t          Centro Educativo Patria Unida", FontFactory.GetFont("Arial", 16, Font.BOLDITALIC));
            doc.Add(parrafo9);
            Paragraph parrafo10 = new Paragraph(string.Format("Bueno por: ${0}  M.N", Total), FontFactory.GetFont("Arial", 14, Font.ITALIC));
            parrafo10.Alignment = Element.ALIGN_RIGHT;
            doc.Add(parrafo10);
            Paragraph parrafo11 = new Paragraph(string.Format("\nRECIBÍ del Sr(a). {0}, la cantidad de: ${1}  00/100 M.N  por concepto de la colegiatura del mes de {2} del {3} del alumno {4} que cursa actualmente {5}.", NPapa, Total, Mes, año, NAlumno, Curso), FontFactory.GetFont("Arial", 14, Font.NORMAL));
            doc.Add(parrafo11);
            Paragraph parrafo12 = new Paragraph(string.Format("\nMérida, Yucatán a {0}", Fecha), FontFactory.GetFont("Arial", 12, Font.BOLD));
            doc.Add(parrafo12);
            Paragraph parrafo13 = new Paragraph("\n\n\n___________________________                    ____________________________");
            doc.Add(parrafo13);
            Paragraph parrafo14 = new Paragraph("            Firma del padre o tutor                                               Sello y/o Firma de la Escuela", FontFactory.GetFont("Arial", 10, Font.BOLD));
            doc.Add(parrafo14);




            doc.Close();

        }
        public void GenerarCheckBox(string Folio, double Total, string NPapa, string NAlumno, string Curso, string Fecha, string Concepto, string ruta)
        {
            string nombre = string.Format(@"{0}", ruta);

            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 72, 72, 72, 86);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(nombre, FileMode.OpenOrCreate));
            doc.Open();

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance("patriaunida.jpg");
            jpg.ScalePercent(10f);
            jpg.Alignment = Image.ALIGN_RIGHT;
            jpg.Alignment = Image.TEXTWRAP;
            doc.Add(jpg);
            Paragraph parrafo0 = new Paragraph(string.Format("Folio:{0}", Folio), FontFactory.GetFont("Arial", 10, Font.NORMAL));

            parrafo0.Alignment = Element.ALIGN_RIGHT;
            doc.Add(parrafo0);

            Paragraph parrafo1 = new Paragraph("      \t          Centro Educativo Patria Unida", FontFactory.GetFont("Arial", 16, Font.BOLDITALIC));

            doc.Add(parrafo1);
            Paragraph parrafo2 = new Paragraph(string.Format("Bueno por: ${0}  M.N", Total), FontFactory.GetFont("Arial", 14, Font.ITALIC));
            parrafo2.Alignment = Element.ALIGN_RIGHT;
            doc.Add(parrafo2);
            Paragraph parrafo3 = new Paragraph(string.Format("\nRECIBÍ del Sr(a). {0}, la cantidad de: ${1}  00/100 M.N  por concepto de {2} del alumno {3} que cursa actualmente {4}.", NPapa, Total, Concepto, NAlumno, Curso), FontFactory.GetFont("Arial", 14, Font.NORMAL));
            doc.Add(parrafo3);
            Paragraph parrafo4 = new Paragraph(string.Format("\nMérida, Yucatán a {0}", Fecha), FontFactory.GetFont("Arial", 12, Font.BOLD));
            doc.Add(parrafo4);
            Paragraph parrafo5 = new Paragraph("\n\n\n___________________________                    ____________________________");
            doc.Add(parrafo5);
            Paragraph parrafo6 = new Paragraph("            Firma del padre o tutor                                               Sello y/o Firma de la Escuela", FontFactory.GetFont("Arial", 10, Font.BOLD));
            doc.Add(parrafo6);

            Paragraph parrafo7 = new Paragraph("\n-------------------------------------------------------------------------------------------------------------------\n\n");
            doc.Add(parrafo7);
            iTextSharp.text.Image jpg2 = iTextSharp.text.Image.GetInstance("patriaunida.jpg");
            jpg2.ScalePercent(10f);
            jpg2.Alignment = Image.ALIGN_RIGHT;
            jpg2.Alignment = Image.TEXTWRAP;
            doc.Add(jpg2);
            Paragraph parrafo8 = new Paragraph(string.Format("Folio:{0}", Folio), FontFactory.GetFont("Arial", 10, Font.NORMAL));
            parrafo8.Alignment = Element.ALIGN_RIGHT;
            doc.Add(parrafo8);
            Paragraph parrafo9 = new Paragraph("    \t          Centro Educativo Patria Unida", FontFactory.GetFont("Arial", 16, Font.BOLDITALIC));
            doc.Add(parrafo9);
            Paragraph parrafo10 = new Paragraph(string.Format("Bueno por: ${0}  M.N", Total), FontFactory.GetFont("Arial", 14, Font.ITALIC));
            parrafo10.Alignment = Element.ALIGN_RIGHT;
            doc.Add(parrafo10);
            Paragraph parrafo11 = new Paragraph(string.Format("\nRECIBÍ del Sr(a). {0}, la cantidad de: ${1}  00/100 M.N  por concepto de {2} del alumno {3} que cursa actualmente {4}.", NPapa, Total, Concepto, NAlumno, Curso), FontFactory.GetFont("Arial", 14, Font.NORMAL));
            doc.Add(parrafo11);
            Paragraph parrafo12 = new Paragraph(string.Format("\nMérida, Yucatán a {0}", Fecha), FontFactory.GetFont("Arial", 12, Font.BOLD));
            doc.Add(parrafo12);
            Paragraph parrafo13 = new Paragraph("\n\n\n___________________________                    ____________________________");
            doc.Add(parrafo13);
            Paragraph parrafo14 = new Paragraph("            Firma del padre o tutor                                               Sello y/o Firma de la Escuela", FontFactory.GetFont("Arial", 10, Font.BOLD));
            doc.Add(parrafo14);
            doc.Close();

        }
        // Fin Generar Pds
       
        //Costos
        public void AgregarIngresos(string fecha, string descripcion, double total)
        {
            int id;
            if (GetInt32("Select id from ingresos", "id", out id) == false)
            {
                using (PgSqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("INSERT INTO ingresos(id,fecha, descripcion,total) " +
                                                    "Values (0,'{0}', '{1}',{2})",
                                                    fecha, descripcion, total);

                    cmd.ExecuteNonQuery();
                }
            }
            else
            {
                using (PgSqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("INSERT INTO ingresos(id,fecha, descripcion,total) " +
                                                    "SELECT MAX(id)+1,'{0}', '{1}',{2} from ingresos",
                                                    fecha, descripcion, total);

                    cmd.ExecuteNonQuery();
                }
            }
        }
        public void AgregarEgresos(string fecha, string descripcion, double total)
        {
            int id;
            if (GetInt32("Select id from Egresos", "id", out id) == false)
            {
                using (PgSqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("INSERT INTO Egresos(id,fecha, descripcion,total) " +
                                                    "Values ( 0,'{0}', '{1}',{2} )",
                                                    fecha, descripcion, total);

                    cmd.ExecuteNonQuery();
                }
            }
            else
            {
                using (PgSqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("INSERT INTO Egresos(id,fecha, descripcion,total) " +
                                                    "SELECT MAX(id)+1,'{0}', '{1}',{2} from egresos",
                                                    fecha, descripcion, total);

                    cmd.ExecuteNonQuery();
                }
            }
        }
        public double ObtenerTotal(int idcurso, string adeudo)
        {
            double total;

            GetDouble(string.Format("Select {0} from costos where idcurso={1};", adeudo, idcurso), string.Format("{0}", adeudo), out total);
            return total;
        }
        public Costos ObtenerCostos(int idCurso)
        {
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "SELECT *From costos Where idcurso=" + idCurso;


                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        Costos costo = new Costos(dr.GetInt32("id"),
                                                 dr.GetInt32("idcurso"),
                                                 dr.GetDouble("seguro"),
                                                 dr.GetDouble("insc"),
                                                 dr.GetDouble("libros"),
                                                 dr.GetDouble("material"),
                                                 dr.GetDouble("primer"),
                                                 dr.GetDouble("segundo"),
                                                 dr.GetDouble("tercero"));
                        return costo;

                    }

                    dr.Close();
                }
            }
            return null;
        }
        public double ObtenerTotalIngresosDia(string fecha)
        {
            double total;

            GetDouble(string.Format("Select sum (total) as totaldia  from ingresos where fecha='{0}'", fecha), "totaldia", out total);
            return total;
            
        }

        public double ObtenerTotalIngresos()
        {
            double total;

            GetDouble("Select sum (total) as totalcurso  from ingresos", "totalcurso", out total);
            return total;

        }
        public double ObtenerTotalEgresosDia(string fecha)
        {
            double total;

            GetDouble(string.Format("Select sum (total) as totaldia  from egresos where fecha='{0}'", fecha), "totaldia", out total);
            return total;

        }
        public double ObtenerTotalEgresosMes(int mes, int uDmes, int year)
        {
            double total;

            GetDouble(string.Format("select sum (total) as totalmes from egresos where fecha >='{2}/{0}/1' and fecha <='{2}/{0}/{1}'", mes, uDmes,year), "totalmes", out total);
            return total;

        }
        public double ObtenerTotalIngresosMes(int mes, int uDmes, int year)
        {
            double total;

            GetDouble(string.Format("select sum (total) as totalmes from ingresos where fecha >='{2}/{0}/1' and fecha <='{2}/{0}/{1}'", mes, uDmes,year), "totalmes", out total);
            return total;

        }
        public double ObtenerTotalEgresos()
        {
            double total;

            GetDouble("Select sum (total) as totalcurso  from Egresos", "totalcurso", out total);
            return total;

        }

        public string ObtenerNombreOtros(bool inscripcion, bool seguro, bool libros, bool material)
        {
            string nombre="";
            //combinaciones tomadas de 4 en 1
            if (inscripcion == true && seguro == false && libros == false && material == false)
            {
                nombre = "Inscripción";
            }
            else if (inscripcion == false && seguro == true && libros == false && material == false)
            {
                nombre = "Seguro";
            }
            else if (inscripcion == false && seguro == false && libros == true && material == false)
            {
                nombre = "Libros"; ;
            }
            else if (inscripcion == false && seguro == false && libros == false && material == true)
            {
                nombre = "Material";
            }

             //combinaciones tomada de 2 =6
            else if (inscripcion == true && seguro == true && libros == false && material == false)
            {
                nombre = "Inscripción y Seguro";
            }
            else if (inscripcion == true && seguro == false && libros == true && material == false)
            {
                nombre = "Inscripción y Libros";
            }
            else if (inscripcion == true && seguro == false && libros == false && material == true)
            {
                nombre = "Inscripción y Material";
            }
            else if (inscripcion == false && seguro == true && libros == true && material == false)
            {
                nombre = "Seguro y Libros";
            }
            else if (inscripcion == false && seguro == true && libros == false && material == true)
            {
                nombre = "Seguro y Material ";
            }
            else if (inscripcion == false && seguro == false && libros == true && material == true)
            {
                nombre = "Libros y Material";
            }


          //Combinaciones tomadas de 3 en 4 =4
            else if (inscripcion == true && seguro == true && libros == true && material == false)
            {
                nombre = "Inscripción, Seguro y Libros";
            }
            else if (inscripcion == true && seguro == true && libros == false && material == true)
            {
                nombre = "Inscripción, Seguro y Material";
            }

            else if (inscripcion == true && seguro == false && libros == true && material == true)
            {
                nombre = "Inscripción, Libros y Material";
            }

            else if (inscripcion == false && seguro == true && libros == true && material == true)
            {
                nombre = "Seguro, Libros y Material ";
            }
            else if (inscripcion == true && seguro == true && libros == true && material == true)
            {
                nombre = "Inscripción, Seguro, Libros y Material";
            }

            return nombre;
        }
        public double ObtenerTotalBotnesExtra(string nombre, int idcurso)
        {
            double total =0;
            nombre=nombre.ToLower().Trim();
            //combinaciones tomadas de 4 en 1
            if (nombre ==  "inscripcion")
            {
                GetDouble("Select c.insc as total FROM costos c " +
                           "inner join curso cu on cu.id=c.idcurso " +
                           "where c.idcurso=" + idcurso, "total", out total);
            }
            else if (nombre== "seguro")
            {
                GetDouble("Select c.seguro  as total FROM costos c " +
                          "inner join curso cu on cu.id=c.idcurso " +
                          "where c.idcurso=" + idcurso, "total", out total);
            }
            else if (nombre== "libros")
            {
                GetDouble("Select c.libros  as total FROM costos c " +
                          "inner join curso cu on cu.id=c.idcurso " +
                          "where c.idcurso=" + idcurso, "total", out total);
            }
            else if (nombre== "material")
            {
                GetDouble("Select c.material  as total FROM costos c " +
                          "inner join curso cu on cu.id=c.idcurso " +
                          "where c.idcurso=" + idcurso, "total", out total);
            }
            return total;
        }
        public double ObtenerTotal(bool inscripcion, bool seguro, bool libros, bool material, int idcurso )
        {
            double total=0;
            //combinaciones tomadas de 4 en 1
            if(inscripcion==true && seguro==false && libros==false && material==false)
            {
                GetDouble("Select c.insc as total FROM costos c " +
                           "inner join curso cu on cu.id=c.idcurso " +
                           "where c.idcurso=" + idcurso, "total", out total);
            }
            else if(inscripcion==false && seguro==true && libros==false && material==false)
            {
                GetDouble("Select c.seguro  as total FROM costos c " +
                          "inner join curso cu on cu.id=c.idcurso " +
                          "where c.idcurso=" + idcurso, "total", out total);
            }
            else if(inscripcion==false && seguro==false && libros==true && material==false)
            {
                GetDouble("Select c.libros  as total FROM costos c " +
                          "inner join curso cu on cu.id=c.idcurso " +
                          "where c.idcurso=" + idcurso, "total", out total);
            }
            else if(inscripcion==false && seguro==false && libros==false && material==true)
            {   
                GetDouble("Select c.material  as total FROM costos c " +
                          "inner join curso cu on cu.id=c.idcurso " +
                          "where c.idcurso=" + idcurso, "total", out total);
            }
            
             //combinaciones tomada de 2 =6
            else if(inscripcion==true && seguro==true && libros==false && material==false)
            {
                GetDouble("Select c.insc+ c.seguro as total FROM costos c " +
                              "inner join curso cu on cu.id=c.idcurso " +
                              "where c.idcurso=" + idcurso, "total", out total);
            }
            else if(inscripcion==true && seguro==false && libros==true && material==false)
            {
                GetDouble("Select c.insc+c.libros as total FROM costos c " +
                              "inner join curso cu on cu.id=c.idcurso " +
                              "where c.idcurso=" + idcurso, "total", out total);
            }
            else if(inscripcion==true && seguro==false && libros==false && material==true)
            {
                GetDouble("Select c.insc+c.material as total FROM costos c " +
                              "inner join curso cu on cu.id=c.idcurso " +
                              "where c.idcurso=" + idcurso, "total", out total);
            }
            else if(inscripcion==false && seguro==true && libros==true && material==false)
            {
                GetDouble("Select c.seguro+c.libros as total FROM costos c " +
                              "inner join curso cu on cu.id=c.idcurso " +
                              "where c.idcurso=" + idcurso, "total", out total);
            }
             else if(inscripcion==false && seguro==true && libros==false && material==true)
            {
                GetDouble("Select c.seguro+c.material as total FROM costos c " +
                              "inner join curso cu on cu.id=c.idcurso " +
                              "where c.idcurso=" + idcurso, "total", out total);
            }
              else if(inscripcion==false && seguro==false && libros==true && material==true)
            {
                GetDouble("Select c.libros +c.material as total FROM costos c " +
                              "inner join curso cu on cu.id=c.idcurso " +
                              "where c.idcurso=" + idcurso, "total", out total);
            }


            //Combinaciones tomadas de 3 en 4 =4
            else if(inscripcion==true && seguro==true && libros==true && material==false)
            {
                GetDouble("Select c.insc +c.seguro +c.libros as total FROM costos c " +
                              "inner join curso cu on cu.id=c.idcurso " +
                              "where c.idcurso=" + idcurso, "total", out total);
            }
            else if(inscripcion==true && seguro==true && libros==false && material==true)
            {
                GetDouble("Select c.insc+c.material +c.seguro as total FROM costos c " +
                              "inner join curso cu on cu.id=c.idcurso " +
                              "where c.idcurso=" + idcurso, "total", out total);
            }
           
            else if(inscripcion==true && seguro==false && libros==true && material==true)
            {
                GetDouble("Select c.material +c.libros + c.insc as total FROM costos c " +
                              "inner join curso cu on cu.id=c.idcurso " +
                              "where c.idcurso=" + idcurso, "total", out total);
            }

            else if(inscripcion==false && seguro==true && libros==true && material==true)
            {
                GetDouble("Select c.material +c.seguro + c.libros as total FROM costos c " +
                              "inner join curso cu on cu.id=c.idcurso " +
                              "where c.idcurso=" + idcurso, "total", out total);
            }
           else if(inscripcion==true && seguro==true && libros==true && material==true)
            {
                GetDouble("Select c.insc+ c.material +c.seguro + c.libros as total FROM costos c " +
                              "inner join curso cu on cu.id=c.idcurso " +
                              "where c.idcurso=" + idcurso, "total", out total);
            }
          
            return total;
        }
        //Fin Costos

        
        //Meses
        public Mensualidades ObtenerMeses(int idalumno)
        {
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "SELECT *From mensualidades Where idalumno=" + idalumno;


                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        Mensualidades meses = new Mensualidades(dr.GetInt32("idalumno"),
                                                                dr.GetBoolean("agosto"),
                                                                dr.GetBoolean("septiembre"),
                                                                dr.GetBoolean("octubre"),
                                                                dr.GetBoolean("noviembre"),
                                                                dr.GetBoolean("diciembre"),
                                                                dr.GetBoolean("enero"),
                                                                dr.GetBoolean("febrero"),
                                                                dr.GetBoolean("marzo"),
                                                                dr.GetBoolean("abril"),
                                                                dr.GetBoolean("mayo"),
                                                                dr.GetBoolean("junio"),
                                                                dr.GetBoolean("julio"),
                                                                dr.GetBoolean("seguro"),
                                                                dr.GetBoolean("libros"),
                                                                dr.GetBoolean("material"),
                                                                dr.GetBoolean("Inscripcion"));
                                                              
                        return meses;

                    }

                    dr.Close();
                }
            }
            return null;
        }
        public void ActualizarEstadoMeses(int id, string mes)
        {
            mes = mes.ToLower().Trim();
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = string.Format("UPDATE mensualidades set {0}=false where idalumno={1}", mes, id);
                cmd.ExecuteNonQuery();
            }
        }
        // Fin de Meses

        //Personal --------
        public void AgregarNuevoPersonal(string Nombre, string Apellido, string celular, string fnacimiento, int estadocivil, int nivelestudios, int categoria, double sueldo)
        {
            Nombre = Nombre.Trim().ToUpper();
            Apellido = Apellido.Trim().ToUpper();

            int id;
            if (GetInt32("Select id from personal", "id", out id) == false)
            {
                using (PgSqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("INSERT INTO personal (id, idcategoria,fnacimiento,nombre,apellido,celular,civil,nivelestudiosid,sueldo) " +
                                                    "Values( 0 ,{0},'{1}','{2}','{3}','{4}',{5},{6},{7})",
                                                    categoria, fnacimiento, Nombre, Apellido, celular, estadocivil, nivelestudios, sueldo);

                    cmd.ExecuteNonQuery();
                }
            }
            else
            {
                using (PgSqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("INSERT INTO personal (id, idcategoria,fnacimiento,nombre,apellido,celular,civil,nivelestudiosid,sueldo) " +
                                                    "SELECT MAX(id)+1,{0},'{1}','{2}','{3}','{4}',{5},{6},{7} FROM personal",
                                                    categoria, fnacimiento, Nombre, Apellido, celular, estadocivil, nivelestudios, sueldo);

                    cmd.ExecuteNonQuery();
                }
            }
        }
        public List<Personal> ObtenerPersonal(int idcategoria)
        {
            List<Personal> personal = new List<Personal>();

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select pe.id,pe.idcategoria,cp.descripcion,pe.fnacimiento, pe.nombre, pe.apellido, pe.celular,ec.descripcion as estadocivil,ne.descripcion as nivelestudios ,pe.sueldo from personal pe " +
                                   "inner join categoriaspersonal cp on pe.idcategoria=cp.id " +
                                    "inner join estadocivil ec on pe.civil=ec.id " +
                                    "inner join nivelestudios ne on pe.nivelestudiosid=ne.id " +
                                    "Where pe.idcategoria=" +idcategoria+" "+
                                    "Order by pe.apellido;";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        personal.Add(new Personal(dr.GetInt32("id"),
                                                  dr.GetString("descripcion"),
                                                  dr.GetString("fnacimiento"),
                                                  dr.GetString("nombre"),
                                                  dr.GetString("apellido"),
                                                  dr.GetString("celular"),
                                                  dr.GetString("estadocivil"),
                                                  dr.GetString("nivelestudios"),
                                                  dr.GetDecimal("sueldo")));
                    }

                    dr.Close();
                }
            }
            return personal;
        }
        public List<CategoriaPersonal> ObtenerNivelCategoria()
        {
            List<CategoriaPersonal> categorias = new List<CategoriaPersonal>();

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select * From categoriaspersonal ";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        categorias.Add(new CategoriaPersonal(dr.GetInt32("id"), dr.GetString("descripcion")));
                    }

                    dr.Close();
                }
            }
            return categorias;
        }
        public List<CategoriaPersonal> ObtenerNivelEstudios()
        {
            List<CategoriaPersonal> categorias = new List<CategoriaPersonal>();

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select * From categoriaspersonal ";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        categorias.Add(new CategoriaPersonal(dr.GetInt32("id"), dr.GetString("descripcion")));
                    }

                    dr.Close();
                }
            }
            return categorias;
        }
        public List<NivelEstudiosPersonal> ObtenerNivelesDeCursoProfesor()
        {
            List<NivelEstudiosPersonal> nivel = new List<NivelEstudiosPersonal>();

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select * From nivelestudios";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        nivel.Add(new NivelEstudiosPersonal(dr.GetInt32("id"), dr.GetString("descripcion")));
                    }

                    dr.Close();
                }
            }
            return nivel;
        }
        public void ActualizarDatosPersonal(int Id, string Nombre, string Apellido, string Fnacimiento, int idcivil, string Celular, int idNivelEstudios,int idCategoria, decimal sueldo)
        {
            Nombre = Nombre.ToUpper().Trim();
            Apellido = Apellido.ToUpper().Trim();

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = string.Format("UPDATE personal set nombre='{0}', apellido='{1}',celular='{2}'," +
                                                "fnacimiento='{3}',idcategoria={4}, civil={5} ,nivelestudiosid={6}, sueldo={7} where id="+Id,Nombre,Apellido,Celular,Fnacimiento,idCategoria,idcivil,idNivelEstudios,sueldo);
                cmd.ExecuteNonQuery();
            }

        }
        public void DarDeBajaPersonal(int idPersonal)
        {
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = String.Format("DELETE FROM Personal per WHERE per.id={0}", idPersonal);
                cmd.ExecuteNonQuery();
            }
        }//AQUI CONCLUYE LA PROGRAMACION DE DATOS PARA MAESTROS
        // FIN PERSONAL


       // Alumnos 
        public List<Alumno> ObtenerAlumnos()
        {
            List<Alumno> alumnos = new List<Alumno>();

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select al.id, al.idpapa, cu.descripcion, " +
                                    "al.fnacimiento, al.curp, al.nombre, al.apellido, " +
                                    "al.telefono, al.pnacer, al.alergias " +
                                    "From Alumnos al inner join curso cu On cu.id=al.idcurso " +
                                    "order by al.apellido, cu.id";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        alumnos.Add(new Alumno(dr.GetInt32("id"),
                                                 dr.GetInt32("idpapa"),
                                                 dr.GetString("descripcion"),
                                                 dr.GetString("fnacimiento"),
                                                 dr.GetString("curp"),
                                                 dr.GetString("nombre"),
                                                 dr.GetString("apellido"),
                                                 dr.GetString("Telefono"),
                                                 dr.GetString("pnacer"),
                                                 dr.GetString("alergias")));
                    }

                    dr.Close();
                }
            }
            return alumnos;
        }
        public Alumno ObtenerAlumnoPorCURP(string curp)
        {
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select al.id, al.idpapa, cu.descripcion, " +
                                    "al.fnacimiento, al.curp, al.nombre, al.apellido, " +
                                    "al.telefono, al.pnacer, al.alergias " +
                                    "From Alumnos al inner join curso cu On cu.id=al.idcurso " +
                                    "Where al.curp='" + curp + "'";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        Alumno alumno = new Alumno(dr.GetInt32("id"),
                                                 dr.GetInt32("idpapa"),
                                                 dr.GetString("descripcion"),
                                                 dr.GetString("fnacimiento"),
                                                 dr.GetString("curp"),
                                                 dr.GetString("nombre"),
                                                 dr.GetString("apellido"),
                                                 dr.GetString("Telefono"),
                                                 dr.GetString("pnacer"),
                                                 dr.GetString("alergias"));
                        return alumno;
                    }

                    dr.Close();
                }
            }
            return null;
        }
        public List<Alumno> ObtenerAlumnosPorCurso(int curso)
        {
            List<Alumno> alumnos = new List<Alumno>();

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select al.id, al.idpapa, cu.descripcion, " +
                                    "al.fnacimiento, al.curp, al.nombre, " + 
                                    "al.apellido, al.telefono, al.pnacer, " + 
                                    "al.alergias From Alumnos al inner join curso cu On cu.id=al.idcurso " +
                                    "where al.idcurso=" + curso +" order by al.apellido";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        alumnos.Add(new Alumno(dr.GetInt32("id"),
                                                dr.GetInt32("idpapa"),
                                                dr.GetString("descripcion"),
                                                dr.GetString("fnacimiento"),
                                                dr.GetString("curp"),
                                                dr.GetString("nombre"),
                                                dr.GetString("apellido"),
                                                dr.GetString("Telefono"),
                                                dr.GetString("pnacer"),
                                                dr.GetString("alergias")));
                    }
                    
                    dr.Close();
                    return alumnos;
                }
            }
            
        }
        public void ActualizarDatosAlumnos(int Id, string Nombre, string Apellido, int idcurso, string fnacimiento, string telefono, string pnacer, string alergias, string curp)
        {
            Nombre = Nombre.ToUpper().Trim();
            Apellido = Apellido.ToUpper().Trim();
            pnacer = pnacer.ToUpper().Trim();
            alergias = alergias.ToUpper().Trim();

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = string.Format("UPDATE alumnos set nombre='{0}', apellido='{1}', idcurso={2}, fnacimiento='{3}', " +
                                                 "telefono='{4}', pnacer='{5}', alergias='{6}', curp='{7}' where id={8};", Nombre, Apellido, idcurso, fnacimiento, telefono, pnacer, alergias, curp, Id);
                cmd.ExecuteNonQuery();
            }

        }
        public Alumno ObtenerAlumnoPorId(int id)
        {
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select al.id, al.idpapa, cu.descripcion, " +
                                    "al.fnacimiento, al.curp, al.nombre, al.apellido, " +
                                    "al.telefono, al.pnacer, al.alergias " +
                                    "From Alumnos al inner join curso cu On cu.id=al.idcurso " +
                                    "WHERE al.id=" + id;

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        Alumno alumno = new Alumno(dr.GetInt32("id"),
                                                 dr.GetInt32("idpapa"),
                                                 dr.GetString("descripcion"),
                                                 dr.GetString("fnacimiento"),
                                                 dr.GetString("curp"),
                                                 dr.GetString("nombre"),
                                                 dr.GetString("apellido"),
                                                 dr.GetString("Telefono"),
                                                 dr.GetString("pnacer"),
                                                 dr.GetString("alergias"));
                        return alumno;
                    }

                    dr.Close();
                }
            }
            return null;
        }
        public void AgregarNuevoAlumno(int IdPapa, int Idcurso, string Fnacimiento, string curp, string Nombre, string Apellido, string Telefono, string ProblemasNacer, string Alergias)
        {
            Nombre = Nombre.Trim().ToUpper();
            Apellido = Apellido.Trim().ToUpper();
            ProblemasNacer = ProblemasNacer.Trim().ToUpper();
            Alergias = Alergias.Trim().ToUpper();
            Telefono = Telefono.Trim().ToUpper();
            curp = curp.Trim().ToUpper();

            int id;
            if (GetInt32("Select id from alumnos", "id", out id) == false)
            {
                using (PgSqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("INSERT INTO alumnos(id, idpapa, idcurso, fnacimiento, curp, nombre, apellido, telefono, pnacer, alergias) VALUES (0,{0},{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}'); ",
                                                    IdPapa, Idcurso, Fnacimiento, curp, Nombre, Apellido, Telefono, ProblemasNacer, Alergias);

                    cmd.ExecuteNonQuery();
                }
            }
            else
            {
                using (PgSqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("INSERT INTO alumnos(id, idpapa, idcurso, fnacimiento, curp, nombre, apellido, telefono, pnacer, alergias) SELECT MAX(id)+1,{0},{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}' FROM alumnos; ",                                                    
                                                    IdPapa, Idcurso, Fnacimiento, curp, Nombre, Apellido, Telefono, ProblemasNacer, Alergias);

                    cmd.ExecuteNonQuery();
                }
            }
        }
        public void DarDeBajaAlumno(int idAlumno)
        {
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = String.Format("DELETE FROM Alumnos al WHERE al.id={0}" , idAlumno);
                cmd.ExecuteNonQuery();
            }
        }
        public void EliminarMensualidades(int idAlumno)
        {
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = String.Format("DELETE FROM MENSUALIDADES where idalumno = {0}", idAlumno); 
                cmd.ExecuteNonQuery();
            }
        }
        public void AgregarAlumnoMensualidades(int Idalumno)
        {
          
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = String.Format("INSERT INTO mensualidades (idalumno,agosto,septiembre,octubre,noviembre,diciembre,enero,febrero,marzo,abril,mayo,junio,julio, seguro, material, libros, inscripcion) VALUES ({0},true,true,true,true,true,true,true,true,true,true,true, true, true,true,true, true)", 
                                                Idalumno);

                cmd.ExecuteNonQuery();
            }
        }
        // FIN ALUMNOS

       
        //Papas
        public Papa ObtenerPapaPorId(int id)
        {
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "SELECT pa.id, pa.nombre, pa.apellido, pa.direccion, pa.celular, " +
                                    "ec.descripcion, pa.profesion FROM papas pa " +
                                    "INNER JOIN estadocivil ec ON ec.id=pa.civil " +
                                    "WHERE pa.id=" + id;

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        Papa papa = new Papa(dr.GetInt32("id"),
                                                dr.GetString("nombre"),
                                                dr.GetString("Apellido"),
                                                dr.GetString("direccion"),
                                                dr.GetString("Celular"),
                                                dr.GetString("descripcion"),
                                                dr.GetString("Profesion"));
                        return papa;
                    }

                    dr.Close();
                }
            }
            return null;
        }
        public List<Papa> ObtenerPapaId(int id)
        {
            List<Papa> papas = new List<Papa>();
            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select pa.id,pa.nombre, pa.apellido, pa.direccion, pa.celular, ec.descripcion,pa.profesion From papas pa " +
                                  "inner join estadocivil ec On ec.id=pa.civil "+
                                  "WHERE pa.id= "+ id;

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        papas.Add(new Papa(dr.GetInt32("id"), 
                                            dr.GetString("nombre"), 
                                            dr.GetString("Apellido"),
                                            dr.GetString("direccion"),
                                            dr.GetString("Celular"), 
                                            dr.GetString("descripcion"), 
                                            dr.GetString("Profesion")));
                    }

                    dr.Close();
                }
            }
            return papas;
        }
        public List<EstadoCivil> ObtenerEstadoCivil()
        {
            List<EstadoCivil> estadocivil = new List<EstadoCivil>();

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Select * From estadocivil";

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read() == true)
                    {
                        estadocivil.Add(new EstadoCivil(dr.GetInt32("id"), dr.GetString("descripcion")));
                    }

                    dr.Close();
                }
            }
            return estadocivil;
        }
        public void AgregarNuevoPapa(string Nombre, string Apellido, string Celular, int EstadoCivil, string Profesion, string Direccion)
        {
            Nombre = Nombre.Trim().ToUpper();
            Apellido = Apellido.Trim().ToUpper();

            Profesion = Profesion.Trim().ToUpper();
            int id;
            if (GetInt32("Select id from papas", "id", out id) == false)
            {
                using (PgSqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("INSERT INTO papas(id,nombre, apellido, celular, civil, profesion, direccion) "
                                                    + "VALUES (0,'{0}', '{1}','{2}', {3}, '{4}','{5}') ", Nombre, Apellido, Celular, EstadoCivil, Profesion, Direccion);

                    cmd.ExecuteNonQuery();
                }
            }
            else
            {
                using (PgSqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("INSERT INTO papas(id,nombre, apellido, celular, civil, profesion, direccion)"
                                                    + "SELECT MAX(id)+1,'{0}', '{1}','{2}', {3}, '{4}','{5}' FROM papas", Nombre, Apellido, Celular, EstadoCivil, Profesion, Direccion);

                    cmd.ExecuteNonQuery();
                }
            }
        }
        public int ObtenerIdPapa(string Nombre)
        {
            int id;
            Nombre = Nombre.ToUpper().Trim();
            GetInt32("Select id From papas where nombre= '" + Nombre + "'", "id", out id);
            return id;
        }
        public void ActualizarDatosPapa(int Id, string Nombre, string Apellido, int Civil, string Profesion, string Celular, string direccion)
        {
            Nombre = Nombre.ToUpper().Trim();
            Apellido = Apellido.ToUpper().Trim();
            Profesion = Profesion.ToUpper().Trim();


            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = string.Format("UPDATE papas set nombre='{0}', apellido='{1}',civil={2}, profesion='{3}' , celular='{4}' , direccion='{6}' where id={5}", Nombre, Apellido, Civil, Profesion, Celular, Id, direccion);
                cmd.ExecuteNonQuery();
            }
        }
        public void DarDeBajaPapa(int idPapa)
        {
            int valor;
            if (GetInt32("select al.id " +
                        "from alumnos al " +
                        "where al.idpapa=" + idPapa,
                        "id",
                        out valor) == false)
            {
                using (PgSqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("DELETE FROM papas pa WHERE pa.id={0}", idPapa);

                    cmd.ExecuteNonQuery();
                }
            }
        }
        //FIN PAPAS        
    }
}
