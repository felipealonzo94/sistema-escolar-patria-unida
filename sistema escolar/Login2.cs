﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDbConnection;


namespace Sistema_Escolar
{
    public partial class Login2 : Form
    {
        public Login2()
        {
            InitializeComponent();
        }

        private void Login2_Load(object sender, EventArgs e)
        {
            using (DbConnection conn = new DbConnection(true))
            {
                cmbUsuarios.DataSource = conn.ObtenerUsuarios();
                cmbUsuarios.DisplayMember = "usuario";
                conn.Close();
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Usuarios usuario = cmbUsuarios.SelectedItem as Usuarios;
            string user = txtContraseña.Text.Trim().ToUpper();
            
            if (user == usuario.Contrasena && cmbUsuarios.Text == "ADMINISTRADOR")
            {
                
                
                Form1 principal = new Form1(usuario.Id);
                principal.Show();
                txtContraseña.Clear();
                
                
            }
            else if (user == usuario.Contrasena && cmbUsuarios.Text == "USUARIO")
            {
        
                Form1 principal = new Form1(usuario.Id);
                principal.Show();
                txtContraseña.Clear();
            }
            else
            {

                MessageBox.Show("Error de Contraseña", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
