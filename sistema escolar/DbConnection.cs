﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Devart.Data.PostgreSql;

namespace MyDbConnection
{
    partial class DbConnection : IDisposable
    {
        private PgSqlConnection conn;

        public DbConnection(bool autoconnect = false)
        {
            conn = new PgSqlConnection();
            conn.Host = "isilo.db.elephantsql.com";
            conn.Port = 5432;
            conn.Database = "rizbkdcj";
            conn.UserId = "rizbkdcj";
            conn.Password = "KeFUUX6upoBUGw3hZIH75Uy4teBPYGib";
            conn.Unicode = true;

            if (autoconnect == true)
            {
                conn.Open();
            }
        }

        public void Open()
        {
            conn.Open();
        }

        public void Close()
        {
            conn.Close();
        }

        public void Dispose()
        {
            conn.Dispose();
        }

        public bool GetInt32(string query, string column, out int value)
        {
            bool found = false;
            value = 0;

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = query;

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read() == true)
                    {
                        value = dr.GetInt32(column);
                        found = true;
                    }

                    dr.Close();
                }
            }

            return found;
        }
        public bool GetDouble(string query, string column, out double value)
        {
            bool found = false;
            value = 0;

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = query;

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read() == true)
                    {
                        value = dr.GetDouble(column);
                        found = true;
                    }

                    dr.Close();
                }
            }

            return found;
        }
        public bool GetDecimal(string query, string column, out decimal value)
        {
            bool found = false;
            value = 0;

            using (PgSqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = query;

                using (PgSqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read() == true)
                    {
                        value = dr.GetDecimal(column);
                        found = true;
                    }

                    dr.Close();
                }
            }

            return found;
        }
    }
}

