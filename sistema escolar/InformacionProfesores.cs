﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDbConnection;
namespace Sistema_Escolar
{
    public partial class InformacionProfesores : Form
    {
        public InformacionProfesores()
        {
            InitializeComponent();
        }

        private void InformacionProfesores_Load(object sender, EventArgs e)
        {
            using (DbConnection conn = new DbConnection(true))
            {
                cmbCategorias.DataSource = conn.ObtenerNivelCategoria();
                cmbCategorias.DisplayMember = "Descripcion";
                cmbCategorias2.DataSource = conn.ObtenerNivelCategoria();
                cmbCategorias2.DisplayMember = "Descripcion";
                cmbCivil.DataSource = conn.ObtenerEstadoCivil();
                cmbCivil.DisplayMember = "Descripcion";
                cmbNivelEstudios.DataSource = conn.ObtenerNivelesDeCursoProfesor();
                cmbNivelEstudios.DisplayMember = "Descripcion";
                conn.Close();
            }
        }

        private void rbVer_CheckedChanged(object sender, EventArgs e)
        {
            if (rbVer.Checked == true && rbModificar.Checked == false && radBaja.Checked == false)
            {
                txtNombre.ReadOnly = true;
                txtApellido.ReadOnly = true;
                txtFnacimiento.ReadOnly = true;
                txtSueldo.ReadOnly = true;
                txtCelular.ReadOnly = true;
                cmbCivil.Enabled = false;
                cmbNivelEstudios.Enabled = false;
                cmbCategorias2.Enabled = false;

                btnModificar.Enabled = false;
                btnBaja.Enabled = false;
            }
            else if (rbVer.Checked == false && rbModificar.Checked == true && radBaja.Checked == false)
            {
                txtNombre.ReadOnly = false;
                txtApellido.ReadOnly = false;
                txtFnacimiento.ReadOnly = false;
                txtSueldo.ReadOnly = false;
                txtCelular.ReadOnly = false;
                cmbCivil.Enabled = true;
                cmbNivelEstudios.Enabled = true;
                cmbCategorias2.Enabled = true;

                btnModificar.Enabled = true;
                btnBaja.Enabled = false;
            }
            else
            {
                txtNombre.ReadOnly = true;
                txtApellido.ReadOnly = true;
                txtFnacimiento.ReadOnly = true;
                txtSueldo.ReadOnly = true;
                txtCelular.ReadOnly = true;
                cmbCivil.Enabled = false;
                cmbNivelEstudios.Enabled = false;
                cmbCategorias2.Enabled = false;

                btnModificar.Enabled = false;
                btnBaja.Enabled = true;
            }
        }

        private void cmbCategorias_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCategorias.SelectedItem != null)
            {
                CategoriaPersonal categorias = cmbCategorias.SelectedItem as CategoriaPersonal;
                using (DbConnection conn = new DbConnection(true))
                {
                    lstBusqueda.DataSource = conn.ObtenerPersonal(categorias.Id);
                    lstBusqueda.DisplayMember = "NombreCompleto";
                    conn.Close();
                }
                if (lstBusqueda.Items.Count == 0)
                {
                    txtNombre.Clear();
                    txtApellido.Clear();
                    txtSueldo.Clear();
                    txtCelular.Clear();
                    txtFnacimiento.Clear();                    

                    using (DbConnection conn = new DbConnection(true))
                    {
                        cmbCategorias2.DataSource = conn.ObtenerNivelCategoria();
                        cmbCategorias2.DisplayMember = "Descripcion";
                        cmbCivil.DataSource = conn.ObtenerEstadoCivil();
                        cmbCivil.DisplayMember = "Descripcion";
                        cmbNivelEstudios.DataSource = conn.ObtenerNivelesDeCursoProfesor();
                        cmbNivelEstudios.DisplayMember = "Descripcion";
                        conn.Close();
                    }
                }
            }
        }

        private void lstBusqueda_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstBusqueda.SelectedItem != null)
            {
                Personal personal = lstBusqueda.SelectedItem as Personal;
                string sueldo = personal.Sueldo.ToString();
                txtNombre.Text = personal.Nombre;
                txtApellido.Text = personal.Apellido;
                txtFnacimiento.Text = personal.FechaNacimiento;
                txtSueldo.Text = sueldo;
                txtCelular.Text = personal.Celular;
                cmbNivelEstudios.Text = personal.NivelEstudios;
                cmbCivil.Text = personal.Civil;
                cmbCategorias2.Text = personal.Categoria;
            }
            else
            {
                txtNombre.Clear();
                txtApellido.Clear();
                txtSueldo.Clear();
                txtCelular.Clear();
                txtFnacimiento.Clear();

                txtSueldo.ReadOnly = true;
                txtNombre.ReadOnly = true;
                txtApellido.ReadOnly = true;
                txtFnacimiento.ReadOnly = true;
                cmbCivil.Enabled = false;
                cmbNivelEstudios.Enabled = false;
                cmbCategorias2.Enabled = false;
                txtCelular.ReadOnly = true;

                using (DbConnection conn = new DbConnection(true))
                {
                    cmbCategorias2.DataSource = conn.ObtenerNivelCategoria();
                    cmbCategorias2.DisplayMember = "Descripcion";
                    cmbCivil.DataSource = conn.ObtenerEstadoCivil();
                    cmbCivil.DisplayMember = "Descripcion";
                    cmbNivelEstudios.DataSource = conn.ObtenerNivelesDeCursoProfesor();
                    cmbNivelEstudios.DisplayMember = "Descripcion";
                    conn.Close();
                }
            }
        }        
        
        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text.Trim() == ""
                || txtApellido.Text.Trim() == ""
                || txtFnacimiento.Text.Trim() == ""
                || txtSueldo.Text.Trim() == ""
                || txtCelular.Text.Trim() == "")
            {
                MessageBox.Show("Hay campos importantes vacios", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (lstBusqueda.SelectedItem != null)
                {
                    Personal personal = lstBusqueda.SelectedItem as Personal;
                    CategoriaPersonal categoria = cmbCategorias2.SelectedItem as CategoriaPersonal;
                    EstadoCivil civil = cmbCivil.SelectedItem as EstadoCivil;
                    NivelEstudiosPersonal nivelestudiospersonal = cmbNivelEstudios.SelectedItem as NivelEstudiosPersonal;
                    DialogResult dr = MessageBox.Show(string.Format("¿Desea Actualizar la siguiente Informacion de {0}?", personal.NombreCompleto), "Informacion", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dr == DialogResult.Yes)
                    {
                        using (DbConnection conn = new DbConnection(true))
                        {
                            conn.ActualizarDatosPersonal(personal.Id, txtNombre.Text, txtApellido.Text, txtFnacimiento.Text, civil.Id, txtCelular.Text, nivelestudiospersonal.Id, categoria.Id, Convert.ToDecimal(txtSueldo.Text));
                            conn.Close();
                        }
                        txtNombre.Clear();
                        txtApellido.Clear();
                        txtSueldo.Clear();
                        txtCelular.Clear();
                        txtFnacimiento.Clear();

                        txtSueldo.ReadOnly = true;
                        txtNombre.ReadOnly = true;
                        txtApellido.ReadOnly = true;
                        txtFnacimiento.ReadOnly = true;
                        cmbCivil.Enabled = false;
                        cmbNivelEstudios.Enabled = false;
                        cmbCategorias2.Enabled = false;
                        btnModificar.Enabled = false;
                        txtCelular.ReadOnly = true;

                        using (DbConnection conn = new DbConnection(true))
                        {
                            cmbCategorias2.DataSource = conn.ObtenerNivelCategoria();
                            cmbCategorias2.DisplayMember = "Descripcion";
                            cmbCivil.DataSource = conn.ObtenerEstadoCivil();
                            cmbCivil.DisplayMember = "Descripcion";
                            cmbNivelEstudios.DataSource = conn.ObtenerNivelesDeCursoProfesor();
                            cmbNivelEstudios.DisplayMember = "Descripcion";
                            lstBusqueda.DataSource = conn.ObtenerPersonal(categoria.Id);
                            conn.Close();
                        }
                        rbModificar.Checked = false;
                        rbVer.Checked = true;
                        btnModificar.Enabled = false;
                    }
                }
                else
                {
                    MessageBox.Show("¡Seleccione una persona de la lista!", "Advertencia", MessageBoxButtons.OK,
                                           MessageBoxIcon.Information);
                }
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿Realmente Desea Salir?", "Informacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (dr == DialogResult.OK)
            {
                Close();
            }
        }

        private void btnBaja_Click(object sender, EventArgs e)
        {
            if (lstBusqueda.SelectedItem != null)
            {
                Personal personal = lstBusqueda.SelectedItem as Personal;
                CategoriaPersonal categoria = cmbCategorias2.SelectedItem as CategoriaPersonal;
                DialogResult dr = MessageBox.Show(string.Format("¿Desea Dar de Baja a: {0}?", personal.NombreCompleto), "Informacion", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dr == DialogResult.Yes)
                {
                    using (DbConnection conn = new DbConnection(true))
                    {
                        conn.DarDeBajaPersonal(personal.Id);
                        conn.Close();
                    }
                    txtNombre.Clear();
                    txtApellido.Clear();
                    txtSueldo.Clear();
                    txtCelular.Clear();
                    txtFnacimiento.Clear();

                    txtSueldo.ReadOnly = true;
                    txtNombre.ReadOnly = true;
                    txtApellido.ReadOnly = true;
                    txtFnacimiento.ReadOnly = true;
                    cmbCivil.Enabled = false;
                    cmbNivelEstudios.Enabled = false;
                    cmbCategorias2.Enabled = false;
                    btnModificar.Enabled = false;
                    txtCelular.ReadOnly = true;

                    using (DbConnection conn = new DbConnection(true))
                    {
                        cmbCategorias2.DataSource = conn.ObtenerNivelCategoria();
                        cmbCategorias2.DisplayMember = "Descripcion";
                        cmbCivil.DataSource = conn.ObtenerEstadoCivil();
                        cmbCivil.DisplayMember = "Descripcion";
                        cmbNivelEstudios.DataSource = conn.ObtenerNivelesDeCursoProfesor();
                        cmbNivelEstudios.DisplayMember = "Descripcion";
                        lstBusqueda.DataSource = conn.ObtenerPersonal(categoria.Id);
                        conn.Close();
                    }
                    rbModificar.Checked = false;
                    rbVer.Checked = true;
                    btnModificar.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show("¡Seleccione una persona de la lista!", "Advertencia", MessageBoxButtons.OK,
                                       MessageBoxIcon.Information);
            }
        }
    }
}
