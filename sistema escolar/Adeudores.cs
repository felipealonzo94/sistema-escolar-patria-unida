﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDbConnection;

namespace Sistema_Escolar
{
    public partial class Adeudores : Form
    {
        public Adeudores()
        {
            InitializeComponent();
        }

        private void Adeudores_Load(object sender, EventArgs e)
        {
            cmbMes.SelectedIndex = 0;
        }

        void AgregarCurso(Alumno alumno)
        {
            string curso;

            if (alumno.IdCurso == "MATERNAL")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "1 KINDER")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "2 KINDER")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "3 KINDER")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "1 PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "2 PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "3 PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "4 PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "5 PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "6 PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "BECADO KINDER")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "BECADO PRIMARIA")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
            else if (alumno.IdCurso == "SITUACION ESPECIAL")
            {
                lstAlumnos.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstAlumnos.Items.Add(curso);
            }
        }
        private void cmbMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstAlumnos.Items.Clear();
            using (DbConnection conn = new DbConnection(true))
            {
                if(cmbMes.SelectedItem!=null)
                {
                    Mensualidades mes = cmbMes.SelectedItem as Mensualidades;
                    string a, b = "";
                    foreach (Alumno alumno in conn.AlumnosQueDebenMes(cmbMes.Text))
                    {
                        a = alumno.IdCurso;
                        if (a != b || lstAlumnos.Items.Count == 0)
                        
                            AgregarCurso(alumno);
                            lstAlumnos.Items.Add(alumno);
                            b = a;
                        
                    }
                if (lstAlumnos.Items.Count > 0)
                {
                    lstAlumnos.Enabled = true;
                    lstAlumnos.Items.RemoveAt(0);
                }
                
                lstAlumnos.DisplayMember = "NombreCompleto";
                }
                conn.Close();
            }
        }
        //if (lstAlumnos.Items.Count == 0)
        //{
        //    MessageBox.Show("¡No se encontró ningún alumno con\nel nombre y/o apellido proporcionado!", "Sin resultados", MessageBoxButtons.OK,
        //    MessageBoxIcon.Information);
        //}
        private void lstAlumnos_DoubleClick(object sender, EventArgs e)
        {

            string SelectedItem = Convert.ToString(lstAlumnos.SelectedItem);

            if (lstAlumnos.SelectedItem != null && SelectedItem.Length > 0 && !SelectedItem.Contains("CURSO:"))
            {
                using (DbConnection conn = new DbConnection(true))
                {
                    Alumno alumno = lstAlumnos.SelectedItem as Alumno;

                    Papa papa = conn.ObtenerPapaPorId(Convert.ToInt32(alumno.IdPapa));

                    string informacion = String.Format("¿Desea visualizar la información general del alumno?");

                    DialogResult result = MessageBox.Show(informacion,
                                                            "Confirmación",
                                                            MessageBoxButtons.YesNo,
                                                            MessageBoxIcon.Question);
                    switch (result)
                    {
                        case DialogResult.Yes:
                            lstAlumnos.Items.Clear();
                          
                            Modificar_Informacion_Alumnos form = new Modificar_Informacion_Alumnos(alumno, papa);

                            DialogResult resultado = form.ShowDialog();
                            break;

                        case DialogResult.No:
                        default:
                            break;
                    }

                    conn.Close();
                }
            }
        }


    }
}

