﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDbConnection;

namespace Sistema_Escolar
{
    public partial class Modificar_Informacion_Alumnos : Form
    {
        Alumno alumno;
        Papa papa;
        public Modificar_Informacion_Alumnos(Alumno alumno, Papa papa)
        {
            InitializeComponent();
            this.alumno = alumno;
            this.papa = papa;
        }

        private void Modificar_Informacion_Alumnos_Load(object sender, EventArgs e)
        {
            if (alumno != null & papa != null)
            {
                using (DbConnection conn = new DbConnection(true))
                {
                    cmbCursoEscolar.DataSource = conn.ObtenerNivelesDeCurso();
                    cmbCursoEscolar.DisplayMember = "Descripcion";
                    cmbEstadoCivil.DataSource = conn.ObtenerEstadoCivil();
                    cmbEstadoCivil.DisplayMember = "Descripcion";

                    Alumno alumnoactualizado = conn.ObtenerAlumnoPorId(alumno.Id);

                    //Botones y Combobox
                    cmbCursoEscolar.Enabled = false;
                    cmbEstadoCivil.Enabled = false;

                    //Información del alumno
                    txtCURP.Text = alumno.CURP;
                    txtNombre.Text = alumnoactualizado.Nombre;
                    txtApellido.Text = alumnoactualizado.Apellido;
                    txtTelefono.Text = alumnoactualizado.Telefono;
                    txtProblemasNacer.Text = alumnoactualizado.ProblemasNacer;
                    txtAlergias.Text = alumnoactualizado.Alergias;
                    txtFechaNacimiento.Text = alumnoactualizado.FechaNacimiento;
                    cmbCursoEscolar.Text = alumnoactualizado.IdCurso;

                    //Informacion Papas
                    txtNombreP.Text = papa.Nombre;
                    txtApellidoP.Text = papa.Apellido;
                    txtDireccion.Text = papa.Direccion;
                    cmbEstadoCivil.Text = papa.EstadoCivil;
                    txtProfesion.Text = papa.Profesion;
                    txtCelular.Text = papa.Celular;   

                    conn.Close();
                }
            }
            else
            {
                using (DbConnection conn = new DbConnection(true))
                {
                    cmbCursoEscolar.DataSource = conn.ObtenerNivelesDeCurso();
                    cmbCursoEscolar.DisplayMember = "Descripcion";
                    cmbEstadoCivil.DataSource = conn.ObtenerEstadoCivil();
                    cmbEstadoCivil.DisplayMember = "Descripcion";
                    conn.Close();
                }
            }
        }

        private void rbVer_CheckedChanged(object sender, EventArgs e)
        {
            if (rbVer.Checked == true && rbModificar.Checked == false && radBaja.Checked == false)
            {
                //Informacion Alumnos
                txtCURP.ReadOnly = true;
                txtNombre.ReadOnly = true;
                txtApellido.ReadOnly = true;
                txtFechaNacimiento.ReadOnly = true;
                txtTelefono.ReadOnly = true;
                txtProblemasNacer.ReadOnly = true;
                txtAlergias.ReadOnly = true;

                //Informacion Papas
                txtNombreP.ReadOnly = true;
                txtApellidoP.ReadOnly = true;
                txtDireccion.ReadOnly = true;
                cmbEstadoCivil.Enabled = false;
                txtProfesion.ReadOnly = true;
                txtCelular.ReadOnly = true;

                //Botones y Combobox
                cmbCursoEscolar.Enabled = false;
                cmbEstadoCivil.Enabled = false;
                btnModificar.Enabled = false;
                btnBaja.Enabled = false;
            }
            else if (rbVer.Checked == false && rbModificar.Checked == true && radBaja.Checked == false)
            {
                //Informacion Alumnos
                txtCURP.ReadOnly = false;
                txtNombre.ReadOnly = false;
                txtApellido.ReadOnly = false;
                txtFechaNacimiento.ReadOnly = false;
                txtTelefono.ReadOnly = false;
                txtProblemasNacer.ReadOnly = false;
                txtAlergias.ReadOnly = false;

                //Informacion Maestros
                txtNombreP.ReadOnly = false;
                txtApellidoP.ReadOnly = false;
                txtDireccion.ReadOnly = false;
                cmbEstadoCivil.Enabled = true;
                txtProfesion.ReadOnly = false;
                txtCelular.ReadOnly = false;

                //Botones y Combobox
                cmbCursoEscolar.Enabled = true;
                cmbEstadoCivil.Enabled = true;
                btnModificar.Enabled = true;
                btnBaja.Enabled = false;
            }
            else
            {
                //Informacion Alumnos
                txtCURP.ReadOnly = true;
                txtNombre.ReadOnly = true;
                txtApellido.ReadOnly = true;
                txtFechaNacimiento.ReadOnly = true;
                txtTelefono.ReadOnly = true;
                txtProblemasNacer.ReadOnly = true;
                txtAlergias.ReadOnly = true;

                //Informacion Papas
                txtNombreP.ReadOnly = true;
                txtApellidoP.ReadOnly = true;
                txtDireccion.ReadOnly = true;
                cmbEstadoCivil.Enabled = false;
                txtProfesion.ReadOnly = true;
                txtCelular.ReadOnly = true;

                //Botones y Combobox
                cmbCursoEscolar.Enabled = false;
                cmbEstadoCivil.Enabled = false;
                btnModificar.Enabled = false;
                btnBaja.Enabled = true;
            }
        }

        string contenido = "Introduzca el Nombre o Apellido del alumno...";
        private void txtBusqueda_MouseClick(object sender, MouseEventArgs e)
        {
            if (txtBusqueda.Text == contenido)
            {
                txtBusqueda.Select(0, 0);
            }
        }

        private void txtBusqueda_TextChanged(object sender, EventArgs e)
        {
            if (txtBusqueda.Text == "")
            {
                txtBusqueda.ForeColor = System.Drawing.Color.DarkGray;
                txtBusqueda.Text = contenido;
            }
            else if (txtBusqueda.Text.Contains(contenido) && txtBusqueda.Text != contenido
                && txtBusqueda.Text != " " + contenido)
            {
                string letra = txtBusqueda.Text.Substring(0, 1);
                txtBusqueda.Text = letra;
                txtBusqueda.Select(1, 0);
                txtBusqueda.ForeColor = System.Drawing.Color.Black;
            }

            if (txtBusqueda.Text.Contains("  "))
            {
                int i = txtBusqueda.Text.IndexOf("  ");
                txtBusqueda.Text = txtBusqueda.Text.Remove(i, 1);
                txtBusqueda.Select(i + 1, 0);
            }

            if (txtBusqueda.Text == " " + contenido)
            {
                txtBusqueda.Text = " " + contenido;
                txtBusqueda.Text = txtBusqueda.Text.Remove(0, 1);
                txtBusqueda.Select(0, 0);
            }
            if (txtBusqueda.Text == " ")
            {
                txtBusqueda.Text = "";
            }

            if (txtBusqueda.Text.Length > 0) btnBuscar.Enabled = true;
            else btnBuscar.Enabled = false;

            if (txtBusqueda.Text == contenido)
            {
                txtBusqueda.ForeColor = System.Drawing.Color.DarkGray;
                btnBuscar.Enabled = false;
                txtBusqueda.Select(0, 0);
            }
        }

        private void txtBusqueda_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnBuscar.PerformClick();

                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            lstBusqueda.Items.Clear();

            using (DbConnection conn = new DbConnection(true))
            {
                cmbCursoEscolar.DataSource = conn.ObtenerNivelesDeCurso();
                cmbCursoEscolar.DisplayMember = "Descripcion";

                cmbEstadoCivil.DataSource = conn.ObtenerEstadoCivil();
                cmbEstadoCivil.DisplayMember = "Descripcion";

                conn.Close();
            }

            //Datos del Alumno
            txtCURP.Clear();
            txtNombre.Clear();
            txtApellido.Clear();
            txtTelefono.Clear();
            txtProblemasNacer.Clear();
            txtAlergias.Clear();
            txtFechaNacimiento.Clear();

            //Informacion Papas
            txtNombreP.Clear();
            txtApellidoP.Clear();
            txtDireccion.Clear();
            txtProfesion.Clear();
            txtCelular.Clear();
            
            string cadena = txtBusqueda.Text.Trim().ToLower();

            using (DbConnection conn = new DbConnection(true))
            {
                string a, b = "";
                foreach (Alumno alumno in conn.ObtenerAlumnos())
                {
                    string nombre = alumno.Nombre.ToLower().Trim();
                    string apellido = alumno.Apellido.ToLower().Trim();
                    string cadenanombre = nombre + " " + apellido;
                    string[] nombre1 = nombre.Split(' ');
                    string[] apellido1 = apellido.Split(' ');
                    string nombre1apellido1 = nombre1[0] + " " + apellido1[0];
                    if ((nombre1apellido1.Contains(cadena)
                        || cadenanombre.Contains(cadena)
                        || apellido.Contains(cadena)
                        || nombre.Contains(cadena)) && txtBusqueda.Text.Length != 0)
                    {
                        a = alumno.IdCurso;
                        if (a != b || lstBusqueda.Items.Count == 0)
                            AgregarCurso(alumno);
                        lstBusqueda.Items.Add(alumno);
                        b = a;
                    }
                    else
                    {
                        string[] cadena2 = txtBusqueda.Text.Trim().ToLower().Split();

                        if (nombre.Contains(cadena2[0]))
                        {
                            a = alumno.IdCurso;
                            if (a != b || lstBusqueda.Items.Count == 0)
                                AgregarCurso(alumno);
                            lstBusqueda.Items.Add(alumno);
                            b = a;
                        }
                    }
                }
                if (lstBusqueda.Items.Count == 0)
                {
                    MessageBox.Show("¡No se encontró ningún alumno con\nel nombre proporcionado!", "Sin resultados", MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                }
                if (lstBusqueda.Items.Count > 0)
                {
                    lstBusqueda.Enabled = true;
                    lstBusqueda.Items.RemoveAt(0);
                }
                lstBusqueda.DisplayMember = "NombreCompleto";

                conn.Close();                        
            }            
        }

        void AgregarCurso(Alumno alumno)
        {
            string curso;

            if (alumno.IdCurso == "MATERNAL")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "1 KINDER")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "2 KINDER")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "3 KINDER")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "1 PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "2 PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "3 PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "4 PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "5 PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "6 PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "BECADO KINDER")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "BECADO PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "SITUACION ESPECIAL")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
        }

        private void lstBuscar_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SelectedItem = Convert.ToString(lstBusqueda.SelectedItem);

            if (lstBusqueda.SelectedItem != null && SelectedItem.Length > 0 && !SelectedItem.Contains("CURSO:"))
            {
                Alumno NuevoAlumno = lstBusqueda.SelectedItem as Alumno;

                //Datos del Alumno
                txtCURP.Text = NuevoAlumno.CURP;
                txtNombre.Text = NuevoAlumno.Nombre;
                txtApellido.Text = NuevoAlumno.Apellido;
                txtTelefono.Text = NuevoAlumno.Telefono;
                txtProblemasNacer.Text = NuevoAlumno.ProblemasNacer;
                txtAlergias.Text = NuevoAlumno.Alergias;
                txtFechaNacimiento.Text = NuevoAlumno.FechaNacimiento;
                cmbCursoEscolar.Text = NuevoAlumno.IdCurso;

                using (DbConnection conn = new DbConnection(true))
                {
                    Papa DatosPapa = conn.ObtenerPapaPorId(NuevoAlumno.IdPapa);

                    //Informacion Papas
                    txtNombreP.Text = DatosPapa.Nombre;
                    txtApellidoP.Text = DatosPapa.Apellido;
                    txtDireccion.Text = DatosPapa.Direccion;
                    cmbEstadoCivil.Text = DatosPapa.EstadoCivil;
                    txtProfesion.Text = DatosPapa.Profesion;
                    txtCelular.Text = DatosPapa.Celular;

                    conn.Close();
                }
            }
            else
            {
                using (DbConnection conn = new DbConnection(true))
                {
                    cmbCursoEscolar.DataSource = conn.ObtenerNivelesDeCurso();
                    cmbCursoEscolar.DisplayMember = "Descripcion";

                    cmbEstadoCivil.DataSource = conn.ObtenerEstadoCivil();
                    cmbEstadoCivil.DisplayMember = "Descripcion";

                    conn.Close();
                }

                //Datos del Alumno
                txtCURP.Clear();
                txtNombre.Clear();
                txtApellido.Clear();
                txtTelefono.Clear();
                txtProblemasNacer.Clear();
                txtAlergias.Clear();
                txtFechaNacimiento.Clear();

                //Informacion Papas
                txtNombreP.Clear();
                txtApellidoP.Clear();
                txtDireccion.Clear();
                txtProfesion.Clear();
                txtCelular.Clear();
            }
        }             

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (txtCURP.Text.Trim() == ""
                || txtNombre.Text.Trim() == ""
                || txtApellido.Text.Trim() == ""
                || txtApellido.Text.Trim() == ""
                || txtFechaNacimiento.Text.Trim() == ""
                || txtTelefono.Text.Trim() == ""
                || txtProblemasNacer.Text.Trim() == ""
                || txtAlergias.Text.Trim() == ""
                || txtNombreP.Text.Trim() == ""
                || txtApellidoP.Text.Trim() == "")
            {
                MessageBox.Show("Hay campos importantes vacios", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (alumno != null && papa != null)
                {
                    Curso curso = cmbCursoEscolar.SelectedItem as Curso;
                    EstadoCivil civil = cmbEstadoCivil.SelectedItem as EstadoCivil;

                    DialogResult respuesta = MessageBox.Show("¿Esta Seguro que desea actualizar la siguiente informacion", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (respuesta == DialogResult.Yes)
                    {
                        using (DbConnection conn = new DbConnection(true))
                        {
                            conn.ActualizarDatosAlumnos(alumno.Id, txtNombre.Text, txtApellido.Text, Convert.ToInt32(curso.Id), txtFechaNacimiento.Text, txtTelefono.Text, txtProblemasNacer.Text, txtAlergias.Text, txtCURP.Text);
                            conn.ActualizarDatosPapa(papa.Id, txtNombreP.Text, txtApellidoP.Text, civil.Id, txtProfesion.Text, txtCelular.Text, txtDireccion.Text);
                            MessageBox.Show("La Informacion ha sido Actualizada", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            cmbCursoEscolar.DataSource = conn.ObtenerNivelesDeCurso();
                            cmbCursoEscolar.DisplayMember = "Descripcion";

                            cmbEstadoCivil.DataSource = conn.ObtenerEstadoCivil();
                            cmbEstadoCivil.DisplayMember = "Descripcion";

                            conn.Close();
                        }

                        txtBusqueda.Text = contenido;
                        lstBusqueda.Items.Clear();

                        //Datos del Alumno
                        txtCURP.Clear();
                        txtNombre.Clear();
                        txtApellido.Clear();
                        txtTelefono.Clear();
                        txtProblemasNacer.Clear();
                        txtAlergias.Clear();
                        txtFechaNacimiento.Clear();

                        //Informacion Papas
                        txtNombreP.Clear();
                        txtApellidoP.Clear();
                        txtDireccion.Clear();
                        txtProfesion.Clear();
                        txtCelular.Clear();

                        btnModificar.Enabled = false;
                        rbVer.Checked = true;
                        rbModificar.Checked = false;

                        //Informacion Alumnos
                        txtNombre.ReadOnly = true;
                        txtApellido.ReadOnly = true;
                        txtTelefono.ReadOnly = true;
                        txtFechaNacimiento.ReadOnly = true;
                        txtProblemasNacer.ReadOnly = true;
                        txtAlergias.ReadOnly = true;


                        //Informacion Maestros
                        txtNombreP.ReadOnly = true;
                        txtApellidoP.ReadOnly = true;
                        cmbEstadoCivil.Enabled = true;
                        txtProfesion.ReadOnly = true;
                        txtCelular.ReadOnly = true;


                        //Botones y Combobox
                        cmbCursoEscolar.Enabled = false;
                        cmbEstadoCivil.Enabled = false;
                        //dtpFechaNacimiento.Enabled = false;
                        btnModificar.Enabled = false;                           
                    }
                }
                else
                {
                    string SelectedItem = Convert.ToString(lstBusqueda.SelectedItem);

                    if (lstBusqueda.SelectedItem != null && SelectedItem.Length > 0 && !SelectedItem.Contains("CURSO:"))
                    {
                        Alumno nuevoalumno = lstBusqueda.SelectedItem as Alumno;
                        Curso curso = cmbCursoEscolar.SelectedItem as Curso;
                        EstadoCivil civil = cmbEstadoCivil.SelectedItem as EstadoCivil;

                        DialogResult respuesta = MessageBox.Show("¿Esta Seguro que desea actualizar la siguiente informacion", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (respuesta == DialogResult.Yes)
                        {
                            using (DbConnection conn = new DbConnection(true))
                            {
                                Papa nuevopapa = conn.ObtenerPapaPorId(nuevoalumno.IdPapa);

                                conn.ActualizarDatosAlumnos(nuevoalumno.Id, txtNombre.Text, txtApellido.Text, Convert.ToInt32(curso.Id), txtFechaNacimiento.Text, txtTelefono.Text, txtProblemasNacer.Text, txtAlergias.Text, txtCURP.Text);
                                conn.ActualizarDatosPapa(nuevopapa.Id, txtNombreP.Text, txtApellidoP.Text, civil.Id, txtProfesion.Text, txtCelular.Text, txtDireccion.Text);
                                MessageBox.Show("La Informacion ha sido Actualizada", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                cmbCursoEscolar.DataSource = conn.ObtenerNivelesDeCurso();
                                cmbCursoEscolar.DisplayMember = "Descripcion";

                                cmbEstadoCivil.DataSource = conn.ObtenerEstadoCivil();
                                cmbEstadoCivil.DisplayMember = "Descripcion";

                                conn.Close();
                            }

                            txtBusqueda.Text = contenido;
                            lstBusqueda.Items.Clear();

                            //Datos del Alumno
                            txtCURP.Clear();
                            txtNombre.Clear();
                            txtApellido.Clear();
                            txtTelefono.Clear();
                            txtProblemasNacer.Clear();
                            txtAlergias.Clear();
                            txtFechaNacimiento.Clear();

                            //Informacion Papas
                            txtNombreP.Clear();
                            txtApellidoP.Clear();
                            txtDireccion.Clear();
                            txtProfesion.Clear();
                            txtCelular.Clear();

                            btnModificar.Enabled = false;
                            rbVer.Checked = true;
                            rbModificar.Checked = false;

                            //Informacion Alumnos
                            txtNombre.ReadOnly = true;
                            txtApellido.ReadOnly = true;
                            txtTelefono.ReadOnly = true;
                            txtFechaNacimiento.ReadOnly = true;
                            txtProblemasNacer.ReadOnly = true;
                            txtAlergias.ReadOnly = true;


                            //Informacion Maestros
                            txtNombreP.ReadOnly = true;
                            txtApellidoP.ReadOnly = true;
                            cmbEstadoCivil.Enabled = true;
                            txtProfesion.ReadOnly = true;
                            txtCelular.ReadOnly = true;


                            //Botones y Combobox
                            cmbCursoEscolar.Enabled = false;
                            cmbEstadoCivil.Enabled = false;
                            //dtpFechaNacimiento.Enabled = false;
                            btnModificar.Enabled = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("¡Seleccione un alumno de la lista!", "Advertencia", MessageBoxButtons.OK,
                                           MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿Realmente Desea Salir?", "Informacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (dr == DialogResult.OK)
            {
                Close();
            }

            else { }
        }

        private void btnBaja_Click(object sender, EventArgs e)
        {
            if (alumno != null && papa != null)
            {
                DialogResult respuesta = MessageBox.Show("¿Esta Seguro que desea dar de baja al alumno?\n(Toda la información del alumno se eliminará de manera permanente)", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (respuesta == DialogResult.Yes)
                {
                    using (DbConnection conn = new DbConnection(true))
                    {
                        conn.DarDeBajaAlumno(alumno.Id);
                        conn.EliminarMensualidades(alumno.Id);
                        conn.DarDeBajaPapa(papa.Id);

                        cmbCursoEscolar.DataSource = conn.ObtenerNivelesDeCurso();
                        cmbCursoEscolar.DisplayMember = "Descripcion";

                        cmbEstadoCivil.DataSource = conn.ObtenerEstadoCivil();
                        cmbEstadoCivil.DisplayMember = "Descripcion";

                        conn.Close();
                    }

                    txtBusqueda.Text = contenido;
                    lstBusqueda.Items.Clear();

                    //Datos del Alumno
                    txtCURP.Clear();
                    txtNombre.Clear();
                    txtApellido.Clear();
                    txtTelefono.Clear();
                    txtProblemasNacer.Clear();
                    txtAlergias.Clear();
                    txtFechaNacimiento.Clear();

                    //Informacion Papas
                    txtNombreP.Clear();
                    txtApellidoP.Clear();
                    txtDireccion.Clear();
                    txtProfesion.Clear();
                    txtCelular.Clear();

                    btnModificar.Enabled = false;
                    rbVer.Checked = true;
                    rbModificar.Checked = false;

                    //Informacion Alumnos
                    txtNombre.ReadOnly = true;
                    txtApellido.ReadOnly = true;
                    txtTelefono.ReadOnly = true;
                    txtFechaNacimiento.ReadOnly = true;
                    txtProblemasNacer.ReadOnly = true;
                    txtAlergias.ReadOnly = true;


                    //Informacion Maestros
                    txtNombreP.ReadOnly = true;
                    txtApellidoP.ReadOnly = true;
                    cmbEstadoCivil.Enabled = true;
                    txtProfesion.ReadOnly = true;
                    txtCelular.ReadOnly = true;


                    //Botones y Combobox
                    cmbCursoEscolar.Enabled = false;
                    cmbEstadoCivil.Enabled = false;
                    //dtpFechaNacimiento.Enabled = false;
                    btnModificar.Enabled = false;
                }
            }
            else
            {
                string SelectedItem = Convert.ToString(lstBusqueda.SelectedItem);

                if (lstBusqueda.SelectedItem != null && SelectedItem.Length > 0 && !SelectedItem.Contains("CURSO:"))
                {
                    Alumno nuevoalumno = lstBusqueda.SelectedItem as Alumno;                    

                    DialogResult respuesta = MessageBox.Show("¿Esta Seguro que desea dar de baja al alumno?\n(Toda la información del alumno se eliminará de manera permanente)", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (respuesta == DialogResult.Yes)
                    {
                        using (DbConnection conn = new DbConnection(true))
                        {
                            Papa nuevopapa = conn.ObtenerPapaPorId(nuevoalumno.IdPapa);

                            conn.DarDeBajaAlumno(nuevoalumno.Id);
                            conn.DarDeBajaPapa(nuevopapa.Id);

                            cmbCursoEscolar.DataSource = conn.ObtenerNivelesDeCurso();
                            cmbCursoEscolar.DisplayMember = "Descripcion";

                            cmbEstadoCivil.DataSource = conn.ObtenerEstadoCivil();
                            cmbEstadoCivil.DisplayMember = "Descripcion";

                            conn.Close();
                        }

                        txtBusqueda.Text = contenido;
                        lstBusqueda.Items.Clear();

                        //Datos del Alumno
                        txtCURP.Clear();
                        txtNombre.Clear();
                        txtApellido.Clear();
                        txtTelefono.Clear();
                        txtProblemasNacer.Clear();
                        txtAlergias.Clear();
                        txtFechaNacimiento.Clear();

                        //Informacion Papas
                        txtNombreP.Clear();
                        txtApellidoP.Clear();
                        txtDireccion.Clear();
                        txtProfesion.Clear();
                        txtCelular.Clear();

                        btnModificar.Enabled = false;
                        rbVer.Checked = true;
                        rbModificar.Checked = false;

                        //Informacion Alumnos
                        txtNombre.ReadOnly = true;
                        txtApellido.ReadOnly = true;
                        txtFechaNacimiento.ReadOnly = true;
                        txtTelefono.ReadOnly = true;
                        txtProblemasNacer.ReadOnly = true;
                        txtAlergias.ReadOnly = true;


                        //Informacion Maestros
                        txtNombreP.ReadOnly = true;
                        txtApellidoP.ReadOnly = true;
                        cmbEstadoCivil.Enabled = true;
                        txtProfesion.ReadOnly = true;
                        txtCelular.ReadOnly = true;


                        //Botones y Combobox
                        cmbCursoEscolar.Enabled = false;
                        cmbEstadoCivil.Enabled = false;
                        //dtpFechaNacimiento.Enabled = false;
                        btnModificar.Enabled = false;
                    }
                }
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
