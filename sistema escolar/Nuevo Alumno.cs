﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyDbConnection;

namespace Sistema_Escolar
{
    public partial class Nuevo_Alumno : Form
    {
        public Nuevo_Alumno()
        {
            InitializeComponent();
        }

        private void Nuevo_Alumno_Load(object sender, EventArgs e)
        {
            using (DbConnection conn = new DbConnection(true))
            {
                cmbCursoEscolar.DataSource = conn.ObtenerNivelesDeCurso();
                cmbCursoEscolar.DisplayMember = "Descripcion";
                cmbEstadoCivil.DataSource = conn.ObtenerEstadoCivil();
                cmbEstadoCivil.DisplayMember = "Descripcion";
                conn.Close();
            }
        }

        void ReestablecerDatos()
        {
            using (DbConnection conn = new DbConnection(true))
            {
                cmbCursoEscolar.DataSource = conn.ObtenerNivelesDeCurso();
                cmbCursoEscolar.DisplayMember = "Descripcion";
                cmbEstadoCivil.DataSource = conn.ObtenerEstadoCivil();
                cmbEstadoCivil.DisplayMember = "Descripcion";

                txtCURP.Clear();
                txtNombre.Clear();
                txtApellido.Clear();
                dtpFechaNacimiento.Value = DateTime.Now;
                txtTelefono.Clear();
                txtProblemasNacer.Clear();
                txtAlergias.Clear();

                txtNombreP.Clear();
                txtApellidoP.Clear();
                txtProfesion.Clear();
                txtCelular.Clear();
                txtDireccion.Clear();

                conn.Close();
            }
        }

        private void rbSi_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSi.Checked == true && rbNo.Checked == false)
            {
                lstBusqueda.Items.Clear();
                txtBusqueda.Text = contenido;

                lblDigNombre.Enabled = true;
                txtBusqueda.Enabled = true;
                lblBusqueda.Enabled = true;
                lstBusqueda.Enabled = true;

                txtNombreP.ReadOnly = true;
                txtApellidoP.ReadOnly = true;
                txtDireccion.ReadOnly = true;
                cmbEstadoCivil.Enabled = false;
                txtProfesion.ReadOnly = true;
                txtCelular.ReadOnly = true;

                txtApellido.ReadOnly = true;
                txtTelefono.ReadOnly = true;

                ReestablecerDatos();
            }
            else
            {
                lstBusqueda.Items.Clear();
                txtBusqueda.Text = contenido;

                lblDigNombre.Enabled = false;
                txtBusqueda.Enabled = false;
                lblBusqueda.Enabled = false;
                lstBusqueda.Enabled = false;

                txtNombreP.ReadOnly = false;
                txtApellidoP.ReadOnly = false;
                txtDireccion.ReadOnly = false;
                cmbEstadoCivil.Enabled = true;
                txtProfesion.ReadOnly = false;
                txtCelular.ReadOnly = false;

                txtApellido.ReadOnly = false;
                txtTelefono.ReadOnly = false;

                ReestablecerDatos();
            }
        }

        string contenido = "Introduzca el Nombre o Apellido del alumno...";
        private void txtBusqueda_MouseClick(object sender, MouseEventArgs e)
        {
            if (txtBusqueda.Text == contenido)
            {
                txtBusqueda.Select(0, 0);
            }
        }

        private void txtBusqueda_TextChanged(object sender, EventArgs e)
        {
            if (txtBusqueda.Text == "")
            {
                txtBusqueda.ForeColor = System.Drawing.Color.DarkGray;
                txtBusqueda.Text = contenido;
            }
            else if (txtBusqueda.Text.Contains(contenido) && txtBusqueda.Text != contenido
                && txtBusqueda.Text != " " + contenido)
            {
                string letra = txtBusqueda.Text.Substring(0, 1);
                txtBusqueda.Text = letra;
                txtBusqueda.Select(1, 0);
                txtBusqueda.ForeColor = System.Drawing.Color.Black;
            }

            if (txtBusqueda.Text.Contains("  "))
            {
                int i = txtBusqueda.Text.IndexOf("  ");
                txtBusqueda.Text = txtBusqueda.Text.Remove(i, 1);
                txtBusqueda.Select(i + 1, 0);
            }

            if (txtBusqueda.Text == " " + contenido)
            {
                txtBusqueda.Text = " " + contenido;
                txtBusqueda.Text = txtBusqueda.Text.Remove(0, 1);
                txtBusqueda.Select(0, 0);
            }
            if (txtBusqueda.Text == " ")
            {
                txtBusqueda.Text = "";
            }

            if (txtBusqueda.Text.Length > 0) btnBuscar.Enabled = true;
            else btnBuscar.Enabled = false;

            if (txtBusqueda.Text == contenido)
            {
                txtBusqueda.ForeColor = System.Drawing.Color.DarkGray;
                btnBuscar.Enabled = false;
                txtBusqueda.Select(0, 0);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            lstBusqueda.Items.Clear();

            ReestablecerDatos();

            string cadena = txtBusqueda.Text.Trim().ToLower();

            using (DbConnection conn = new DbConnection(true))
            {
                string a, b = "";
                foreach (Alumno alumno in conn.ObtenerAlumnos())
                {
                    string nombre = alumno.Nombre.ToLower().Trim();
                    string apellido = alumno.Apellido.ToLower().Trim();
                    string cadenanombre = nombre + " " + apellido;
                    string[] nombre1 = nombre.Split(' ');
                    string[] apellido1 = apellido.Split(' ');
                    string nombre1apellido1 = nombre1[0] + " " + apellido1[0];
                    if ((nombre1apellido1.Contains(cadena)
                        || cadenanombre.Contains(cadena)
                        || apellido.Contains(cadena)
                        || nombre.Contains(cadena)) && txtBusqueda.Text.Length != 0)
                    {
                        a = alumno.IdCurso;
                        if (a != b || lstBusqueda.Items.Count == 0)
                            AgregarCurso(alumno);
                        lstBusqueda.Items.Add(alumno);
                        b = a;
                    }
                    else
                    {
                        string[] cadena2 = txtBusqueda.Text.Trim().ToLower().Split();

                        if (nombre.Contains(cadena2[0]))
                        {
                            a = alumno.IdCurso;
                            if (a != b || lstBusqueda.Items.Count == 0)
                                AgregarCurso(alumno);
                            lstBusqueda.Items.Add(alumno);
                            b = a;
                        }
                    }
                }
                if (lstBusqueda.Items.Count == 0)
                {
                    MessageBox.Show("¡No se encontró ningún alumno con\nel nombre proporcionado!", "Sin resultados", MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                }
                if (lstBusqueda.Items.Count > 0)
                {
                    lstBusqueda.Enabled = true;
                    lstBusqueda.Items.RemoveAt(0);
                }
                lstBusqueda.DisplayMember = "NombreCompleto";

                conn.Close();            
            }
        }

        void AgregarCurso(Alumno alumno)
        {
            string curso;

            if (alumno.IdCurso == "MATERNAL")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "1 KINDER")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "2 KINDER")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "3 KINDER")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "1 PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "2 PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "3 PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "4 PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "5 PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "6 PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "SITUACION ESPECIAL")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "BECADO KINDER")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
            else if (alumno.IdCurso == "BECADO PRIMARIA")
            {
                lstBusqueda.Items.Add("");
                curso = "CURSO: " + alumno.IdCurso;
                lstBusqueda.Items.Add(curso);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void archivoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        } 
        
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregarAlumno_Click(object sender, EventArgs e)
        {
            Curso curso = cmbCursoEscolar.SelectedItem as Curso;
            EstadoCivil estado = cmbEstadoCivil.SelectedItem as EstadoCivil;
            //Condicion Elementos Vacios
            if (txtCURP.Text.Length == 0 || txtAlergias.Text.Length == 0 || txtApellido.Text.Length == 0 || txtApellidoP.Text.Length == 0 || txtCelular.Text.Length == 0 || txtEstadoCivil.Text.Length == 0 || txtNombre.Text.Length == 0 || txtNombreP.Text.Length == 0 || txtProblemasNacer.Text.Length == 0 || txtProfesion.Text.Length == 0 || txtTelefono.Text.Length == 0)
            {
                MessageBox.Show("Debe LLenar Todos Los Campos");
            }
            else
            {
                //AGREGAR ALUMNOS Y PAPAS
                if (rbNo.Checked == true)
                {
                    DialogResult dialogResult = MessageBox.Show("¿Está Seguro Que Desea almacenar los siguientes datos?", "Advertencia", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        using (DbConnection conn = new DbConnection(true))
                        {
                            conn.AgregarNuevoPapa(txtNombreP.Text, txtApellidoP.Text, txtCelular.Text, estado.Id, txtProfesion.Text, txtDireccion.Text);
                            conn.AgregarNuevoAlumno(conn.ObtenerIdPapa(txtNombreP.Text), curso.Id, dtpFechaNacimiento.Value.ToString("yyyy-MM-dd"), txtCURP.Text, txtNombre.Text, txtApellido.Text, txtTelefono.Text, txtProblemasNacer.Text, txtAlergias.Text);
                            Alumno alumnonuevo = conn.ObtenerAlumnoPorCURP(txtCURP.Text.Trim().ToUpper());
                            conn.AgregarAlumnoMensualidades(alumnonuevo.Id);
                            conn.Close();
                        }


                        MessageBox.Show("El Alumno y El Papá ha sido Guardado de manera exitosa", "Alumno Guardado", MessageBoxButtons.OK);

                        DialogResult dialogResult2 = MessageBox.Show("¿Desea Agregar Otro Alumno?", "Informacion", MessageBoxButtons.YesNo);

                        if (dialogResult2 == DialogResult.Yes)
                        {
                            ReestablecerDatos();

                        }
                        else if (dialogResult2 == DialogResult.No)
                        {
                            Close();
                        }
                    }
                    //else if (dialogResult == DialogResult.No)
                    //{
                    //    do something else
                    //}
                }
                //AGREGAR SOLO ALUMNO
                else
                {
                    string SelectedItem = Convert.ToString(lstBusqueda.SelectedItem);
                    if (lstBusqueda.SelectedItem != null && SelectedItem.Length > 0 && !SelectedItem.Contains("CURSO:"))
                    {
                        Alumno hermano = lstBusqueda.SelectedItem as Alumno;

                        DialogResult dialogResult = MessageBox.Show("¿Está Seguro Que Desea almacenar los siguientes datos?", "Advertencia", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            using (DbConnection conn = new DbConnection(true))
                            {
                                Papa DatosPapa = conn.ObtenerPapaPorId(hermano.IdPapa);                                
                                conn.AgregarNuevoAlumno(DatosPapa.Id, curso.Id, dtpFechaNacimiento.Value.ToString("yyyy-MM-dd"), txtCURP.Text, txtNombre.Text, txtApellido.Text, txtTelefono.Text, txtProblemasNacer.Text, txtAlergias.Text);
                                Alumno alumnonuevo = conn.ObtenerAlumnoPorCURP(txtCURP.Text.Trim().ToUpper());
                                conn.AgregarAlumnoMensualidades(alumnonuevo.Id);
                                conn.Close();
                            }

                            MessageBox.Show("El Alumno ha sido Guardado de manera exitosa", "Alumno Guardado", MessageBoxButtons.OK);

                            DialogResult dialogResult2 = MessageBox.Show("¿Desea Agregar Otro Alumno?", "Información", MessageBoxButtons.YesNo);

                            if (dialogResult2 == DialogResult.Yes)
                            {
                                txtBusqueda.Text = contenido;
                                lstBusqueda.Items.Clear();
                                ReestablecerDatos();

                            }
                            else if (dialogResult2 == DialogResult.No)
                            {
                                Close();
                            }
                        }
                    }
                }
            }
        }

        private void lstBuscar_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SelectedItem = Convert.ToString(lstBusqueda.SelectedItem);

            if (lstBusqueda.SelectedItem != null && SelectedItem.Length > 0 && !SelectedItem.Contains("CURSO:"))
            {
                Alumno hermano = lstBusqueda.SelectedItem as Alumno;
                txtApellido.Text = hermano.Apellido;
                txtTelefono.Text = hermano.Telefono;
                using (DbConnection conn = new DbConnection(true))
                {
                    Papa papa = conn.ObtenerPapaPorId(hermano.IdPapa);

                    txtNombreP.Text = papa.Nombre;
                    txtCelular.Text = papa.Celular;
                    txtApellidoP.Text = papa.Apellido;
                    cmbEstadoCivil.Text = papa.EstadoCivil;
                    txtProfesion.Text = papa.Profesion;
                    txtDireccion.Text = papa.Direccion;

                    conn.Close();

                }
            }
            else
            {
                ReestablecerDatos();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {

        }

        private void txtNombreP_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtApellidoP_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtProfesion_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCelular_TextChanged(object sender, EventArgs e)
        {

        }                        
    }
}
