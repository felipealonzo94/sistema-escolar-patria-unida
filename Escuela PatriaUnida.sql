﻿-- Table: "Alumnos"

DROP TABLE alumnos;
DROP TABLE papas;
DROP TABLE personal;
DROP TABLE estadocivil;
DROP TABLE nivelestudios;
DROP TABLE mensualidades;
DROP TABLE costos;
DROP TABLE curso;
DROP TABLE categoriaspersonal;
DROP TABLE ingresos;
DROP TABLE egresos;
DROP TABLE usuarios;

CREATE TABLE usuarios
(
  id integer not null,
  usuario character varying NOT NULL,
  contrasena character varying NOT NULL,
  CONSTRAINT usuarios_pkey PRIMARY KEY (id)
  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE usuarios
  OWNER TO postgres;

  
CREATE TABLE mensualidades
(
  idalumno integer NOT NULL,
  agosto boolean,
  septiembre boolean,
  octubre boolean,
  noviembre boolean,
  diciembre boolean,
  enero boolean,
  febrero boolean,
  marzo boolean,
  abril boolean,
  mayo boolean,
  junio boolean,
  julio boolean,
  seguro boolean, 
  material boolean,
  libros boolean, 
  inscripcion boolean,
  CONSTRAINT mensualidades_idalumno_key UNIQUE (idalumno)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mensualidades
  OWNER TO postgres;

CREATE TABLE ingresos
(
  id integer NOT NULL,
  fecha date NOT NULL,  
  descripcion character varying NOT NULL,
  total numeric NOT NULL,
  CONSTRAINT ingresos_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ingresos
  OWNER TO postgres;
  
CREATE TABLE egresos
(
  id integer NOT NULL,
  fecha date NOT NULL,  
  descripcion character varying NOT NULL,
  total numeric NOT NULL,
  CONSTRAINT egresos_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE egresos
  OWNER TO postgres;
  
CREATE TABLE estadocivil
(
  id integer NOT NULL,
  descripcion character varying NOT NULL,
  CONSTRAINT estadocivil_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE estadocivil
  OWNER TO postgres;

  

  
CREATE TABLE curso
(
  id integer NOT NULL,
  descripcion character varying NOT NULL,
  CONSTRAINT curso_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE curso
  OWNER TO postgres;
  
CREATE TABLE costos
(
  id integer NOT NULL,
  idcurso integer NOT NULL,
  seguro integer NOT NULL,
  insc numeric NOT NULL,
  libros numeric NOT NULL,
  material numeric NOT NULL,
  primer numeric NOT NULL,
  segundo numeric NOT NULL,
  tercero numeric NOT NULL,
  CONSTRAINT costos_pkey PRIMARY KEY (id),
  CONSTRAINT costos_idcurso_fkey FOREIGN KEY (idcurso)
      REFERENCES curso (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE costos
  OWNER TO postgres;
  
  CREATE TABLE categoriaspersonal
(
  id integer NOT NULL,
  descripcion character varying NOT NULL,
  CONSTRAINT categoriaspersonal_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE categoriaspersonal
  OWNER TO postgres;

CREATE TABLE nivelestudios
(
  id integer NOT NULL,
  descripcion character varying NOT NULL,
  CONSTRAINT nivelestudios_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE nivelestudios
  OWNER TO postgres;

CREATE TABLE papas
(
  id integer NOT NULL,
  nombre character varying NOT NULL,
  apellido character varying NOT NULL,
  celular character varying NOT NULL,
  civil integer NOT NULL,
  profesion character varying,
  direccion character varying,
  CONSTRAINT papas_pkey PRIMARY KEY (id),
  CONSTRAINT papas_civil_fkey FOREIGN KEY (civil)
      REFERENCES estadocivil (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE papas
  OWNER TO postgres;
  
 CREATE TABLE alumnos
(
  id integer NOT NULL Unique,  
  idpapa integer NOT NULL,
  idcurso integer NOT NULL,
  fnacimiento character varying NOT NULL,
  curp character varying ,
  nombre character varying NOT NULL,
  apellido character varying NOT NULL,
  telefono character varying NOT NULL,
  pnacer character varying NOT NULL,
  alergias character varying NOT NULL,
  CONSTRAINT alumnos_pkey PRIMARY KEY (id),
  CONSTRAINT alumnos_idcurso_fkey FOREIGN KEY (idcurso)
      REFERENCES curso (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT alumnos_idpapa_fkey FOREIGN KEY (idpapa)
      REFERENCES papas (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE alumnos
  OWNER TO postgres;
 


 CREATE TABLE personal
(
  id integer NOT NULL,
  idcategoria integer NOT NULL,
  fnacimiento date NOT NULL,
  nombre character varying NOT NULL,
  apellido character varying NOT NULL,
  celular character varying NOT NULL,
  civil integer NOT NULL,
  nivelestudiosid integer NOT NULL,
  sueldo numeric NOT NULL,
  CONSTRAINT personal_pkey PRIMARY KEY (id),
  CONSTRAINT personal_civil_fkey FOREIGN KEY (civil)
      REFERENCES estadocivil (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT personal_idcategoria_fkey FOREIGN KEY (idcategoria)
      REFERENCES categoriaspersonal (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT personal_nivelestudiosid_fkey FOREIGN KEY (nivelestudiosid)
      REFERENCES nivelestudios (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE personal
  OWNER TO postgres;
  

  --********************************************
  --INGRESO DE VALORES
  --********************************************
INSERT INTO usuarios(id,usuario, contrasena) Values (0,'ADMINISTRADOR', 'PATRIAUNIDA');
INSERT INTO usuarios(id,usuario, contrasena) Values (1,'USUARIO', 'ESCUELA');

INSERT INTO estadocivil (id, descripcion) Values (0, 'SOLTERO/A');
INSERT INTO estadocivil (id, descripcion) Values (1, 'CASADO/A');
INSERT INTO estadocivil (id, descripcion) Values (2, 'DIVORCIADO/A');
INSERT INTO estadocivil (id, descripcion) Values (3, 'VIUDO/A');


INSERT INTO nivelestudios (id, descripcion) Values (0, 'PRIMARIA');
INSERT INTO nivelestudios (id, descripcion) Values (1, 'SECUNDARIA');
INSERT INTO nivelestudios (id, descripcion) Values (2, 'PREPARATORIA');
INSERT INTO nivelestudios (id, descripcion) Values (3, 'UNIVERSIDAD');
INSERT INTO nivelestudios (id, descripcion) Values (4, 'MAESTRIA');


INSERT INTO curso (id, descripcion) VALUES (0, 'MATERNAL');
INSERT INTO curso (id, descripcion) VALUES (1, '1 KINDER');
INSERT INTO curso (id, descripcion) VALUES (2, '2 KINDER');
INSERT INTO curso (id, descripcion) VALUES (3, '3 KINDER');
INSERT INTO curso (id, descripcion) VALUES (4, '1 PRIMARIA');
INSERT INTO curso (id, descripcion) VALUES (5, '2 PRIMARIA');
INSERT INTO curso (id, descripcion) VALUES (6, '3 PRIMARIA');
INSERT INTO curso (id, descripcion) VALUES (7, '4 PRIMARIA');
INSERT INTO curso (id, descripcion) VALUES (8, '5 PRIMARIA');
INSERT INTO curso (id, descripcion) VALUES (9, '6 PRIMARIA');
INSERT INTO curso (id, descripcion) VALUES (10, 'BECADO KINDER');
INSERT INTO curso (id, descripcion) VALUES (11, 'BECADO PRIMARIA');
INSERT INTO curso (id, descripcion) VALUES (12, 'SITUACION ESPECIAL');

INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(0,0,750, 1000, 600, 400, 690, 720, 850);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(1,1,750, 1100, 600, 500, 690, 720, 850);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(2,2,750, 1200, 600, 600, 690, 720, 850);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(3,3,750, 1300, 600, 700, 690, 720, 850);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(4,4,1000, 1400,600, 800, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(5,5,1000, 1500, 600, 900, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(6,6,1000, 1600,600, 1000, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(7,7,1000, 1700,600, 1100, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(8,8,1000, 1800,600, 1200, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(9,9,1000, 1800,600, 1200, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(10,10,1000, 1800,600, 1200, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(11,11,1000, 1800,600, 1200, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(12,12,1000, 1800,600, 1200, 850, 900, 1000);

INSERT INTO categoriaspersonal (id, descripcion) VALUES (0, 'Docente');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (1, 'Nanas');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (2, 'Educacion Fisica');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (3, 'Ingles');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (4, 'Administrativos');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (5, 'Cocineras');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (6, 'Intendentes');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (7, 'Otros');

