﻿-- Table: "Alumnos"

DROP TABLE alumnos;
DROP TABLE papas;
DROP TABLE personal;
DROP TABLE estadocivil;
DROP TABLE nivelestudios;
DROP TABLE mensualidades;
DROP TABLE costos;
DROP TABLE curso;
DROP TABLE categoriaspersonal;
DROP TABLE ingresos;
DROP TABLE egresos;
DROP TABLE usuarios;

CREATE TABLE usuarios
(
  id integer not null,
  usuario character varying NOT NULL,
  contrasena character varying NOT NULL,
  CONSTRAINT usuarios_pkey PRIMARY KEY (id)
  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE usuarios
  OWNER TO postgres;

  
CREATE TABLE mensualidades
(
  idalumno integer NOT NULL,
  agosto boolean,
  septiembre boolean,
  octubre boolean,
  noviembre boolean,
  diciembre boolean,
  enero boolean,
  febrero boolean,
  marzo boolean,
  abril boolean,
  mayo boolean,
  junio boolean,
  julio boolean,
  seguro boolean, 
  material boolean,
  libros boolean, 
  inscripcion boolean,
  CONSTRAINT mensualidades_idalumno_key UNIQUE (idalumno)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mensualidades
  OWNER TO postgres;

CREATE TABLE ingresos
(
  id integer NOT NULL,
  fecha date NOT NULL,  
  descripcion character varying NOT NULL,
  total numeric NOT NULL,
  CONSTRAINT ingresos_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ingresos
  OWNER TO postgres;
  
CREATE TABLE egresos
(
  id integer NOT NULL,
  fecha date NOT NULL,  
  descripcion character varying NOT NULL,
  total numeric NOT NULL,
  CONSTRAINT egresos_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE egresos
  OWNER TO postgres;
  
CREATE TABLE estadocivil
(
  id integer NOT NULL,
  descripcion character varying NOT NULL,
  CONSTRAINT estadocivil_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE estadocivil
  OWNER TO postgres;

  

  
CREATE TABLE curso
(
  id integer NOT NULL,
  descripcion character varying NOT NULL,
  CONSTRAINT curso_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE curso
  OWNER TO postgres;
  
CREATE TABLE costos
(
  id integer NOT NULL,
  idcurso integer NOT NULL,
  seguro integer NOT NULL,
  insc numeric NOT NULL,
  libros numeric NOT NULL,
  material numeric NOT NULL,
  primer numeric NOT NULL,
  segundo numeric NOT NULL,
  tercero numeric NOT NULL,
  CONSTRAINT costos_pkey PRIMARY KEY (id),
  CONSTRAINT costos_idcurso_fkey FOREIGN KEY (idcurso)
      REFERENCES curso (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE costos
  OWNER TO postgres;
  
  CREATE TABLE categoriaspersonal
(
  id integer NOT NULL,
  descripcion character varying NOT NULL,
  CONSTRAINT categoriaspersonal_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE categoriaspersonal
  OWNER TO postgres;

CREATE TABLE nivelestudios
(
  id integer NOT NULL,
  descripcion character varying NOT NULL,
  CONSTRAINT nivelestudios_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE nivelestudios
  OWNER TO postgres;

CREATE TABLE papas
(
  id integer NOT NULL,
  nombre character varying NOT NULL,
  apellido character varying NOT NULL,
  celular numeric NOT NULL,
  civil integer NOT NULL,
  profesion character varying,
  direccion character varying,
  CONSTRAINT papas_pkey PRIMARY KEY (id),
  CONSTRAINT papas_civil_fkey FOREIGN KEY (civil)
      REFERENCES estadocivil (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE papas
  OWNER TO postgres;
  
 CREATE TABLE alumnos
(
  id integer NOT NULL Unique,  
  idpapa integer NOT NULL,
  idcurso integer NOT NULL,
  fnacimiento date NOT NULL,
  curp character varying ,
  nombre character varying NOT NULL,
  apellido character varying NOT NULL,
  telefono character varying NOT NULL,
  pnacer character varying NOT NULL,
  alergias character varying NOT NULL,
  CONSTRAINT alumnos_pkey PRIMARY KEY (curp),
  CONSTRAINT alumnos_idcurso_fkey FOREIGN KEY (idcurso)
      REFERENCES curso (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT alumnos_idpapa_fkey FOREIGN KEY (idpapa)
      REFERENCES papas (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE alumnos
  OWNER TO postgres;
 


 CREATE TABLE personal
(
  id integer NOT NULL,
  idcategoria integer NOT NULL,
  fnacimiento date NOT NULL,
  nombre character varying NOT NULL,
  apellido character varying NOT NULL,
  celular character varying NOT NULL,
  civil integer NOT NULL,
  nivelestudiosid integer NOT NULL,
  sueldo numeric NOT NULL,
  CONSTRAINT personal_pkey PRIMARY KEY (id),
  CONSTRAINT personal_civil_fkey FOREIGN KEY (civil)
      REFERENCES estadocivil (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT personal_idcategoria_fkey FOREIGN KEY (idcategoria)
      REFERENCES categoriaspersonal (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT personal_nivelestudiosid_fkey FOREIGN KEY (nivelestudiosid)
      REFERENCES nivelestudios (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE personal
  OWNER TO postgres;



select al.nombre, al.apellido, mes.agosto, mes.septiembre,mes.octubre, mes.noviembre, mes.diciembre from alumnos al
cross join mensualidades mes where mes.idalumno=al.id; 


  


  --********************************************
  --INGRESO DE VALORES
  --********************************************
INSERT INTO usuarios(id,usuario, contrasena) Values (0,'ADMINISTRADOR', 'PATRIAUNIDA');
INSERT INTO usuarios(id,usuario, contrasena) Values (1,'USUARIO', 'ESCUELA');

INSERT INTO estadocivil (id, descripcion) Values (0, 'SOLTERO/A');
INSERT INTO estadocivil (id, descripcion) Values (1, 'CASADO/A');
INSERT INTO estadocivil (id, descripcion) Values (2, 'DIVORCIADO/A');
INSERT INTO estadocivil (id, descripcion) Values (3, 'VIUDO/A');


INSERT INTO nivelestudios (id, descripcion) Values (0, 'PRIMARIA');
INSERT INTO nivelestudios (id, descripcion) Values (1, 'SECUNDARIA');
INSERT INTO nivelestudios (id, descripcion) Values (2, 'PREPARATORIA');
INSERT INTO nivelestudios (id, descripcion) Values (3, 'UNIVERSIDAD');
INSERT INTO nivelestudios (id, descripcion) Values (4, 'MAESTRIA');

INSERT INTO curso (id, descripcion) VALUES (0, 'MATERNAL');
INSERT INTO curso (id, descripcion) VALUES (1, '1 KINDER');
INSERT INTO curso (id, descripcion) VALUES (2, '2 KINDER');
INSERT INTO curso (id, descripcion) VALUES (3, '3 KINDER');
INSERT INTO curso (id, descripcion) VALUES (4, '1 PRIMARIA');
INSERT INTO curso (id, descripcion) VALUES (5, '2 PRIMARIA');
INSERT INTO curso (id, descripcion) VALUES (6, '3 PRIMARIA');
INSERT INTO curso (id, descripcion) VALUES (7, '4 PRIMARIA');
INSERT INTO curso (id, descripcion) VALUES (8, '5 PRIMARIA');
INSERT INTO curso (id, descripcion) VALUES (9, '6 PRIMARIA');

INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(0,0,750, 1000, 600, 400, 690, 720, 850);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(1,1,750, 1100, 600, 500, 690, 720, 850);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(2,2,750, 1200, 600, 600, 690, 720, 850);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(3,3,750, 1300, 600, 700, 690, 720, 850);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(4,4,1000, 1400,600, 800, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(5,5,1000, 1500, 600, 900, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(6,6,1000, 1600,600, 1000, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(7,7,1000, 1700,600, 1100, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(8,8,1000, 1800,600, 1200, 850, 900, 1000);
INSERT INTO costos(id, idcurso, insc, libros,seguro, material, primer,segundo,tercero) values(9,9,1000, 1800,600, 1200, 850, 900, 1000);

INSERT INTO categoriaspersonal (id, descripcion) VALUES (0, 'Docente');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (1, 'Nanas');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (2, 'Educacion Fisica');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (3, 'Ingles');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (4, 'Administrativos');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (5, 'Cocineras');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (6, 'Intendentes');
INSERT INTO categoriaspersonal (id, descripcion) VALUES (7, 'Otros');

  
INSERT INTO papas(id, nombre, apellido, Celular, Civil, Profesion) VALUES(1, 'FELIPE SANTIAGO', 'ALONZO AMAYA', 9991837699,1, 'Licenciado En Administracion de Empresas');
INSERT INTO papas(id, nombre, apellido, Celular, Civil, Profesion) VALUES(2, 'JUAN', 'PEREZ ALCOCER', 9992325673 ,2, 'Ejecutivo de Coca Cola');

INSERT INTO alumnos(id, idpapa, idcurso, fnacimiento, curp, nombre, apellido, telefono, pnacer, alergias) VALUES(0, 1, 5, '30/09/1994', 'JWSJPIOFJO31', 'FELIPE', 'ALONZO CAMAS', '9863598', 'ninguno', 'ninguna');
INSERT INTO alumnos(id, idpapa, idcurso, fnacimiento, curp, nombre, apellido, telefono, pnacer, alergias) VALUES(1, 1, 9, '15/02/1998', 'JFPJPAIOE453', 'FELIPE', 'ALONZO ESTRELLA', '9863599', 'ninguna', 'ninguna');

INSERT INTO personal (id, idcategoria,fnacimiento,nombre,apellido,celular,civil,nivelestudiosid,sueldo) VALUES(0,0,'23/12/1970','SINESIO','VALENZUELA TEJADA','9992343123',1,3,2900);
INSERT INTO personal (id, idcategoria,fnacimiento,nombre,apellido,celular,civil,nivelestudiosid,sueldo) VALUES(1,0,'12/05/1980','CASIMIRO TOLEDO','Valencia Torado','9991324501',2,3,4200);
INSERT INTO personal (id, idcategoria,fnacimiento,nombre,apellido,celular,civil,nivelestudiosid,sueldo) VALUES(2,2,'01/02/1963','SANTIAGO','ORLOVA CORDERO','9993124013',2,3,1200);
INSERT INTO personal (id, idcategoria,fnacimiento,nombre,apellido,celular,civil,nivelestudiosid,sueldo) VALUES(3,1,'10/06/1975','EDGAR CLAUDIO','CHIQUITO PEREZ','9999148284',3,2,1900);
INSERT INTO personal (id, idcategoria,fnacimiento,nombre,apellido,celular,civil,nivelestudiosid,sueldo) VALUES(4,3,'11/10/1934','CRISTOBAL ','MENDOZA VARELA','9992139431',2,1,2850.30);
INSERT INTO personal (id, idcategoria,fnacimiento,nombre,apellido,celular,civil,nivelestudiosid,sueldo) VALUES(5,5,'12/04/1957','REGINA','MARTINES CARDENAS','9992310923',1,1,1200);
INSERT INTO personal (id, idcategoria,fnacimiento,nombre,apellido,celular,civil,nivelestudiosid,sueldo) VALUES(6,5,'02/09/1980','FABIOLA','BURGOS ESTRELLA','9991094209',1,1,1650);


INSERT INTO mensualidades (idalumno,agosto,septiembre,octubre,noviembre,diciembre,enero,febrero,marzo,abril,mayo,junio,julio, seguro, material, libros, inscripcion) Values(0,true,true,true,true,true,true,true,true,true,true,true, false, true,false,true, false);
INSERT INTO mensualidades (idalumno,agosto,septiembre,octubre,noviembre,diciembre,enero,febrero,marzo,abril,mayo,junio, julio, seguro, material, libros, inscripcion) Values(1,true,true,true,true,true,true,true,true,true,true,true, true, true,true,true, true);
--INSERT INTO mensualidades (idalumno,agosto,septiembre,octubre,noviembre,diciembre,enero,febrero,marzo,abril,mayo,junio,julio, seguro, material, libros, inscripcion) Values(2,true,true,true,true,true,true,true,true,true,true,true, true, true,true,true, true);



Select *from usuarios;
--*********************************************
--QUERYS
--*********************************************

--Select fnacimiento From Alumnos;

--select al.id, al.nombre, al.apellido, al.telefono ,al.idpapa FROM papas pa
--inner join alumnos al on pa.id=al.idpapa;

 --Select fnacimiento From Alumnos al extract(year from al.fnacimiento)
--select * FROM estadocivil

--Select al.id, al.idpapa,al.nombre, al.apellido,cu.descripcion ,pa.nombre, pa.apellido From Alumnos al
--inner join curso cu on al.idcurso=cu.id
--inner join papas pa on pa.id=al.idpapa
--where cu.id=4;

--Select al.id, al.idpapa, cu.descripcion, al.fnacimiento, al.nombre, al.apellido, al.telefono, al.pnacer, al.alergias From Alumnos al
--inner join curso cu On cu.id=al.idcurso;

--Select al.id, al.idpapa, cu.descripcion, al.fnacimiento, al.nombre, al.apellido, al.telefono, al.pnacer, al.alergias From Alumnos al inner join curso cu On cu.id=al.idcurso 

--select*From papas;
--INSERT INTO alumnos(id, idpapa, idcurso, fnacimiento, nombre, apellido, telefono, pnacer, alergias) SELECT MAX(id)+1,2,5,'20/05/2002','JESUS', 'ALBERTO SANCHEZ','9222222','8 MESINO','PENICILINA' FROM alumnos

--Select pa.id,pa.nombre, pa.apellido, pa.celular, ec.descripcion,pa.profesion, pa.direccion From papas pa
--inner join estadocivil ec On ec.id=pa.civil
--WHERE Pa.id=1;


--Select *From Alumnos al
--inner join papas pa on al.idpapa=pa.id
--order by al.idpapa;
--Select id From papas where nombre= 'ZAZI NOEMI'


--UPDATE papas set nombre='FELIPE', apellido='ALONZO AMAYA',civil=2, profesion='Economia' ,celular='9992324564' where id=1; 
--UPDATE personal set nombre='', apellido='',celular='', fnacimiento='',idcategoria=, civil= ,nivelestudiosid= where id=
--UPDATE mensualidades set agosto=false where idalumno=0;
--Select*From papas;
--INSERT INTO papas(id,nombre, apellido, celular, civil, profesion) SELECT MAX(id)+1,'ZAZI NOEMI', 'CAMAS RODRIGUEZ','9999973491', 0, 'MAESTRA DE INGLES' FROM papas
 Select al.id, al.idpapa, cu.descripcion, al.fnacimiento, al.nombre, al.apellido, al.telefono, al.pnacer, al.alergias From Alumnos al inner join curso cu On cu.id=al.idcurso where al.nombre= 'FELIPE' Order by al.IdCurso;
Select*FROM personal;
Select pe.id,cp.descripcion,pe.fnacimiento, pe.nombre, pe.apellido, pe.celular,ec.descripcion,ne.descripcion,pe.sueldo from personal pe
inner join categoriaspersonal cp on pe.idcategoria=cp.id
inner join estadocivil ec on pe.civil=ec.id
inner join nivelestudios ne on pe.nivelestudiosid=ne.id;

--Select al.id, al.idpapa, cu.descripcion, al.fnacimiento, al.nombre, al.apellido, al.telefono, al.pnacer, al.alergias From Alumnos al inner join curso cu On cu.id=al.idcurso where al.idcurso= 2;
--INSERT INTO personal (id, idcategoria,fnacimiento,nombre,apellido,celular,civil,nivelestudiosid,sueldo) SELECT MAX(id)+1,, '//','','', '',,, FROM personal

Select*From mensualidades where idalumno=0;

-- Cargar los que deben un mes
Select * From alumnos al
inner join mensualidades m on al.id=m.idalumno
inner join curso c on c.id=al.idcurso
where m.agosto=false;

Select c.seguro+ c.insc+  c.libros+ c.material as total FROM costos c
inner join curso cu on cu.id=c.idcurso
where c.idcurso=0;

Select * FROM costos c
inner join curso cu on cu.id=c.idcurso
where c.idcurso=0;

Select primer from costos where idcurso=1;
Select idcurso from alumnos where id=1;

Select al.id, al.idpapa, cu.descripcion, al.fnacimiento, 
al.curp, al.nombre, al.apellido, al.telefono, al.pnacer, 
al.alergias From Alumnos al inner join curso cu On cu.id=al.idcurso where al.idcurso=5 order by al.apellido;

Select*From Egresos;

--SELECT* EXTRACT(MONTH FROM ingresos.fecha) '2000-12-16 12:21:13');
--Select*From Ingresos on interval date '01/08/2015' - date '30/08/2015')

--UPDATE mensualidades set inscripcion=False, libros=True, material=False, seguro=False  where idalumno=1
Select sum (total) as totaldia from ingresos
where fecha='27/08/2015';